<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifRegularAdmin
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return redirect('/');

	    if(!Auth::user()->isRegularAdmin()) return redirect('/');
	        
	    return $next($request);    
    }
}
