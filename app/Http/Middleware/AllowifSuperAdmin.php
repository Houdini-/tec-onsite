<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AllowifSuperAdmin
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check()) return redirect('/');

	    if(!Auth::user()->isSuperAdmin())  return redirect('/');;
	        
	    return $next($request);    
    }
}
