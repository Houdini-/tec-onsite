<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'home';
    }

    public function index()
    {
    	$data = ['module' => $this->module];

        return view('home',$data);
    }
}
