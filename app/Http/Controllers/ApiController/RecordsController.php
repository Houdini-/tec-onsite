<?php

namespace App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TEC;
use App\Models\RTT;
use App\Models\FULLTAX;
use App\Models\TTR;
use DB;
use Yajra\DataTables\Facades\DataTables;
use Auth;
class RecordsController extends Controller
{
    //
    protected function all_transaction_datatables($user_id = "")
    {
        try
        {
            $data = array();
            
            $data = self::get_alltrans($user_id);
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data)  {
                $start_tools = '<a href="/payment/'.$data->onsite_reference.'/'.$data->cashier_id.'" class="btn btn-primary text-white ';
                if(!$data->cashier_id) $start_tools .= " d-none ";
                $start_tools .='" ><i class="mdi mdi-print"></i> PRINT OR</a>';
                return  $start_tools;
            })
            ->addColumn('class_type', function($data)  {
                if($data->class == "1") $type = "FIRST CLASS";
                else if($data->class == "2") $type = "ECONOMY/BUSINESS CLASS";
                return  $type;
            })
            ->setRowId(function($data){
                return $data->id;
            });

            $rawColumns = ['action','onsite_reference','or_no', 'full_name','fulltax_amount','class_type','ticket_no','created_at','country_name','trans_type'];

        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
    protected function get_alltrans($user_id = "")
	{
        if($user_id != ""){
            $if_cashier = DB::table('users')->find($user_id);
            $user_level = $if_cashier->level;
        
            $rtt = DB::table('rtt_application as a')
            ->leftjoin('cashier_transactions as b','b.onsite_reference','a.onsite_reference')
            ->selectRaw('a.id,type_reduced_id,or_no,a.onsite_reference,concat(first_name," ",ifnull(middle_name,"")," ",last_name) as full_name,format(reduced_amount,2),"REDUCED" as trans_type,ticket_no,date_application,country_id,cashier_id')
            ->where('is_onsite','1')
            ->whereNotnull('a.onsite_reference');

            $fulltax = DB::table('fulltax_application as a')
            ->leftjoin('cashier_transactions as b','b.onsite_reference','a.onsite_reference')
            ->selectRaw('a.id,class,or_no,a.onsite_reference,concat(first_name," ",ifnull(middle_name,"")," ",last_name) as full_name,format(fulltax_amount,2) as fulltax_amount,"FULLTAX" as trans_type,ticket_no,a.created_at,airlines_id as destination_id,cashier_id')
            ->where('is_onsite','1')
            ->whereNotnull('a.onsite_reference')
            ->union($rtt);
        
            $sql = DB::table(DB::raw("({$fulltax->toSql()}) as a "))
            ->leftjoin('countries as b','b.id','a.destination_id');
            if($user_level == "5"){
                $sql->join('cashier_transactions as c','c.onsite_reference','a.onsite_reference');
              
            }
            
            $sql->selectRaw('a.*,b.name as country_name')
            
            ->groupby('onsite_reference')
            ->orderby('created_at','desc')
            
            ->mergeBindings($fulltax)
            ->mergeBindings($sql)
            ->get();
            info($sql->toSql());
            return $sql;
        }   


        // if($id == "tec")$data = TEC::selectRaw("tec_application.id,'Exemption' as trans_type, CONCAT(tec_application.first_name,' ',ifnull(tec_application.middle_name,''),' ',tec_application.last_name) as name,passport_no,ticket_no,date_flight as departure_date,concat(users.first_name,' ',users.last_name) as processor,date_application")->leftjoin('users','users.id','tec_application.created_by');
        // else if($id == "ttr")  $data =TTR::selectRaw("tr_application.id,'Refund' as trans_type, CONCAT(tr_application.first_name,' ',ifnull(tr_application.middle_name,''),' ',tr_application.last_name) as name,passport_no,ticket_no,date_flight as departure_date,concat(users.first_name,' ',users.last_name) as processor,date_application")->leftjoin('users','users.id','tr_application.created_by');
        // else if($id == "rtt") $data = RTT::selectRaw("rtt_application.id,'Reduced' as trans_type, CONCAT(rtt_application.first_name,' ',ifnull(rtt_application.middle_name,''),' ',rtt_application.last_name) as name,passport_no,ticket_no,date_flight as departure_date,concat(users.first_name,' ',users.last_name) as processor,date_application")->leftjoin('users','users.id','rtt_application.created_by');
        // else  $data = FULLTAX::selectRaw("fulltax_application.id,'Full Tax' as trans_type, CONCAT(fulltax_application.first_name,' ',ifnull(fulltax_application.middle_name,''),' ',fulltax_application.last_name) as name,passport_no,ticket_no,departure_date,concat(users.first_name,' ',users.last_name) as processor,date_application")->leftjoin('users','users.id','fulltax_application.created_by');
		// return $data->get();
        
	}
}
