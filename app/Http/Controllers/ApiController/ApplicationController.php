<?php

namespace App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Auth;
class ApplicationController extends Controller
{
    //
    public function filter_tec_application_by(request $request)
    {   
        try
        { 

            $tec_id = $request->get('tec_id');
            $filter_by = 'application_status';
            $filter_user = $request->get('filter_user');
            $filter_list = $request->get('filter_list');
            $filter_order = $request->get('filter_order');
            $filter_value = $request->get('filter_value');
            $filter_tec = $request->get('filter_tec');
            $start = $request->get('start');
            $filter_month = $request->get('filter_month');
            $filter_year = $request->get('filter_year');
            $filter_from = $request->get('filter_from');
            $filter_to = $request->get('filter_to');


            $query = DB::table('tec_application as ta')
            ->where('ta.deleted_at', null)
            ->select('ta.*', DB::raw('CONCAT(UCASE(ta.last_name),", ",UCASE(ta.first_name)," ", IFNULL(SUBSTRING(UCASE(ta.middle_name), 1, 1), ""), IF(ISNULL(SUBSTRING(UCASE(ta.middle_name), 1, 1)) or ta.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("TEC-", LPAD(ta.tec_no, 5, 0)) AS tec_id'), DB::raw('DATE_FORMAT(ta.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

            if($filter_by)
            {
                if($filter_by == 'application_status')
                {
                    if($filter_list) 
                    {
                        if($filter_list == 4)
                        {
                            $query->whereNotNull('ta.generate_to_email');

                            if(Auth::user()->isRegularAdmin()) 
                            {
                                $query->where('ta.supervisor_id', Auth::user()->id);
                              
                            }
                            
                        }
                        elseif($filter_list == 'all')
                        {
                            if($request->get('dupcheck') != "1")
                            {
                                if(Auth::user()->isRegularAdmin()) 
                                {
                                    $query->wherein('ta.status', [2, 3])->whereNotNull('ta.generate_to_email');
                                }
                                elseif(Auth::user()->isProcessor() || Auth::user()->isSuperAdmin())
                                {
                                    $query->wherein('ta.status', [1, 2, 3])->whereNotNull('ta.generate_to_email');
                                }
                            }
                        }
                        elseif($filter_list == 2)
                        {
                            $query->where('ta.status', $filter_list)->whereNull('ta.generate_to_email');
                        }
                        else
                        {
                            $query->where('ta.status', $filter_list);
                        }
                    }  
                }
            }
            if(in_array($filter_order, ['0', '1'])) 
            {
                $filter_order = $filter_order == '1' ? 'asc' : 'desc';

                $query->orderby('ta.id', $filter_order);
            }
            // Only for the processor
            if(Auth::user()->isProcessor())
            {
                $query->where('ta.assign_processor_id', Auth::user()->id);
            }
            else
            {
                if($filter_user) $query->where('ta.assign_processor_id', $filter_user);
            }
            //if($filter_value) $query->where('ta.id', 'like', '%'.$filter_value.'%');
            if($filter_value) 
            {
                $query->where(function ($query) use ($filter_value) {
                    $query->where(DB::raw('LCASE(CONCAT(ta.last_name,ta.first_name))'), 'like', '%'.$filter_value.'%');
                           //->having('app_id', 'like', '%'.$filter_value.'%');
                        //   ->orwhere('ta.middle_name', 'like', '%'.$filter_value.'%');
                });
            }
            if($filter_month && $filter_year)
            {
                $start_date = implode('-', [$filter_year, $filter_month, '01']);
                $end_date = implode('-', [$filter_year, $filter_month, '31']);

                $query->whereBetween('date_application', [ $start_date , $end_date]);
            }

            if($filter_tec !="")
            {
                $query->having('tec_id','like','%'.$filter_tec.'%');
            }
            if($filter_from != "" && $filter_to != "" ){
                $from = date('Y-m-d', strtotime($filter_from));
                $to = date('Y-m-d', strtotime($filter_to));
                $query->whereBetween('date_application',[$from,$to]);
            }
            else if($filter_from != "")
            {
                $from = date('Y-m-d', strtotime($filter_from));
                $query->where('date_application','<=',$from);
            }
            $response = array();

            if($tec_id)
            {
                $tec_application = $query->where('ta.id', $tec_id)->first(); 

                /**
                $tec_application->file_0 = null;
                $tec_application->file_1 = null;
                $tec_application->file_2 = null;
                $tec_application->file_3 = null;

                foreach($this->get_tec_application_upload(null, $tec_application->id, $tec_application->applicant_type_id) as $key => $val)
                {
                    $file_name = substr($val->file_name, 0, 6); //'file_'.$key;

                    $tec_application->$file_name =  $val->file_name;
                }
                **/
 
                $response['tec_application'] = $tec_application;
            }
            else
            {

                //$response['query'] = $query;
                if($start >= 0) $query->skip($start)->take(10);
  
              
                $tec_application = $query->get();

                 $response['tec_applications'] = $tec_application;
                $response['count_tec_application'] = count($tec_application);
            }  
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }
}
