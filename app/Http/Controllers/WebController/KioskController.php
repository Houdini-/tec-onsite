<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TT_KIOSK;
use App\Models\Countries;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode; 
use Illuminate\Support\Facades\Storage;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
 
class KioskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kiosk.welcome');
    }

    public function show_singlemultiple()
    {
        return view('kiosk.type');

    }
    public function show_single()
    {
        $countries = Countries::orderby('name','asc')->get();
        $data = ['countries'=>$countries];
        return view('kiosk.single-form',$data);
    }
    public function show_multiple()
    {
        $countries = Countries::orderby('name','asc')->get();
        $data = ['countries'=>$countries];
        return view('kiosk.multiple-form',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validation(Request $request)
    {
        $request->validate([
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'sometimes|nullable|max:50',
            'ticket_no' => 'required',
            'passport_no' => 'required',
            'mobile_no' => 'required',
            'email_address' => 'required',
            'destination' => 'required',
        ]);
        $data = ['details' => $request->all()];
        return view('kiosk.single-validate',$data);
    }
    public function multiple_validation(Request $request)
    {
        $data = ['details' => $request->all()];
        return view('kiosk.multiple-validate',$data);

    }
    public function store(Request $request)
    {
        try{
            $last_name      =  $request->last_name;
            $first_name     =  $request->first_name;
            $middle_name    =  $request->middle_name;
            $ticket_no      =  $request->ticket_no;
            $passport_no    =  $request->passport_no;
            $mobile_no      =  $request->mobile_no;
            $email_address  =  $request->email_address;
            $destination    =  $request->destination;


            $ttk = new TT_KIOSK;
            $ttk->ref_no            =   self::getKioskReference();
            $ttk->last_name         =   $last_name;
            $ttk->first_name        =   $first_name;
            $ttk->middle_name       =   $middle_name;
            $ttk->ticket_no         =   $ticket_no;
            $ttk->passport_no       =   $passport_no;
            $ttk->mobile_no         =   $mobile_no;
            $ttk->email_address     =   $email_address;
            $ttk->destination       =   $destination;
            if($ttk->save()) 
            {
                self::generate_qrcode($ttk->ref_no);
                $id = $ttk->ref_no;
                return redirect()->route('kiosk.show_qr',$id);
            }
            
            // 
            

        } catch (\Throwable $th) {
            
            throw $th;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function generate_qrcode($id)
    {
        $f_tax = TT_KIOSK::where('ref_no',$id)->first();
        $qrcode_msg = $f_tax->ref_no;

         $pngImage = QrCode::format('png')->merge('img/tieza-logo.png', 0.3, true)
         ->size(500)->errorCorrection('H')
         ->generate($qrcode_msg);

         $output_file = '/tieza/fulltax/qr-code/'.$f_tax->ref_no.'/_qrcode.png';
         Storage::disk('storage')->put($output_file, $pngImage);

         return response()->json([
            'details' =>$f_tax ], 201);
    }
    public function show_qrcode($id)
    {
        // $_id = decrypt($id);
        $kiosk = TT_KIOSK::where('ref_no',$id)->first();
        
        $data = ['id' => $kiosk->ref_no,'kiosk' => $kiosk];
        
        return view('kiosk.qr_display',$data);
    }
    public function getKioskReference(){
        $kiosk = TT_KIOSK::orderBy('id','desc')->first();
      
        $last_ticket = $kiosk->ref_no ?? 0;
		$tick_date	= substr($last_ticket, 0, 10);
		$tick_date_now = date('Y-m-d');
		$tick_pre 	= "-APPKIOSK-";
		$tick_start = "000";
		$tick_num	= substr($last_ticket, 21, 24);
		
		if ($tick_date == $tick_date_now) { 
			$tick_d1 = $tick_date; 
			$tick_d2 = $tick_pre;
			$tick_dx = $tick_num + 1;
			$tick_d3 = str_pad($tick_dx, 3, '0', STR_PAD_LEFT);
		} else { 
			$tick_d1 = $tick_date_now;
		    $tick_d2 = $tick_pre;
			$tick_d3 = $tick_start;
		}
		 return $tick_dt = $tick_d1.$tick_d2.$tick_d3;
    }
    public function get_kioskdetails($id)
    {
        $details = TT_KIOSK::where('ref_no',$id)->get();
        $data = ['details'=>$details, 'count'=>count($details)];
        return $data;
    }
    public function store_multiple(Request $request)
    {
        $ref_no  =  self::getKioskReference();
        try{
            for ($i=0; $i < count($request->last_name) ; $i++) { 

            $last_name      =  $request['last_name'][$i];
            $first_name     =  $request['first_name'][$i];
            $middle_name    =  $request['middle_name'][$i];
            $ticket_no      =  $request['ticket_no'][$i];
            $passport_no    =  $request['passport_no'][$i];
            $mobile_no      =  $request['mobile_no'][$i];
            $email_address  =  $request['email_address'][$i];
            $destination    =  $request['destination'][$i];


            $ttk = new TT_KIOSK;
            $ttk->ref_no            =   $ref_no;
            $ttk->last_name         =   $last_name;
            $ttk->first_name        =   $first_name;
            $ttk->middle_name       =   $middle_name;
            $ttk->ticket_no         =   $ticket_no;
            $ttk->passport_no       =   $passport_no;
            $ttk->mobile_no         =   $mobile_no;
            $ttk->email_address     =   $email_address;
            $ttk->destination       =   $destination;
                if($ttk->save()) 
                {
                   
                }
            }
            self::generate_qrcode($ref_no);
            // $id = encrypt($ref_no);
            return redirect()->route('kiosk.show_qr',$ref_no);
        } catch (\Throwable $th) {
            throw $th;
        }
        
    }
    public function display_multiple($id)
    {
        $kiosk = TT_KIOSK::where('ref_no',$id)->get();
        $data = ['details'=>$kiosk,'module'=>'application'];
        return view('kiosk.multiple-display',$data);
    }
}
