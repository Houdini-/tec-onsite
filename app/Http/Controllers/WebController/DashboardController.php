<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TEC;
use App\Models\RTT;
use App\Models\FULLTAX;
use App\Models\TTR;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $ft_fc = FULLTAX::where('class','1')->get();
        $ft_ec = FULLTAX::where('class','2')->get();

        $tec_ad = TEC::where('status','1')->get();
        $tec_aa = TEC::where('status','2')->get();
        $tec_da = TEC::whereIn('status',['3','5'])->get();
        $tec_ta = TEC::where('status','<>','0')->get();

        $rtt_ad = RTT::where('status','1')->get();
        $rtt_aa = RTT::where('status','2')->get();
        $rtt_da = RTT::whereIn('status',['3','5'])->get();
        $rtt_ta = RTT::where('status','<>','0')->get();

        $ttr_ad = TTR::where('status','1')->get();
        $ttr_aa = TTR::where('status','2')->get();
        $ttr_da = TTR::whereIn('status',['3','5'])->get();
        $ttr_ta = TTR::where('status','<>','0')->get();

        $data = ['module' => 'blank','ft_fc'=>$ft_fc->count(),'ft_ec'=>$ft_ec->count(),
        'tec_ad'=>$tec_ad->count(),'tec_aa'=>$tec_aa->count(),'tec_da'=>$tec_da->count(),'tec_ta'=>$tec_ta->count(),
        'rtt_ad'=>$rtt_ad->count(),'rtt_aa'=>$rtt_aa->count(),'rtt_da'=>$rtt_da->count(),'rtt_ta'=>$rtt_ta->count(),
        'ttr_ad'=>$ttr_ad->count(),'ttr_aa'=>$ttr_aa->count(),'ttr_da'=>$ttr_da->count(),'ttr_ta'=>$ttr_ta->count()
        ];
        return view('dashboard.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
