<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Airlines;
use App\Models\FULLTAX;
use DB;
use Illuminate\Support\Facades\Auth;


class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   protected $application_status = array(
        ['id' => '1', 'name' => 'awaiting_documents'],
        ['id' => '2', 'name' => 'approved'],
        ['id' => '3', 'name' => 'denied'],
        ['id' => '5', 'name' => 'cancelled'],
        );
    public function index()
    {
        //
       
        $airlines = Airlines::orderby('name')->get();
        
        $data = ['module' => 'application','application_status' =>$this->application_status ?? '','airlines' => $airlines];
        return view('reports.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function print_application(Request $request)
    {
        // $option = $request->get('radio-inline');
        $report_name    = $request->get('rpt_name');
        $month          = $request->get('month');
        $year           = $request->get('year');
        $status         = $request->get('status');
        $completed      = $request->get('completed');
        $start_date     = $request->get('start_date');
        $end_date       = $request->get('end_date');
        $data = array();
        $data = array_merge($data, ['report_name' => $report_name]);
        if($report_name == "ft_payment"){
            $query = DB::table('fulltax_application');
            
            if($start_date && $end_date)
            {
                $query->whereBetween(DB::raw('DATE(created_at)'), [$start_date, $end_date]);
                $data['start_date'] = $start_date;
                $data['end_date'] = $end_date;
            }
            else
            {
                if($month) 
                {
                    $data['month'] = $month;
                }
                if($year) 
                {
                    $data['year'] = $year;
                }    
            }
            $applications = $query->get()->toarray(); 
            // $val->count = $count_application = count($applications);
            // $total_count += $val->count;
            $data = array_merge($data,['applications' => $applications]);
            return view('reports.ft_payment',$data);
        }
        else if($report_name == "tec_granted"){
           
            $query = DB::table('tec_application as a')
            ->leftjoin('sections as b','b.id','a.applicant_type_id')
            ->selectRaw('a.*,b.code as section_code')
            ->wherenotnull('generate_to_email');
            if($start_date && $end_date)
            {
                $query->whereBetween(DB::raw('DATE(date_application)'), [$start_date, $end_date]);
                $data['start_date'] = $start_date;
                $data['end_date'] = $end_date;
            }
            else
            {
                if($month) 
                {
                    $data['month'] = $month;
                }
                if($year) 
                {
                    $data['year'] = $year;
                }    
            }
            
            $applications = $query->get()->toarray(); 
            // $val->count = $count_application = count($applications);
            // $total_count += $val->count;
            $data = array_merge($data,['applications' => $applications]);
            return view('reports.tec_granted',$data);
        }
        else if($report_name == "rtt_granted"){
            return view('reports.rtt_granted');
        }
        else if($report_name == "rep_coll_dep"){
            return view('reports.collection_deposit',$data);
        }
        else if($report_name == "summ_cash_coll_dep"){
            return view('reports.summary_cash_collection_deposit',$data);
        }
        else if($report_name == "summ_coll_dep"){
            return view('reports.summary_collection_deposit',$data);
        }
        else if($report_name == "deposits"){
            return view('reports.deposits',$data);
        }
        else if($report_name == "daily_collection"){
            return view('reports.daily_collection',$data);
        }
        else if($report_name == "or_used"){
            return view('reports.or_used',$data);
        }
        else if($report_name == "monthly_time_motion"){
            return view('reports.monthly_time_motion',$data);
        }
        else if($report_name == "tec_used"){
            return view('reports.tec_used',$data);
        }
        else if($report_name == "rtt_used"){
            return view('reports.rtt_used',$data);
        }
        else if($report_name == "rtt_payment"){
            return view('reports.payment_reduced',$data);
        }
        else if($report_name == "summ_cash_dep"){

            return view('reports.summary_cash_deposit',$data);
        }
    }
}
