<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Countries;
use App\Models\Airlines;
use App\Models\Sections;
use App\Models\TEC;
use App\Models\RTT;
use App\Models\RTT_AMT;
use App\Models\FULLTAX;
use App\Models\TTR_AMT;
use App\Models\TT_KIOSK;
use App\Models\OS_REFERENCE;
use App\Models\Users_settings;
use App\Models\PassengerType;
use App\Models\CashierTrans;

use Session;
use PDF;
use Crypt;
use DB;

class ApplicationController extends Controller
{
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->id){
            $details = TT_KIOSK::find(decrypt($request->id));
            $type = $request->type;
        }
        $countries = Countries::selectRaw('id,UPPER(name) as name')->orderBy('name')->get();
        $section = Sections::all();
        $passenger = PassengerType::all();



        $data = ['module' => 'application','countries'=>$countries,'section' => $section, 'passenger' => $passenger];
        return view('applications.application',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $reference_no = self::getOnsiteReference();
            // $or_no = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
            for ($i=0; $i < count($request->lastname) ; $i++) { 
               $type = $request['code'][$i];
               $amount = str_replace(",","",$request['amount'][$i]);
               if($type == "1"){//fulltax
                    $ft = new FULLTAX;
                    $ft->last_name              =   $request['lastname'][$i];
                    $ft->first_name             =   $request['firstname'][$i];
                    $ft->middle_name            =   $request['middlename'][$i];
                    $ft->passport_no            =   $request['passport'][$i];
                    $ft->ticket_no              =   $request['ticket'][$i];
                    $ft->class                  =   $request['class'][$i];
                    $ft->type                   =   $request['type'][$i];
                    $ft->type_others            =   $request['type_others'][$i];
                    $ft->airlines_id            =   $request['destination'][$i];
                    $ft->fulltax_amount         =   $amount ?? 0.00; 
                    $ft->is_onsite              =   "1";
                    $ft->onsite_reference       =   $reference_no;
                    $ft->created_by             =   auth()->user()->id;
                    // $ft->or_no                  =   $or_no->or_no;
                    if($ft->save()) {
                        
                    }
                    else info('error');
               }
               else if($type == "2"){//exemption
                    $tec_cert = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                    
                    $tec = new TEC;
                    $tec->last_name              =   $request['lastname'][$i];
                    $tec->first_name             =   $request['firstname'][$i];
                    $tec->middle_name            =   $request['middlename'][$i];
                    $tec->passport_no            =   $request['passport'][$i];
                    $tec->ticket_no              =   $request['ticket'][$i];
                    $tec->country_id             =   $request['destination'][$i];
                    $tec->applicant_type_id      =   $request['section'][$i];
                    $tec->date_validity          =   $request['validity'][$i];
                    $tec->is_onsite              =   "1";
                    $tec->onsite_reference       =   $reference_no;
                    $tec->date_application       =   now();
                    $tec->created_by             =   auth()->user()->id;
                    $tec->tec_no                 =   $tec_cert->tec_no;
                    if($tec->save()) {
                        try {
                            $data = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                            $data->tec_no    =   ($data->tec_no ?? 0) + 1;
                            $data->update();
                            info($newtec);
                          
                        } catch (\Throwable $th) {
                            info($th);
                        }
                    }
                    else info('error');

               }
               else if($type == "3"){//reduced
                    $rtt_cert = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                    
                    $rtt = new RTT;
                    $rtt->last_name              =   $request['lastname'][$i];
                    $rtt->first_name             =   $request['firstname'][$i];
                    $rtt->middle_name            =   $request['middlename'][$i];
                    $rtt->passport_no            =   $request['passport'][$i];
                    $rtt->ticket_no              =   $request['ticket'][$i];
                    $rtt->country_id             =   $request['destination'][$i];
                    $rtt->applicant_type_id      =   $request['section'][$i];
                    $rtt->date_validity          =   $request['validity'][$i];
                    $rtt->reduced_amount         =   $amount ?? 0.00;
                    $rtt->with_ticket            =   $request['withticket'][$i];
                    $rtt->type_reduced_id        =   $request['class'][$i];
                    $rtt->is_onsite              =   "1";
                    $rtt->onsite_reference       =   $reference_no;
                    $rtt->date_application       =   now();
                    $rtt->created_by             =   auth()->user()->id;
                    $rtt->rtt_no                 =   $rtt_cert->rtt_no;
                    // $rtt->or_no                  =   $or_no->or_no;

                    if($rtt->save()) {
                        try {
                            $data = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                            $data->rtt_no    =   ($data->rtt_no ?? 0) + 1;
                            $data->update();
                        } catch (\Throwable $th) {
                            info($th);
                        }
                    }
               }
            }
            try {
                $data = Users_settings::where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                $data->or_no    =   ($data->or_no ?? 0) + 1;
                $data->update();
              
            } catch (\Throwable $th) {
                info($th);
            }
            $onsite_ref = OS_REFERENCE::where('id','1')
            ->update(['reference_no' => $reference_no]);
                return $reference_no;
           } catch (\Throwable $th) {
              info($th);
           }
    }
    public function confirmation($id)
    {
        $tec = DB::table('tec_application as a')
        ->selectRaw('last_name,first_name,middle_name,"EXEMPTION", tec_no,0.00,a.id,"N/A",b.code,onsite_reference,passport_no,ticket_no,date_application ')
        ->leftjoin('sections as b','b.id','a.applicant_type_id')
        ->where('onsite_reference',$id);

        $rtt = DB::table('rtt_application as a')
        ->leftjoin('sections as b','b.id','a.applicant_type_id')
        ->selectRaw('last_name,first_name,middle_name,"REDUCED", rtt_no,reduced_amount,a.id,if(type_reduced_id = "2","E","F"),b.code,onsite_reference,passport_no,ticket_no,date_application')
        ->where('onsite_reference',$id);

        $details = DB::table('fulltax_application as a')
        ->selectRaw('last_name,first_name,middle_name,"FULL TAX" as trans,"N/A" as cert_no,fulltax_amount as _amount,a.id,if(class= "2","E","F") as class,"N/A" as section,onsite_reference,passport_no,ticket_no,created_at as application_date')
        ->where('onsite_reference',$id)
        ->union($tec)
        ->union($rtt)
        ->get();
        
        $ifhasCashier = false;
        foreach ($details as $item) {
            if($item->trans == "REDUCED" || $item->trans == "FULL TAX"){
                $ifhasCashier = true;
            }
        }
         $data = ['module' => 'application','details' => $details];
        if($ifhasCashier)  return view('applications.confirmation',$data);
        else  return view('applications.confirmation2',$data);
      
    }
    public function save_rtt(Request $request)
    {
    }
    public function save_fulltax(Request $request)
    {
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getSectionDesc($id)
    {
        $sections = Sections::find($id);
        return $sections->name;
    }
    public function view_application($type,$id)
    {
        $countries = Countries::orderBy('name')->get();
        $airlines = Airlines::orderBy('name')->get();
        $tec_sect = Sections::where('section_type','1')->get();
        $rtt_sect = Sections::whereIn('section_type',['3','4'])->wherenotnull('code')->get();
        $tec ="";
        $fulltax = "";
        $rtt = "";

        switch ($type) {
            case 'tec':
                $tec = TEC::from('tec_application as ta')->selectRaw('CONCAT("TEC-", tec_no) AS tec_id,CONCAT("APP-", LPAD(id, 5, 0)) AS app_id,DATE_FORMAT(date_application,"%Y-%m-%d") as date_app ,ta.*')->where('id',$id)->first();
                break;      
            case 'fulltax':
                $fulltax = FULLTAX::from('fulltax_application as ft')->selectRaw('DATE_FORMAT(date_application,"%Y-%m-%d") as date_app,ft.*')->where('id',$id)->first();
                // dd($fulltax);
                break;
            case 'rtt':
                //LPAD(rtt_no, 5, 0)
                $rtt = RTT::from('rtt_application as rtt')->selectRaw('CONCAT("RTT-", rtt_no) AS rtt_id,CONCAT("APP-", LPAD(id, 5, 0)) AS app_id,DATE_FORMAT(date_application,"%Y-%m-%d") as date_app ,rtt.*')->where('id',$id)->first();
            default:
                // dd($type);
                break;
        }

           
        
        $data = ['module' => 'application','countries'=>$countries,'airlines'=>$airlines,'tec_sect' => $tec_sect,'rtt_sect' => $rtt_sect,'tec'=>$tec,'fulltax' => $fulltax,'rtt' => $rtt,'type'=> $type];
        return view('applications.show',$data);

    }
    public function generate_cert($id,$type)
    {
        switch ($type) {
            case 'tec':
                $tec = TEC::from('tec_application as a')
                ->leftjoin('sections as b','a.applicant_type_id','b.id')
                ->leftjoin('users as c','a.created_by','c.id')
                ->selectraw('a.*,b.*,concat(c.first_name," ",c.last_name) as processor_name')
                ->where('a.id',$id)->first();
                $data = [
                   'tec' => $tec,
                ];
                return view('applications.tec_cert_sf',$data);
                break;
            case 'rtt':
                $rtt = RTT::from('rtt_application as a')
                ->leftjoin('sections as b','a.applicant_type_id','b.id')
                ->leftjoin('users as c','a.created_by','c.id')
                ->selectraw('a.*,b.*,concat(c.first_name," ",c.last_name) as processor_name')
                ->where('a.id',$id)->first();
                info($rtt);
                $data = [
                   'rtt' => $rtt,
                ];
                return view('applications.rtt_cert_sf',$data);
                break;
            case 'fulltax':
                # code...
                $fulltax = FULLTAX::from('fulltax_application as a')
                ->leftjoin('users as b','a.created_by','b.id')
                ->selectraw('a.*,b.*,concat(a.first_name," ",a.last_name) as payor_name
                ,concat(b.first_name," ",b.last_name) as processor_name')
                ->where('a.id',$id)->first();
                $is_multiple = "";
                $total = 0.00;
                if($fulltax->kiosk_reference != null)
                {
                    $is_multiple = FULLTAX::from('fulltax_application as a')
                    ->selectraw('a.*,concat(a.first_name," ",a.last_name) as payor_name')
                    ->where('kiosk_reference',$fulltax->kiosk_reference)
                    ->get();
                }
               
                $data = [
                    'ft' => $fulltax,
                    'recipients' => $is_multiple,
                    'total' => $total,
                 ];
                return view('applications.official_receipt',$data);
                break;
            default:
                # code...
                break;
        }
        

         // $view = "applications.gen_cert";
        // $pdf =  PDF::setPaper('a4', 'portrait')->setOptions([ 'defaultFont' => 'cambria', 
        // 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(true);
        //  $filename = $tec->last_name.'-'.$tec->first_name.'.pdf';
        // return $pdf->download($filename);
    }
    public function onsite_print_or($onsite_reference){


        $ft = DB::table('fulltax_application as a')
        ->leftjoin('users as b','a.created_by','b.id')
        ->selectraw('a.ticket_no,a.fulltax_amount,concat(a.first_name," ",a.last_name) as payor_name
        ,concat(b.first_name," ",b.last_name) as processor_name')
        ->where('onsite_reference',$onsite_reference)->first();
        if(!$ft){
            $ft = DB::table('rtt_application as a')
            ->leftjoin('users as b','a.created_by','b.id')
            ->selectraw('a.ticket_no,a.reduced_amount as fulltax_amount,concat(a.first_name," ",a.last_name) as payor_name
            ,concat(b.first_name," ",b.last_name) as processor_name')
            ->where('onsite_reference',$onsite_reference)->first();
        }

        $sub = DB::table('rtt_application')
        ->selectRaw('first_name,last_name,ticket_no,reduced_amount')
        ->where('onsite_reference',$onsite_reference);
        $is_multiple = DB::table('fulltax_application')
        ->selectRaw('first_name,last_name,ticket_no,fulltax_amount')
        ->where('onsite_reference',$onsite_reference)
        ->union($sub)
        ->get();
        $total = 0.00;
        
        $data = [
            'ft' => $ft,
            'recipients' => $is_multiple,
            'total' => $total,
         ];
        return view('applications.official_receipt',$data);


    }
    public function get_rtt_sections_by($id)
    {
        $data = Sections::where('section_type',$id)->wherenotnull('code')->get();
        return $data;
    }
    public function save_usersettings(Request $request)
    {
        try {
            $data                =       new Users_settings;
            $data->user_id       =       auth()->user()->id;
            $data->start_tec     =       $request->start_tec;
            $data->end_tec       =       $request->end_tec;
            $data->tec_no        =       $request->tec_no;
            $data->start_rtt     =       $request->start_rtt;
            $data->end_rtt       =       $request->end_rtt;
            $data->rtt_no        =       $request->rtt_no;
            $data->start_or      =       $request->start_or;
            $data->end_or        =       $request->end_or;
            $data->or_no         =       $request->or_no;
            $data->cashier_id    =       $request->level;
            $data->shift_id      =       $request->shift_id;
            $data->office_id     =       $request->office_id;

            if($data->save()){  return back()->with('success','User Settings successfully saved!');}
           
        } catch (\Throwable $th) {
            throw $th;
            //  return back()->with('failed', $th); 
        }
    
    }
    public function getOnsiteReference(){
        $ref = OS_REFERENCE::first();
       
        $last_ticket = $ref->reference_no ?? 0;
		$tick_date	= substr($last_ticket, 2, 2);
		$tick_date_now = date('y');
		$tick_start = "00000";
		$tick_num	= substr($last_ticket, 4, 9);
     
		if ($tick_date == $tick_date_now) { 
			$tick_d1 = $tick_date; 
			$tick_dx = $tick_num + 1;
			$tick_d3 = str_pad($tick_dx, 5, '0', STR_PAD_LEFT);
            
		} else { 
			$tick_d1 = $tick_date_now;
			$tick_d3 = $tick_start;
		}
        $tick = 'OS';
        return $tick_dt  = $tick.$tick_d1.$tick_d3;
    }
    public function sectionWhere($type){
        // info($type);
       if($type == "3")  $getsect =  Sections::wherein('section_type',['3','4'])->get();
       else $getsect =  Sections::where('section_type',$type)->get();
       return $getsect;
    }
    public function getrtt_amt($section,$class){
        $secttype = Sections::find($section);
        $type = $secttype->section_type ?? '';
        $rtt_amt = RTT_AMT::where('type_applicant_fee_id',$class)
        ->where('section_type_id',$type)
        ->first();
        return number_format($rtt_amt->amount,2) ?? 0;
    }
    public function payment_details($id,$cashier){

        // dd($cashier);
        $ifexist = CashierTrans::where('cashier_id',$cashier)->where('onsite_reference',$id)->first();
        if(!$ifexist){
            $sqlCashier = new CashierTrans;
            $sqlCashier->cashier_id          = $cashier;
            $sqlCashier->onsite_reference    = $id;
            $sqlCashier->created_by          = auth()->user()->id;
            $sqlCashier->save();
        }

        $tec = DB::table('tec_application as a')
        ->selectRaw('a.id as id,a.last_name,a.first_name,a.middle_name,"EXEMPTION", tec_no,0.00,a.id,"N/A",b.code,onsite_reference,concat(c.first_name," ",ifnull(c.middle_name,"")," ",c.last_name) as processor_name ,a.passport_no,a.ticket_no,"" as or_no,"" as mop,"" as mop_multi,"" as peso_amt,"" as dollar_rate,"" as card_ref,"" as card_no')
        ->leftjoin('sections as b','b.id','a.applicant_type_id')
        ->leftjoin('users as c','c.id','a.created_by')
        ->where('onsite_reference',$id);

        $rtt = DB::table('rtt_application as a')
        ->leftjoin('sections as b','b.id','a.applicant_type_id')
        ->leftjoin('users as c','c.id','a.created_by')
        ->selectRaw('a.id as id,a.last_name,a.first_name,a.middle_name,"REDUCED", rtt_no,reduced_amount,a.id,if(type_reduced_id = "2","E","F"),b.code,onsite_reference,concat(c.first_name," ",ifnull(c.middle_name,"")," ",c.last_name) as processor_name,a.passport_no,a.ticket_no,or_no,mop,mop_multi,peso_amt,dollar_rate,card_ref,card_no')
        ->where('onsite_reference',$id);

        $details = DB::table('fulltax_application as a')
        ->leftjoin('users as c','c.id','a.created_by')
        ->selectRaw('a.id as id,a.last_name,a.first_name,a.middle_name,"FULL TAX" as trans,"N/A" as cert_no,fulltax_amount as _amount,a.id,if(class= "2","E","F") as class,"N/A" as section,onsite_reference,concat(c.first_name," ",ifnull(c.middle_name,"")," ",c.last_name) as processor_name,a.passport_no,a.ticket_no,or_no,mop,mop_multi,peso_amt,dollar_rate,card_ref,card_no')
        ->where('onsite_reference',$id)
        ->union($tec)
        ->union($rtt)
        ->get();
        $data = ['module' => 'application','details' => $details];
       return view('applications.payment',$data);
 
    }
    public function save_payment_or(Request $request){
        
        $check_or_fulltax = DB::table('fulltax_application')
        ->where('or_no',$request->or_no)
        ->first();
        $check_or_reduced = DB::table('rtt_application')
        ->where('or_no',$request->or_no)
        ->first();
        if($check_or_reduced || $check_or_fulltax){
            $ft_osref = $check_or_fulltax->onsite_reference ?? "";
            $rtt_osref = $check_or_reduced->onsite_reference ?? "";
            if($ft_osref != $request->_id && $rtt_osref != $request->_id){
                return back()->with('error','You have entered a duplicate OR number.');
            }
        } 

        $fulltax = DB::table('fulltax_application')
        ->where('onsite_reference',$request->_id)
        ->update([
            'or_no'=> $request->or_no,
            'mop'=> $request->payment_option,
            'mop_multi'=> $request->mop_multi,
            'peso_amt'=> $request->peso_,
            'dollar_rate'=> $request->dollar_,
            'card_ref'=> $request->card_ref,
            'card_no'=> $request->card_no,
        ]);
        $rtt = DB::table('rtt_application')
        ->where('onsite_reference',$request->_id)
        ->update([
            'or_no'=> $request->or_no,
            'mop'=> $request->payment_option,
            'mop_multi'=> $request->mop_multi,
            'peso_amt'=> $request->peso_,
            'dollar_rate'=> $request->dollar_,
            'card_ref'=> $request->card_ref,
            'card_no'=> $request->card_no,
        ]);

        return back()->with('success','OR number and payment method have been updated!');
    }
}
