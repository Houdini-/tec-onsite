<?php

namespace App\Http\Controllers\WebController\FileSetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FS_CASHIER;
use App\Models\User;

use Yajra\DataTables\Facades\DataTables;
use Session;
use Illuminate\Support\Facades\Crypt;
class CashierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.cashier.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.cashier.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $code = $request->code;
        $last_name = $request->last_name;
        $first_name = $request->first_name;
        $middle_name = $request->middle_name;
        $password = $request->password;
        
        
        try {
            //code...
            $data = new User;
            $data->username             =   $code;
            $data->password             =   bcrypt($password);
            $data->last_name            =   $last_name;
            $data->first_name           =   $first_name;
            $data->middle_name          =   $middle_name ?? ' ';
            $data->level                =   "5";
            $data->status               =   "1";
            $data->created_by           =   auth()->user()->id;
            if($data->save())return back()->with('success', 'Successfully saved!'); 
            

        } catch (\Throwable $th) {
            throw $th;
            // return back()->with('failed', $th); 

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $office = User::find($id);
        $data = ['module' => 'application','val'=>$office];
        return view('filesetup.cashier.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $code = $request->code;
        $last_name = $request->last_name;
        $first_name = $request->first_name;
        $middle_name = $request->middle_name;
        $position = $request->position;
        try {
            $data                       =   User::find($id);
            $data->code                 =   $code;
            $data->last_name            =   $last_name;
            $data->first_name           =   $first_name;
            $data->middle_name          =   $middle_name;
            $data->position             =   $position;
            $data->update();
            return back()->with('success', 'Successfully updated!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function load_cashier()
    {
        try
        {
            $data = array();
            $data = User::from("users as a")
            ->selectraw("concat(first_name,' ',ifnull(middle_name,''),' ',last_name) as name,a.* ")
            ->where('level','5')
            ->get();
        
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) {
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.route('cashier.show', $data->id).'" class="dropdown-item" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';
                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            });

            $rawColumns = ['action','username', 'name'];

        }
        catch(Exception $e)
        {
            info($e->getMessage());
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
}
