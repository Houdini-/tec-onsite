<?php

namespace App\Http\Controllers\WebController\Filesetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Signatories;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;
use Session;

class SignatoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.signatories.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::get();
        $data = ['module' => 'application','users' => $users];
        return view('filesetup.signatories.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $name = $request->name;
        $position = $request->position;
        
        try {
            //code...
            $data = new Signatories;
            $data->name               =   $name;
            $data->position           =   $position;
            $data->created_by         =   auth()->user()->id;
            $data->save();
            return back()->with('success', 'Successfully saved!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $users = User::get();
        $signatories = Signatories::find($id);
        $data = ['module' => 'application','val'=>$signatories,'users'=>$users];
        return view('filesetup.signatories.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $data               =   Signatories::find($id);
            $data->name         =   $request->name;
            $data->position     =   $request->position;
            $data->update();
            return back()->with('success', 'Successfully updated!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function load_signatories()
    {
        try
        {
            $data = array();
            $data = Signatories::with('users')->get();
            $datatables = Datatables::of($data)
            ->addColumn('name',function($data)
            {
                $column = $data->users->first_name.' '.$data->users->last_name;
                return $column;
            })
            ->addColumn('action', function($data) {
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.route('signatories.show', $data->id).'" class="dropdown-item" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';
                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            });

            $rawColumns = ['action','name', 'position'];

        }
        catch(Exception $e)
        {
            info($e->getMessage());
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
}
