<?php

namespace App\Http\Controllers\WebController\FileSetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\FS_SHIFTS;
use Yajra\DataTables\Facades\DataTables;
use Session;
class ShiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.shifts.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.shifts.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $code = $request->code;
        $name = $request->name;
        $from = $request->from;
        $to   = $request->to;

        
        try {
            //code...
            $data = new FS_SHIFTS;
            $data->code           =   $code;
            $data->name           =   $name;
            $data->from           =   $from;
            $data->to             =   $to;
            $data->created_by     =   auth()->user()->id;
            $data->save();
            return back()->with('success', 'Successfully saved!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $shifts = FS_SHIFTS::find($id);
        $data = ['module' => 'application','val'=>$shifts];
        return view('filesetup.shifts.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code = $request->code;
        $name = $request->name;
        $from = $request->from;
        $to   = $request->to;

        
        try {
            //code...
            $data = FS_SHIFTS::find($id);
            $data->code           =   $code;
            $data->name           =   $name;
            $data->from           =   $from;
            $data->to             =   $to;
            // $data->created_by     =   auth()->user()->id;
            $data->update();
            return back()->with('success', 'Successfully saved!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function load_shifts()
    {
        try
        {
            $data = array();
            $data = FS_SHIFTS::get();
        
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) {
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.route('shifts.show', $data->id).'" class="dropdown-item" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';
                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            });

            $rawColumns = ['action','code', 'name'];

        }
        catch(Exception $e)
        {
            info($e->getMessage());
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
}
