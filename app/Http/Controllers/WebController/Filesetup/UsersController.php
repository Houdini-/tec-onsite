<?php

namespace App\Http\Controllers\WebController\Filesetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Airlines;
use App\Models\FS_TTU;
use App\Models\FS_OFFICE;
use App\Models\FS_SATELLITE;
use App\Models\FS_SHIFTS;


use Yajra\DataTables\Facades\DataTables;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = ['module' => 'application'];
        return view('filesetup.user_account.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_level = array( ['id' => 0, 'name' => 'Super Admin'],
        ['id' => 1, 'name' => 'Regular Admin'],
        ['id' => 2, 'name' => 'Processor'],
        ['id' => 3, 'name' => 'User'],
        ['id' => 4, 'name' => 'Travel Tax User'],
        ['id' => 5, 'name' => 'Kiosk']);

        $ttu       = FS_TTU::orderby('name')->get();
        $office    = FS_OFFICE::orderby('name')->get();
        $airlines  = Airlines::orderby('name')->get(); 
        $satellite = FS_SATELLITE::orderby('name')->get(); 
        $shift = FS_SHIFTS::orderby('name')->get(); 




        $data = ['module' => 'application','user_level' => $user_level,'ttu'=>$ttu,'office'=>$office,'airlines'=>$airlines,'satellite'=>$satellite,'shift'=>$shift];
        return view('filesetup.user_account.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = new User;
            $data->first_name         =   $request->first_name;;
            $data->middle_name        =   $request->middle_name;
            $data->last_name          =   $request->last_name;
            $data->username           =   $request->username;;
            $data->password           =   bcrypt($request->password);
            $data->email              =   $request->email;
            $data->level              =   $request->level;
            $data->ttu_id             =   $request->ttu;
            $data->office_id          =   $request->office;
            $data->airlines_id        =   $request->airlines;
            $data->satellite_id       =   $request->satellite;
            $data->shift_id           =   $request->shift;
            $data->created_by         =   auth()->user()->id;
            $data->save();
            return back()->with('success', 'Successfully saved!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user_level = array( ['id' => 0, 'name' => 'Super Admin'],
        ['id' => 1, 'name' => 'Regular Admin'],
        ['id' => 2, 'name' => 'Processor'],
        ['id' => 3, 'name' => 'User'],
        ['id' => 4, 'name' => 'Travel Tax User'],
        ['id' => 5, 'name' => 'Kiosk']);

        $ttu       = FS_TTU::orderby('name')->get();
        $office    = FS_OFFICE::orderby('name')->get();
        $airlines  = Airlines::orderby('name')->get(); 
        $satellite = FS_SATELLITE::orderby('name')->get(); 
        $shift = FS_SHIFTS::orderby('name')->get(); 


        $users = User::find($id);
        $data = ['module' => 'application','val'=>$users,'user_level' => $user_level,'ttu'=>$ttu,'office'=>$office,'airlines'=>$airlines,'satellite'=>$satellite,'shift'=>$shift];
        return view('filesetup.user_account.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = User::find($id);
            $data->first_name         =   $request->first_name;;
            $data->middle_name        =   $request->middle_name;
            $data->last_name          =   $request->last_name;
            $data->username           =   $request->username;;
            $data->password           =   $request->password != $data->password ? bcrypt($request->password) : $data->password;
            $data->email              =   $request->email;
            $data->level              =   $request->level;
            $data->ttu_id             =   $request->ttu;
            $data->office_id          =   $request->office;
            $data->airlines_id        =   $request->airlines;
            $data->satellite_id       =   $request->satellite;
            $data->shift_id           =   $request->shift;
            $data->updated_by         =   auth()->user()->id;
            $data->save();
            return back()->with('success', 'Successfully saved!'); 

        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('failed', $th); 

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function load_user_account()
    {
        try
        {
            $data = array();
            $data = User::orderby('id','desc')->get();
            $datatables = Datatables::of($data)
            ->addColumn('name',function($data)
            {
                $column = $data->first_name.' '.$data->last_name;
                return $column;
            })
            ->addColumn('acct_level',function($data)
            {
                $level = "";
                if($data->level == "0") $level = "Super Admin";
                else if($data->level == "1") $level = "Regular Admin";
                else if($data->level == "2") $level = "Processor";
                else if($data->level == "3") $level = "User";
                else if($data->level == "4") $level = "Travel Tax User";
                else if($data->level == "5") $level = "Kiosk";

                return $level;
            })
            ->addColumn('action', function($data) {
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.route('user_account.show', $data->id).'" class="dropdown-item" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';
                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            });

            $rawColumns = ['action','username', 'name','email','acct_level'];

        }
        catch(Exception $e)
        {
            info($e->getMessage());
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
}
