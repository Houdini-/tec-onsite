<?php

namespace App\Http\Controllers\WebController\Filesetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users_settings;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;
use Session;    

class ORCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['module' => 'application'];
        return view('filesetup.certificate_series.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user  = User::get();
        $data = ['module' => 'application','users'=>$user];
        return view('filesetup.certificate_series.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chkExist = Users_settings::find($request->_user);
        if($chkExist){
            $chkExist->update([
                'start_or' => $request->or_start,
                'end_or' => $request->or_end,
                'or_no' => $request->or_designation,
                'start_tec' => $request->tec_start,
                'end_tec' => $request->tec_end,
                'tec_no' => $request->tec_designation,
                'start_rtt' => $request->rtt_start,
                'end_rtt' => $request->rtt_end,
                'rtt_no' => $request->rtt_designation,
            ]);
        }
        else{
            $sqlInsert = new Users_settings;
            $sqlInsert->user_id     = $request->_user;
            $sqlInsert->start_or    = $request->or_start;
            $sqlInsert->end_or      = $request->or_end;
            $sqlInsert->or_no       = $request->or_designation;
            $sqlInsert->start_tec   = $request->tec_start;
            $sqlInsert->end_tec     = $request->tec_end;
            $sqlInsert->tec_no      = $request->tec_designation;
            $sqlInsert->start_rtt   = $request->rtt_start;
            $sqlInsert->end_rtt     = $request->rtt_end;
            $sqlInsert->rtt_no      = $request->rtt_designation;
            if(!$sqlInsert->save())return back()->with('error', 'Saving Failed!');
        }
        return back()->with('success', 'Successfully saved!'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $val = Users_settings::find($id);
        $users = User::all();
        $data = ['module' => 'application','val'=>$val,'users'=>$users];
        return view('filesetup.certificate_series.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chkExist = Users_settings::find($id);
        if($chkExist){
            $chkExist->start_or    = $request->or_start;
            $chkExist->end_or      = $request->or_end;
            $chkExist->or_no       = $request->or_designation;
            $chkExist->start_tec   = $request->tec_start;
            $chkExist->end_tec     = $request->tec_end;
            $chkExist->tec_no      = $request->tec_designation;
            $chkExist->start_rtt   = $request->rtt_start;
            $chkExist->end_rtt     = $request->rtt_end;
            $chkExist->rtt_no      = $request->rtt_designation;
            if($chkExist->update())return back()->with('success', 'Successfully updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function load_fs_certificate()
    {
        try
        {
            $data = array();
            $data = Users_settings::with('user')->get();
            
            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) {
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.route('certificate.show', $data->id).'" class="dropdown-item" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';
                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            })
            ->addColumn('fullname', function($data) {
              return $data->user->first_name.' '.$data->user->last_name;
            });

            $rawColumns = ['action','fullname','or_no', 'tec_no','rtt_no'];

        }
        catch(Exception $e)
        {
            info($e->getMessage());
            return response(['errors' => $e->getMessage()], 201);
        }
       
        return $datatables->rawColumns($rawColumns)->make(true);
    }
    public function get_settings($id){
        $settings = Users_settings::find($id);

        return $settings;
    }
}
