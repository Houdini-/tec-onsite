<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TEC extends Model
{
    use HasFactory;
    protected $table = 'tec_application';
}
