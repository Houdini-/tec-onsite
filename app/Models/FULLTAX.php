<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FULLTAX extends Model
{
    use HasFactory;
    protected $table = 'fulltax_application';

}
