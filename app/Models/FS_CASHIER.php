<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FS_CASHIER extends Model
{
    use HasFactory;
    protected $table = 'fs_cashier';

}
