<?php

namespace App\Models;

use App\Http\Traits\System_Config;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $user_level = array(
		['id' => 0, 'name' => 'super_admin', 'level' => 0],
		['id' => 1, 'name' => 'regular_admin', 'level' => 1],
		['id' => 2, 'name' => 'processor', 'level' => 2],
		['id' => 3, 'name' => 'user', 'level' => 3],
		['id' => 4, 'name' => 'travel_tax_user', 'level' => 4],
		['id' => 5, 'name' => 'cashier', 'level' => 5],

	);

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function isSuperAdmin()
    {
        return Auth::user()->level == 0;
    }

    // Regular Admin or Supervisor Account

    public function isRegularAdmin()
    {
        return Auth::user()->level == 1;
    }

    public function isProcessor()
    {
        return Auth::user()->level == 2;
    }

    public function isUser()
    {
        return Auth::user()->level == 3;
    }

    public function isTravelTaxUser()
    {
        return Auth::user()->level == 4;
    }
    public function isCashier()
    {
        return Auth::user()->level == 5;
    }

    public function getFullName()
    {
        $user = Auth::user();

        return $user->last_name.', '.$user->first_name.' '.$user->middle_name[0].'.';
    }

    public function getFullNameFML()
    {
        $user = Auth::user();

        return $user->first_name.' '.$user->middle_name[0].'. '.$user->last_name;
    }

    public function getUserLevel()
    {
        $user = Auth::user();
        $user_level = $this->user_level;
        return $user_level[$user->level]['name'];
    }
}
