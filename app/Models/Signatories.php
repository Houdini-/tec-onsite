<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Signatories extends Model
{
    use HasFactory;
    protected $table = 'signatories';

    public function users() {
        return $this->belongsTo(User::class,'name','id');
    }
}
