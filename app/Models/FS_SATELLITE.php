<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FS_SATELLITE extends Model
{
    use HasFactory;
    protected $table = 'fs_satellite';

}
