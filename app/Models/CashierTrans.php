<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashierTrans extends Model
{
    use HasFactory;
    protected $table = 'cashier_transactions';

}
