<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TT_KIOSK extends Model
{
    use HasFactory;
    protected $table = 'traveltax_kiosk';

}
