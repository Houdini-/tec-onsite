<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Users_settings extends Model
{
    use HasFactory;
    protected $table = 'users_settings';

    public function cashier() {
        return $this->belongsTo(FS_CASHIER::class,'cashier_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'cashier_id','id');
    }
}
