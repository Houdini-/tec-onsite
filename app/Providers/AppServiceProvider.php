<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\FS_CASHIER;
use App\Models\User;
use App\Models\FS_SHIFTS;
use App\Models\FS_OFFICE;
use App\Models\Users_settings;
use Illuminate\Support\Facades\Auth;
use View; 
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
            if(Auth::check())
            {
                $cashier = User::where('level','5')->get();
                View::share('cashier', $cashier);
                $office = FS_OFFICE::get();
                View::share('office', $office);
                $shifts = FS_SHIFTS::get();
                View::share('shifts', $shifts);
                $user_settings = Users_settings::with('user')
                ->where('user_id',auth()->user()->id)->orderby('id','desc')->first();
                View::share('usettings',$user_settings);
            }
        });
      
    }
}
