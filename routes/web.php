<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebController\ApplicationController;
use App\Http\Controllers\WebController\RecordsController;
use App\Http\Controllers\WebController\HomeController;
use App\Http\Controllers\WebController\ReportsController;
use App\Http\Controllers\WebController\FileSetupController;
use App\Http\Controllers\WebController\DashboardController;
use App\Http\Controllers\WebController\CollectionController;
use App\Http\Controllers\WebController\KioskController;
use App\Http\Controllers\Auth\LoginController;


// Filesetup //
use App\Http\Controllers\WebController\Filesetup\TTUController;
use App\Http\Controllers\WebController\Filesetup\OfficeController;
use App\Http\Controllers\WebController\Filesetup\SatelliteController;
use App\Http\Controllers\WebController\Filesetup\SignatoriesController;
use App\Http\Controllers\WebController\Filesetup\UsersController;
use App\Http\Controllers\WebController\Filesetup\CashierController;
use App\Http\Controllers\WebController\Filesetup\ShiftsController;
use App\Http\Controllers\WebController\Filesetup\ProvincialController;
use App\Http\Controllers\WebController\Filesetup\OSSCOController;
use App\Http\Controllers\WebController\Filesetup\AirlinesController;
use App\Http\Controllers\WebController\Filesetup\ReferenceController;
use App\Http\Controllers\WebController\Filesetup\ORCertificateController;





// End of Filesetup //
use Illuminate\Support\Facades\View;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::resource('/kiosk', KioskController::class);
Route::get('/kiosk-sm',[KioskController::class,'show_singlemultiple'])->name('kiosk.sm');
Route::get('/kiosk-single',[KioskController::class,'show_single'])->name('kiosk.single');
Route::get('/kiosk-multiple',[KioskController::class,'show_multiple'])->name('kiosk.multiple');
Route::post('/kiosk-validate',[KioskController::class,'validation'])->name('kiosk.validate');
Route::post('/kiosk-multiple-validate',[KioskController::class,'multiple_validation'])->name('kiosk.multiple-validate');
Route::get('/kiosk-qr/{id}',[KioskController::class,'show_qrcode'])->name('kiosk.show_qr');
Route::post('/kiosk-store',[KioskController::class,'store_multiple'])->name('kiosk.store_multiple');
Route::get('/kiosk-multiple-display/{id}',[KioskController::class,'display_multiple'])->name('kiosk.multiple-display');

Route::any('/tec_application/json', 'App\Http\Controllers\ApiController\ApplicationController@filter_tec_application_by');



Route::get('/', function () {
    return view('auth.login');
});   

// Route::get('/',[LoginController::class,'login_mode']);


Auth::routes();

// Route::middleware(['isSuperAdmin'])->group(function () {
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::resource('/application', ApplicationController::class);
    Route::resource('/records', RecordsController::class);
    Route::resource('/home', HomeController::class);
    Route::resource('/reports', ReportsController::class);
    Route::resource('/filesetup', FileSetupController::class);
    Route::resource('/traveltaxunit', TTUController::class);
    Route::resource('/office_location', OfficeController::class);
    Route::resource('/satellite', SatelliteController::class);
    Route::resource('/signatories', SignatoriesController::class);
    Route::resource('/user_account', UsersController::class);
    Route::resource('/cashier', CashierController::class);
    Route::resource('/shifts', ShiftsController::class);
    Route::resource('/provincial', ProvincialController::class);
    Route::resource('/ossco', OSSCOController::class);
    Route::resource('/dashboard', DashboardController::class);
    Route::resource('/collection', CollectionController::class);
    Route::resource('/airlines', AirlinesController::class);
    Route::resource('/reference', ReferenceController::class);
    Route::resource('/certificate', ORCertificateController::class);











    Route::post('/print', [ReportsController::class,'print_application'])->name('reports.print');
    Route::get('/get_section/{id}', [ApplicationController::class,'getSectionDesc'])->name('getSD');
    Route::get('/view_application/{type}/{id}', [ApplicationController::class,'view_application'])->name('view_application');
    Route::get('/confirmation/{id}', [ApplicationController::class,'confirmation'])->name('confirmation_');
    Route::get('/payment/{id}/{cashier}', [ApplicationController::class,'payment_details'])->name('payment_');
    Route::post('/save_rtt', [ApplicationController::class,'save_rtt'])->name('rtt_save');
    Route::post('/save_fulltax', [ApplicationController::class,'save_fulltax'])->name('fulltax_save');
    Route::post('/save_usersettings', [ApplicationController::class,'save_usersettings'])->name('usettings_save');
    Route::get('/get_osreference', [ApplicationController::class,'getOnsiteReference']);
    Route::get('/get_sectwhere/{type}', [ApplicationController::class,'sectionWhere']);
    Route::get('/getrtt_amt/{section}/{class}', [ApplicationController::class,'getrtt_amt']);
    Route::get('/generate_cert/{id}/{type}', [ApplicationController::class,'generate_cert'])->name('generate_cert');
    Route::get('/load_traveltax', [TTUController::class,'load_traveltaxunit'])->name('load_ttu');
    Route::get('/load_office', [OfficeController::class,'load_office'])->name('load_office');
    Route::get('/load_satellite', [SatelliteController::class,'load_satellite'])->name('load_satellite');
    Route::get('/load_signatories', [SignatoriesController::class,'load_signatories'])->name('load_signatories');
    Route::get('/load_user_account', [UsersController::class,'load_user_account'])->name('load_user_account');
    Route::get('/load_cashier', [CashierController::class,'load_cashier'])->name('load_cashier');
    Route::get('/load_shifts', [ShiftsController::class,'load_shifts'])->name('load_shifts');
    Route::get('/load_provincial', [ProvincialController::class,'load_provincial'])->name('load_provincial');
    Route::get('/load_ossco', [OSSCOController::class,'load_ossco'])->name('load_ossco');
    Route::get('/load_airlines', [AirlinesController::class,'load_airlines'])->name('load_airlines');
    Route::get('/load_reference', [ReferenceController::class,'load_reference'])->name('load_reference');
    Route::get('/load_fs_certificate', [ORCertificateController::class,'load_fs_certificate'])->name('load_fs_certificate');
    Route::get('/get_settings/{id}', [ORCertificateController::class,'get_settings']);

    Route::get('/get_rtt_sections_by/{id}', [ApplicationController::class,'get_rtt_sections_by'])->name('get_rtt_sections_by');
    Route::get('/get_usettings', [HomeController::class,'get_usettings'])->name('get_usettings');
    Route::get('/get_kioskdetails/{id}', [KioskController::class,'get_kioskdetails'])->name('get_kioskdetails');
    Route::get('/onsite_print_or/{ref}', [ApplicationController::class,'onsite_print_or'])->name('onsite_print_or');
    Route::post('/save_payment_or', [ApplicationController::class,'save_payment_or'])->name('save_payment_or');


});

Route::get('/qr_code_ft/{id}/{file}', [ function ($id,$file) {
	$folder_path = storage_path('tieza/fulltax/qr-code/'.$id.'/'.$file);

	$explode_file = explode('.', $file);

	if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	{
	   	$content = 'image/jpeg';
	}
	elseif(in_array($explode_file[1], ['pdf']))
	{
	   	$content = 'application/pdf';
	}

	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	abort(404);
}]);
// Route::middleware(['isRegularAdmin'])->prefix('supervisor')->group(function () {

//     Route::any('/home', [HomeController::class, 'index'])->name('home');
//     Route::resource('/application', ApplicationController::class);
//     Route::resource('/records', RecordsController::class);
//     Route::resource('/home', HomeController::class);
//     Route::resource('/reports', ReportsController::class);
//     Route::resource('/filesetup', FileSetupController::class);
//     Route::resource('/traveltaxunit', TTUController::class);
//     Route::resource('/office_location', OfficeController::class);
//     Route::resource('/satellite', SatelliteController::class);
//     Route::resource('/signatories', SignatoriesController::class);
//     Route::resource('/user_account', UsersController::class);

//     Route::post('/print', [ReportsController::class,'print_application'])->name('reports.print');
//     Route::get('/get_section/{id}', [ApplicationController::class,'getSectionDesc'])->name('getSD');
//     Route::get('/view_application/{type}/{id}', [ApplicationController::class,'view_application'])->name('view_application');
//     Route::post('/save_rtt', [ApplicationController::class,'save_rtt'])->name('rtt_save');
//     Route::post('/save_fulltax', [ApplicationController::class,'save_fulltax'])->name('fulltax_save');
//     Route::get('/generate_cert/{id}', [ApplicationController::class,'generate_cert'])->name('generate_cert');
//     Route::get('/load_traveltax', [TTUController::class,'load_traveltaxunit'])->name('load_ttu');
//     Route::get('/load_office', [OfficeController::class,'load_office'])->name('load_office');
//     Route::get('/load_satellite', [SatelliteController::class,'load_satellite'])->name('load_satellite');
//     Route::get('/load_signatories', [SignatoriesController::class,'load_signatories'])->name('load_signatories');
//     Route::get('/load_user_account', [UsersController::class,'load_user_account'])->name('load_user_account');

// });

// Route::middleware(['isUser'])->prefix('user')->group(function () {
//     Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');   
// });