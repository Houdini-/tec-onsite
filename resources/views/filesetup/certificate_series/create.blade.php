@extends('filesetup.index')
@section('fs-content')
<h4><b> OR/Certificate</b></h4>
<hr></hr>
<form action="{{route('certificate.store')}}" method="post">
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Employee Name</label>
            <select name="_user" id="_user" class="form-control form-control-sm" tabindex="10">
                @foreach ($users as $item)
                    <option value="{{$item->id}}" @if(auth()->user()->id == $item->id) selected @endif>{{$item->first_name.' '.$item->last_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <h5> Official Receipt</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="or_start" id="or_start" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="or_end" id="or_end" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="or_designation" id="or_designation" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <hr></hr>
    <h5> Exemption Certificate</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="tec_start" id="tec_start" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="tec_end" id="tec_end" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="tec_designation" id="tec_designation" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <h5> Reduced Certificate</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="rtt_start" id="rtt_start" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="rtt_end" id="rtt_end" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="rtt_designation" id="rtt_designation" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#certificate').addClass('active text-white');
    $('#_user').change(function()
    {
      
        var id = $(this).val();
        $.ajax({
            url:"/get_settings/"+id,
            type:"get",
            success:function(response)
            {
               $('#or_designation').val(response.or_no);
               $('#tec_designation').val(response.tec_no);
               $('#rtt_designation').val(response.rtt_no);

            },
            error:function(response){
                
            }
        })
    })
    $('#_user').change();
})
</script>
@endsection