@extends('filesetup.index')
@section('fs-content')
@include('layouts.auth-partials.datatables-css')
<div class="row py-2">
   <div class="col-12">
     <a href="{{route('certificate.create')}}" class="btn btn-primary text-white"><i class="mdi mdi-plus"></i> ADD</a>
     <table class="table table-striped bg-white table-hover table-fw-widget" id="tbl">
            <thead>
                <tr>
                    <td> </td>
                    <td width="25%">User</td>
                    <td width="">OR #</td>
                    <td width="">TEC #</td>
                    <td width="">RTT #</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
    </div>
</div>

@endsection

@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script type="text/javascript"> 
    $(document).ready(function () {
        
        App.init();
        App.dataTables();
        loadDatatable();
        $('#certificate').addClass('active text-white');
    });

    function loadDatatable()
    {
        
        @php
        $columns = array(['data' => 'action'],['data' => 'fullname'],['data' => 'or_no'],['data' => 'tec_no'],['data' => 'rtt_no']);
        @endphp
        var url = '{{ route("load_fs_certificate") }}';
        load_datables('#tbl', url ,{!! json_encode($columns) !!}, null);

    }
</script>
@endsection