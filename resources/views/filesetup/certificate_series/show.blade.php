@extends('filesetup.index')
@section('fs-content')
<h4><b> OR/Certificate</b></h4>
<form action="{{route('certificate.update',$val->id)}}" method="post">
    @method('put')
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="button" class="btn btn-primary btn-md" id="btn_edit"> <i class="mdi mdi-edit"></i> Edit</button>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Employee Name</label>
            <select name="_user" id="_user" class="form-control form-control-sm" tabindex="10" disabled>
                @foreach ($users as $item)
                    <option value="{{$item->id}}" @if($val->user_id == $item->id) selected @endif>{{$item->first_name.' '.$item->last_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <h5> Official Receipt</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="or_start" id="or_start" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->start_or}}">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="or_end" id="or_end" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->end_or}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="or_designation" id="or_designation" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->or_no}}">
        </div>
    </div>
    <hr></hr>
    <h5> Exemption Certificate</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="tec_start" id="tec_start" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->start_tec}}">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="tec_end" id="tec_end" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->end_tec}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="tec_designation" id="tec_designation" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->tec_no}}">
        </div>
    </div>
    <h5> Reduced Certificate</h5>
    <div class="row py-2">
        <div class="col-12 col-md-4">
            <label for="code" >Start</label>
            <input type="text" name="rtt_start" id="rtt_start" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->start_rtt}}">
        </div>
        <div class="col-12 col-md-4">
            <label for="code" >End</label>
            <input type="text" name="rtt_end" id="rtt_end" class="form-control form-control-sm" placeholder="Enter Code.."value="{{$val->end_rtt}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Designation</label>
            <input type="text" name="rtt_designation" id="rtt_designation" class="form-control form-control-sm" placeholder="Enter Code.." value="{{$val->rtt_no}}">
        </div>
    </div>
    <div class="row btn-action py-2 d-none">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" id="btn_cancel" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function()
{
    $('#certificate').addClass('active text-white');
    $(':input').prop('readonly',true);
    $('#btn_edit').click(function()
    {
        $(':input').prop('readonly',false);
        $('.btn-action').removeClass('d-none');
        $('#btn_edit').hide();
    });
    $('#btn_cancel').click(function()
    {
        location.reload();

    });
    // $('#_user').change(function()
    // {
      
    //     var id = $(this).val();
    //     $.ajax({
    //         url:"/get_settings/"+id,
    //         type:"get",
    //         success:function(response)
    //         {
    //            $('#or_designation').val(response.or_no);
    //            $('#tec_designation').val(response.tec_no);
    //            $('#rtt_designation').val(response.rtt_no);

    //         },
    //         error:function(response){
                
    //         }
    //     })
    // })
    // $('#_user').change();
});
</script>
@endsection
