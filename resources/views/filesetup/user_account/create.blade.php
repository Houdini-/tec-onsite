@extends('filesetup.index')
@section('fs-content')
<h4><b> User Account</b></h4>
<form action="{{route('user_account.store')}}" method="post">
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Firstname</label>
            <input type="text" name="first_name" id="first_name" class="form-control form-control-sm" placeholder="Enter Firstname..">
        </div>
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Middlename</label>
            <input type="text" name="middle_name" id="middle_name" class="form-control form-control-sm" placeholder="Enter Middlename..">
        </div>
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Lastname</label>
            <input type="text" name="last_name" id="last_name" class="form-control form-control-sm" placeholder="Enter Lastname..">
        </div>
    </div>
  
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Username</label>
            <input type="text" name="username" id="username" class="form-control form-control-sm" placeholder="Enter Username..">
        </div>
        <div class="col-12 col-md-6">
            <label for="code" >Password</label>
            <input type="password" name="password" id="password" class="form-control form-control-sm" placeholder="Enter Password..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Email Address</label>
            <input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="Enter Email Address..">
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Level</label>
              <select class="form-control form-control-sm" name="level" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($user_level as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code">Office/Location</label>
              <select class="form-control form-control-sm" name="office" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($office as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Shift</label>
              <select class="form-control form-control-sm" name="shift" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($shift as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code">Travel Tax Unit</label>
              <select class="form-control form-control-sm" name="ttu" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($ttu as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2 d-none">
        <div class="col-12 col-md-6">
            <label for="code">Airport</label>
              <select class="form-control form-control-sm" name="airlines" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($airlines as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Satellite</label>
              <select class="form-control form-control-sm" name="satellite" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($satellite as $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#user_account').addClass('active text-white');
})
</script>
@endsection