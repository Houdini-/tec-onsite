@extends('filesetup.index')
@section('fs-content')
<h4><b> User Account</b></h4>
<form action="{{route('user_account.update',$val->id)}}" method="post">
    @method('put')
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="button" class="btn btn-primary btn-md" id="btn_edit"> <i class="mdi mdi-edit"></i> Edit</button>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Firstname</label>
            <input type="text" name="first_name" id="first_name" class="form-control form-control-sm" placeholder="Enter Firstname.." value="{{$val->first_name}}">
        </div>
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Middlename</label>
            <input type="text" name="middle_name" id="middle_name" class="form-control form-control-sm" placeholder="Enter Middlename.." value="{{$val->middle_name}}">
        </div>
        <div class="col-12 col-md-4 py-2">
            <label for="code" >Lastname</label>
            <input type="text" name="last_name" id="last_name" class="form-control form-control-sm" placeholder="Enter Lastname.." value="{{$val->last_name}}">
        </div>
    </div>
  
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Username</label>
            <input type="text" name="username" id="username" class="form-control form-control-sm" placeholder="Enter Username.." value="{{$val->username}}">
        </div>
        <div class="col-12 col-md-6">
            <label for="code" >Password</label>
            <input type="password" name="password" id="password" class="form-control form-control-sm" placeholder="Enter Password.." value="{{$val->password}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Email Address</label>
            <input type="email" name="email" id="email" class="form-control form-control-sm" placeholder="Enter Email Address.." value="{{$val->email}}">
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Level</label>
              <select class="form-control form-control-sm" name="level" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($user_level as $item)
                    <option value="{{$item['id']}}" @if($item['id'] == $val->level) selected @endif >{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code">Office/Location</label>
              <select class="form-control form-control-sm" name="office" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($office as $item)
                    <option value="{{$item['id']}}"  @if($item['id'] == $val->office_id) selected @endif>{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Shift</label>
              <select class="form-control form-control-sm" name="shift" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($shift as $item)
                    <option value="{{$item['id']}}"  @if($item['id'] == $val->shift_id) selected @endif>{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code">Travel Tax Unit</label>
              <select class="form-control form-control-sm" name="ttu" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($ttu as $item)
                    <option value="{{$item['id']}}"  @if($item['id'] == $val->ttu_id) selected @endif>{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2 d-none">
        <div class="col-12 col-md-6">
            <label for="code">Airport</label>
              <select class="form-control form-control-sm" name="airlines" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($airlines as $item)
                    <option value="{{$item['id']}}"@if($item['id'] == $val->airlines_id) selected @endif >{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
        <div class="col-12 col-md-6">
            <label for="code">Satellite</label>
              <select class="form-control form-control-sm" name="satellite" id="level">
                <option selected disabled>Please select option</option>
                @foreach ($satellite as $item)
                    <option value="{{$item['id']}}"@if($item['id'] == $val->satellite_id) selected @endif >{{$item['name']}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2 btn-action d-none">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function()
{
    $('#user_account').addClass('active text-white');
    $(':input').prop('readonly',true);
    $('select').prop('disabled',true);

    $('#btn_edit').click(function()
    {
        $(':input').prop('readonly',false);
        $('select').prop('disabled',false); 
        $('.btn-action').removeClass('d-none');
        $('#btn_edit').hide();
    });
    $('#btn_cancel').click(function()
    {
        location.reload();
    });
});
</script>
@endsection
