@extends('layouts.master-auth')
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
    <div class="card-header card-header-contrast">File Setup</div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-lg-12 col-xl-3 col-md-12 col-xs-12 col-sm-12">
            <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="airlines" href="{{ route('airlines.index')}}">
                        <span class="text-primary mdi mdi-globe icon"></span>
                        <span class="text">Airlines</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="ttu" href="{{ route('traveltaxunit.index')}}">
                        <span class="text-primary mdi mdi-globe icon"></span>
                        <span class="text">Travel Tax Unit</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="office" href="{{ route('office_location.index')}}">
                        <span class="text-primary mdi mdi-my-location icon"></span>
                        <span class="text">Office / Location</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="satellite"  href="{{ route('satellite.index')}}">
                        <span class="text-primary mdi mdi-satellite icon"></span>
                        <span class="text">Satellite</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="provincial" href="{{ route('provincial.index')}}">
                        <span class="text-primary mdi mdi-home icon"></span>
                        <span class="text">Provincial</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="ossco" href="{{ route('ossco.index')}}">
                        <span class="text-primary mdi mdi-assignment icon"></span>
                        <span class="text">OSSCO</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="signatories" href="{{ route('signatories.index')}}">
                        <span class="text-primary mdi mdi-assignment icon"></span>
                        <span class="text">Signatories</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="user_account" href="{{ route('user_account.index')}}">
                        <span class="text-primary mdi mdi-accounts icon"></span>
                        <span class="text">User Account</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="cashier" href="{{ route('cashier.index')}}">
                        <span class="text-primary mdi mdi-accounts-outline icon"></span>
                        <span class="text">Cashier</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="shifts" href="{{ route('shifts.index')}}">
                        <span class="text-primary mdi mdi-assignment icon"></span>
                        <span class="text">Shifts</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="reference" href="{{ route('reference.index')}}">
                        <span class="text-primary mdi mdi-assignment icon"></span>
                        <span class="text">Reference Code</span>
                    </a>
                </div>
                <div class="list-group d-flex">
                    <a class="list-group-item d-flex list-group-item-action" id="certificate" href="{{ route('certificate.index')}}">
                        <span class="text-primary mdi mdi-assignment icon"></span>
                        <span class="text">OR/Certificate</span>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-12 col-xl-9 col-md-12 col-xs-12 col-sm-12">   
                 @yield('fs-content')
            </div>
        </div>
    </div>
</div>

@endsection


@section('scripts')
<script type="text/javascript"> 
    $(document).ready(function () {
      
        App.formElements();

       
    });
</script>
@endsection