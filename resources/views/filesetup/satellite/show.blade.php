@extends('filesetup.index')
@section('fs-content')
<h4><b> Satellite</b></h4>
<form action="{{route('satellite.update',$val->id)}}" method="post">
    @method('put')
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="button" class="btn btn-primary btn-md" id="btn_edit"> <i class="mdi mdi-edit"></i> Edit</button>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Code</label>
            <input type="text" name="code" id="code" class="form-control form-control-sm" value="{{$val->code}}" >
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control form-control-sm" value="{{$val->name}}" >
        </div>
    </div>
    <div class="row btn-action py-2 d-none">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" id="btn_cancel" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function()
{
    $('#satellite').addClass('active text-white');
    $(':input').prop('readonly',true);
    $('#btn_edit').click(function()
    {
        $(':input').prop('readonly',false);
        $('.btn-action').removeClass('d-none');
        $('#btn_edit').hide();
    });
    $('#btn_cancel').click(function()
    {
        location.reload();

    });
});
</script>
@endsection
