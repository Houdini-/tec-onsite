@extends('filesetup.index')
@section('fs-content')
<h4><b> Shifts </b></h4>
<form action="{{route('shifts.store')}}" method="post">
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Reference Code</label>
            <input type="text" name="code" id="code" class="form-control form-control-sm" placeholder="Enter Reference Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">From Time</label>
            <input type="text" id="from" name="from" class="form-control form-control-sm datetimepicker"  data-show-meridian="true" data-start-view="1" data-date="0000-00-00T05:25:07Z" data-date-format="hh:ii" data-link-field="dtp_input1">

        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">To Time</label>
            <input type="text" id="to" name="to" class="form-control form-control-sm datetimepicker"  data-show-meridian="true" data-start-view="1" data-date="0000-00-00T05:25:07Z" data-date-format="hh:ii" data-link-field="dtp_input1">

        </div>
    </div>

    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    App.formElements();
   
    $('#shifts').addClass('active text-white');
    // $('#trans_date').change(function()
    // {
    //     alert($(this).val());
    // });
})
</script>
@endsection