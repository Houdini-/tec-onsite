@extends('filesetup.index')
@section('fs-content')
@include('layouts.auth-partials.datatables-css')
<div class="row py-2">
   <div class="col-12">
     <a href="{{route('shifts.create')}}" class="btn btn-primary text-white"><i class="mdi mdi-plus"></i> ADD</a>
     <table class="table table-striped bg-white table-hover table-fw-widget" id="tbl">
            <thead>
                <tr>
                    <td> </td>
                    <td width="20%">Reference Code</td>
                    <td width="40%">Name</td>
                    <td width="20%">From</td>
                    <td width="20%">To</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
    </div>
</div>

@endsection

@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script type="text/javascript"> 
    $(document).ready(function () {
        
        App.init();
        App.dataTables();
        loadDatatable();
        $('#shifts').addClass('active text-white');
    });

    function loadDatatable()
    {
        
        @php
        $columns = array(['data' => 'action'],['data' =>'code'],['data' => 'name'],['data' => 'from'],['data' => 'to']);
        @endphp
        var url = '{{ route("load_shifts") }}';
        load_datables('#tbl', url ,{!! json_encode($columns) !!}, null);

    }
</script>
@endsection