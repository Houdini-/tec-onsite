@extends('filesetup.index')
@section('fs-content')
<h4><b> Shifts</b></h4>
<form action="{{route('shifts.update',$val->id)}}" method="post">
    @method('put')
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="button" class="btn btn-primary btn-md" id="btn_edit"> <i class="mdi mdi-edit"></i> Edit</button>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Reference Code</label>
            <input type="text" name="code" id="code" class="form-control form-control-sm" placeholder="Enter Reference Code.." value="{{$val->code}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Enter Name.." value="{{$val->name}}">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">From Time</label>
            <input type="text" id="from" name="from" class="form-control form-control-sm datetimepicker"  data-show-meridian="true" data-start-view="1" data-date="0000-00-00T05:25:07Z" data-date-format="hh:ii" data-link-field="dtp_input1"  value="{{$val->from}}">

        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">To Time</label>
            <input type="text" id="to" name="to" class="form-control form-control-sm datetimepicker"  data-show-meridian="true" data-start-view="1" data-date="0000-00-00T05:25:07Z" data-date-format="hh:ii" data-link-field="dtp_input1" value="{{$val->to}}">

        </div>
    </div>
    <div class="row btn-action py-2 d-none">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" id="btn_cancel" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function()
{
    App.formElements();
    $('#shifts').addClass('active text-white');
    $(':input').prop('readonly',true);
    $('#btn_edit').click(function()
    {
        $(':input').prop('readonly',false);
        $('.btn-action').removeClass('d-none');
        $('#btn_edit').hide();
    });
    $('#btn_cancel').click(function()
    {
        location.reload();

    });
});
</script>
@endsection
