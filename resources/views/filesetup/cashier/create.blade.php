@extends('filesetup.index')
@section('fs-content')
<h4><b> Cashier</b></h4>
<form action="{{route('cashier.store')}}" method="post">
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Code/Username</label>
            <input type="text" name="code" id="code" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Password</label>
            <input type="password" name="password" id="password" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="last_name">Lastname</label>
            <input type="text" name="last_name" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="first_name">Firstname</label>
            <input type="text" name="first_name" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="middle_name">Middlename</label>
            <input type="text" name="middle_name" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    {{-- <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="position">Position</label>
            <input type="text" name="position" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div> --}}
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#cashier').addClass('active text-white');
})
</script>
@endsection