@extends('filesetup.index')
@section('fs-content')
<h4><b> Airlines</b></h4>
<form action="{{route('airlines.store')}}" method="post">
    @csrf
    <!-- <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Code</label>
            <input type="text" name="code" id="code" class="form-control form-control-sm" placeholder="Enter Code..">
        </div>
    </div> -->
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#airlines').addClass('active text-white');
})
</script>
@endsection