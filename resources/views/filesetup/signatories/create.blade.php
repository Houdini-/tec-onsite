@extends('filesetup.index')
@section('fs-content')
<h4><b> Signatories</b></h4>
<form action="{{route('signatories.store')}}" method="post">
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Name</label>
              <select class="form-control form-control-sm" name="name" id="">
                <option selected disabled>Please select option</option>
                @foreach ($users as $item)
                    <option value="{{$item->id}}">{{$item->first_name.' '.$item->last_name}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="name">Position</label>
            <input type="text" name="position" id="position" class="form-control form-control-sm" placeholder="Enter Name..">
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>


@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#signatories').addClass('active text-white');
})
</script>
@endsection