@extends('filesetup.index')
@section('fs-content')
<h4><b> Satellite</b></h4>
<form action="{{route('signatories.update',$val->id)}}" method="post">
    @method('put')
    @csrf
    <div class="row py-2">
        <div class="col-12 col-md-6">
        <button type="button" class="btn btn-primary btn-md" id="btn_edit"> <i class="mdi mdi-edit"></i> Edit</button>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="code" >Name</label>
              <select class="form-control form-control-sm" name="name" id="">
                <option selected disabled>Please select option</option>
                @foreach ($users as $item)
                    <option value="{{$item->id}}"  @if($val->name == $item->id) selected @endif>{{$item->first_name.' '.$item->last_name}}</option>
                @endforeach
              </select>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-12 col-md-6">
            <label for="position">Position</label>
            <input type="text" name="position" id="position" class="form-control form-control-sm" value="{{$val->position}}" >
        </div>
    </div>
    <div class="row btn-action py-2 d-none">
        <div class="col-12 col-md-6">
        <button type="submit" class="btn btn-primary btn-md">Save</button>
        <button type="button" id="btn_cancel" class="btn btn-danger btn-md">Cancel</button>
        </div>
    </div>
</form>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function()
{
    $('#signatories').addClass('active text-white');
    $(':input').prop('readonly',true);
    $('select').prop('disabled',true);

    $('#btn_edit').click(function()
    {
        $(':input').prop('readonly',false);
        $('select').prop('disabled',false);
        $('.btn-action').removeClass('d-none');
        $('#btn_edit').hide();
    });
    $('#btn_cancel').click(function()
    {
        location.reload();
    });
});
</script>
@endsection
