<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
     <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family:    ;
            font-size: 10pt;
            background: white;
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            vertical-align: top;
        }
        td {
            padding: 0px;
            font-size: 9pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 150px;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: none;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }
          .doubleUnderline {
            text-decoration:underline;
            border-bottom: 1px solid #000;
          }


        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: block;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
    </style>
</head>
<body>
    <div class="page-header" style="text-align: center;">
        <img src="{{ asset('img/logo_arial2.png') }}" width="60%"/>
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer_2021.png') }}" width="100%">
    </div>

    <div>
        <div class="container-fluid">
            <br><br>
            <div class="col-md-12">
                <div class="page">
                    <div class="page-header-space">
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="font-size: 20pt;">
                            <b>SUMMARY OF COLLECTION AND DEPOSITS</b> 
                        </div>
                        {{-- @if(isset($month) && isset($year))
                        <div class="col-md-12 text-center" style="font-size: 12pt;">
                            <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                            <br>
                            <br>
                        </div>
                        @else
                        <div class="col-md-12 text-center" style="font-size: 12pt;">
                            <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                            <br>
                            <br>
                        </div>
                        @endif --}}
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            November, 2022
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col-md-2 offset-md-3 border-1 border-bottom">
                            PESO
                        </div>
                        &emsp;
                        <div class="col-md-2 border-1 border-bottom" >
                            DOLLAR
                        </div>
                    </div>
                  
                    <div class="row  py-1">
                        <div class="col-md-3 ">
                            Beginning Balance  
                        </div>
                        <div class="col-md-2 text-right " >
                            827,900.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right " >
                            33.00
                        </div>
                    </div>
                    <div class="row  py-1">
                        <div class="col-md-3 ">
                            Total Collection for the Day  
                        </div>
                        <div class="col-md-2 text-right  " >
                            139,300.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right " >
                            0.00
                        </div>
                    </div>
                    <div class="row  py-1">
                        <div class="col-md-3 ">
                            Miscellaneous Income 
                        </div>
                        <div class="col-md-2 text-right " >
                            0.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right " >
                            0.00
                        </div>
                    </div>
                    <div class="row  py-1">
                        <div class="col-md-3 ">
                            Payment of Shortage/s
                        </div>
                        <div class="col-md-2 text-right border-1 border-bottom" >
                            0.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right  border-1 border-bottom" >
                            0.00
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-md-2">
                            TOTAL
                        </div>
                        <div class="col-md-2 text-right offset-md-5">
                            969,000.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right">
                            969,000.00
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            TOTAL DEPOSITS
                        </div>
                        <div class="col-md-2 text-right offset-md-5 border-1 border-bottom">
                            832,940.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right border-1 border-bottom">
                            0.00
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            BALANCE END
                        </div>
                        <div class="col-md-2 text-right offset-md-5 border-1 border-bottom">
                            136,060.00
                        </div>
                        &emsp;
                        <div class="col-md-2 text-right border-1 border-bottom">
                            33.00
                        </div>
                    </div>
                    <br>
                    <div class="row ">
                        <div class="col-md-12 text-center">
                            <h3 >CERTIFICATION</h3>
                            <br>
                            <h5>I hereby certify on my official oath that the above is true and correct statement <br>
                            of collections and deposits at the close business on <u>November 2, 2021</u></h5>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-5">
                           <h4> Prepared By:</h4>
                        </div>
                        <div class="col-5 offset-1">
                         <h4>Noted By:</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
