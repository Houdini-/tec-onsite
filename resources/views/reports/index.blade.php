@extends('layouts.master-auth')
@section('content')
<form action="{{route('reports.print')}}" method="post">
    @csrf
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
    <div class="card-header card-header-contrast">Reports</div>
    <div class="card-body">
        <form action="{{route('reports.print')}}" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <label for="rpt_name">Report Name</label>
                <select class="select2 select2-xs @error('rpt_name') is-invalid @endif" 
                    name="rpt_name"
                    id="rpt_name">   
                    <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                    <option value="ft_payment">FULL TRAVEL TAX PAYMENT</option>
                    <option value="tec_granted">TRAVEL TAX EXEMPTION APPLICANTS GRANTED</option>
                    <option value="rtt_granted">REDUCED TRAVEL TAX APPLICANTS GRANTED</option>
                    <option value="rep_coll_dep">REPORT OF COLLECTION AND DEPOSITS</option>
                    <option value="summ_cash_coll_dep">SUMMARY OF CASH COLLECTION AND DEPOSITS</option>
                    <option value="summ_coll_dep">SUMMARY OF COLLECTION AND DEPOSITS</option>
                    <option value="deposits">DEPOSITS</option>
                    <option value="daily_collection">DAILY COLLECTION REPORT</option>
                    <option value="or_used">OFFICIAL RECEIPT ISSUED/USED</option>
                    <option value="summ_cash_dep">SUMMARY OF CASH DEPOSITS</option>
                    <option value="monthly_time_motion">MONTHLY TIME AND MOTION</option>
                    <option value="tec_used">TRAVEL TAX EXEMPTION CERTIFICATE ISSUED/USED</option>
                    <option value="rtt_used">REDUCE TRAVEL TAX CERTIFICATE ISSUED/USED</option>
                    <option value="rtt_payment">PAYMENT/REDUCED TRAVEL TAX</option>








                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <label for="rpt_name">Month</label>
                    <select class="select2 select2-xs criteria" 
                            name="month" 
                            id="month">   
                            <option value="">{{ __('page.please_select') }}</option>
                            <option value="01">January</option>
                            <option value="02">February</option>
                            <option value="03">March</option>
                            <option value="04">April</option>
                            <option value="05">May</option>
                            <option value="06">June</option>
                            <option value="07">July</option>
                            <option value="08">August</option>
                            <option value="09">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                    </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <div class="form-group" id="frm_input_year">
                    <label class="control-label">{{ __('page.year')}}</label>
                    <select class="select2 select2-xs criteria" 
                            name="year" 
                            id="year">   
                            <option value="">{{ __('page.please_select') }}</option>
                            @for($i = 2020; $i <= 2025; $i++)
                                <option valae="{{ $i }}">{{ $i }}</option>
                            @endfor
                    </select>
                    <div class="select_error" id="error-year"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <div class="form-group">
                    <label class="control-label">Start Date</label>
                    <input type="date" class="form-control form-control-xs criteria" id="start_date" name="start_date">
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                <div class="form-group">
                    <label class="control-label">End Date</label>
                    <input type="date" class="form-control form-control-xs criteria" id="end_date" name="end_date">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                <button type="submit" class="btn btn-primary " id="preview"><i class="mdi mdi-eye"></i> Preview</button>
           </div>
       </div>
        </form>
    </div>
</div>
</form>

@endsection


@section('scripts')
<script type="text/javascript"> 

    $(document).ready(function () {
      
        App.formElements();
    });

    $('.custom-control-input').change(function()
    {
    
    });
 
    $('#preview').click(function()
    {
       





    });
  
</script>
@endsection