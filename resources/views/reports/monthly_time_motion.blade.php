<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
     <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family: courier;
            font-size: 10pt;
            background: white;
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            vertical-align: top;
        }
        td {
            padding: 0px;
            font-size: 9pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 150px;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: none;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }
          .doubleUnderline {
            text-decoration:underline;
            border-bottom: 1px solid #000;
          }


        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: block;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
    </style>
</head>
<body>
    <div class="page-header" style="text-align: center;">
        <img src="{{ asset('img/logo_arial2.png') }}" width="60%"/>
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer_2021.png') }}" width="100%">
    </div>

    <div>
        <div class="container-fluid">
            <br><br>
            <div class="col-md-12">
                <div class="page">
                    <div class="page-header-space">
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="font-size: 20pt;">
                            <b>MONTHLY SUMMARY REPORT</b> 
                            <br>
                        </div>
                       
                    </div>
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th colspan='20'> <center>MONTHLY TIME AND MOTION(EXEMPTION SECTION)</center></th>
                                </tr>
                                <tr>
                                    <th colspan='20'> <center>PROCESSOR</center></th>
                                </tr>
                                <tr>
                               
                                    <th>DATE</th>
                                    <th>QTY</th>
                                    <th>TOTAL TIME</th>
                                    <th>AVERAGE TIME</th>
                        
                                </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>

                        </div>
                        <div class="col-md-3"></div>

                    </div>

                    <br><br><br>
                    <div class="row">
                        <div class="col-5">
                           <h4> Prepared By:</h4>
                        </div>
                        <div class="col-5 offset-1">
                         <h4>Noted By:</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
