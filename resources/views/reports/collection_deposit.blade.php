<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
     <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family: arial;
            font-size: 13pt;
            background: white;
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            vertical-align: top;
        }
        td {
            padding: 0px;
            font-size: 9pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 150px;
          }

          .page-footer, .page-footer-space {
          }

          .page-header { 
            display: none;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }
          .doubleUnderline {
            text-decoration:underline;
            border-bottom: 1px solid #000;
          }


        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: block;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
    </style>
</head>
<body>
    <div class="page-header" style="text-align: center;">
        <img src="{{ asset('img/logo_arial2.png') }}" width="60%"/>
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer_2021.png') }}" width="100%">
    </div>

    <div>
        <div class="container-fluid">
            <br><br>
            <div class="col-md-12">
                <div class="page">
                    <div class="page-header-space">
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="font-size: 20pt;">
                            <b>REPORT OF COLLECTION AND DEPOSITS - NAIA-TERMINAL 1</b> 
                            <br>
                        </div>
                        @if(isset($month) && isset($year))
                        <div class="col-md-12 text-center" style="font-size: 12pt;">
                            <b>For the month of {{ date('F Y', strtotime($month.'/01/'.$year)) }}</b> 
                            <br>
                            <br>
                        </div>
                        @else
                        {{-- <div class="col-md-12 text-center" style="font-size: 12pt;">
                            <b>For the period of <u>{{ date('F d, Y',strtotime($start_date)) }} - {{ date('F d, Y',strtotime($end_date)) }}</u></b> 
                            <br>
                            <br>
                        </div> --}}
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center" style="font-size: 20pt;">
                            DATE
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered text-center">
                                <tbody>
                                    <tr>
                                        <td colspan="2" width="20%">OFFICIAL RECEIPT</td>
                                        <td rowspan="2" width="10%">RESPONSIBILITY <br> CENTER CODE</td>
                                        <td rowspan="2" width="10%">PAYOR</td>
                                        <td colspan="3" width="60%">TRAVEL TAX</td>
                                    </tr>
                                    <tr>
                                        <td>FROM</td>
                                        <td>&nbsp&nbspTO&nbsp&nbsp</td>
                                        <td class="text-right">PESO (CASH)</td>
                                        <td width="10%">DEB./CRED. CARD</td>
                                        <td>DOLLAR</td>
                                      
                                    </tr>
                                    <tr>
                                        <td>3439167</td>
                                        <td>3439237</td>
                                        <td></td>
                                        <td>VARIOUS PAYOR</td>
                                        <td class="text-right">139,300.00</td>
                                        <td class="text-right">9,320.00</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td class="text-right">TOTALS &nbsp;&nbsp;&nbsp; 139,300.00</td>
                                        <td class="text-right">9,320.00</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr></hr>
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 text-right">
                                TOTAL PESO (CASH) &nbsp;&nbsp; P
                        </div>
                        <div class="col-md-1 text-right">
                            <u style="  text-decoration-line: underline; text-decoration-style: double;"> 139,900.00</u>
                        </div>
                        <div class="col-md-2 text-right">
                                TOTAL DOLLAR &nbsp;&nbsp; $
                        </div>
                        <div class="col-md-1 text-right">
                             <u style="  text-decoration-line: underline; text-decoration-style: double;">    </u>
                        </div>
                        <div class="col-md-4">
                            TOTAL CREDIT/DEBIT CARD P <u style="  text-decoration-line: underline; text-decoration-style: double;">9,320.00</u>
                        </div>
                    </div><br>
                    <div class="row px-2">
                      <div class="col-12">
                          SUMMARY:
                      </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                            Undeposited Collection per last Report No. &nbsp;&nbsp;&nbsp; 21-11-0305 &emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P
                        </div>
                        <div class="col-md-1 text-right">
                            <u style="  text-decoration-line: underline; text-decoration-style: double;"> 829,700.00</u>
                        </div>
                        <div class="col-md-2 text-right">
                            $
                        </div>
                        <div class="col-md-1 text-right">
                          33.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                            Collections per O.R. Nos. 343916 - 3439237
                        </div>
                        <div class="col-md-1 text-right">
                           139,900.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right">
                           0.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                            ADD: Miscellaneous Income
                        </div>
                        <div class="col-md-1 text-right">
                            0.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right">
                           0.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                            &emsp; * LESS: Payment of Shortage/s
                        </div>
                        <div class="col-md-1 text-right">
                          0.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                           0.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div> <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                         
                        </div>
                        <div class="col-md-1 text-right">
                          ___________
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                            ___________
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                         
                        </div>
                        <div class="col-md-1 text-right">
                                969,000.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                           33.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                         
                        </div>
                        <div class="col-md-1 text-right">
                          969,000.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                           33.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                         Deposits
                        </div>
                        <div class="col-md-1 text-right">
                          832,940.00
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                           0.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                         
                        </div>
                        <div class="col-md-1 text-right">
                          ___________
                        </div>
                        <div class="col-md-2 text-right">
                    
                        </div>
                        <div class="col-md-1 text-right ">
                            ___________
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br>
                    <div class="row px-2">
                        <div class="col-md-4 ">
                            Undeposited Collection per last Report No. &nbsp;&nbsp;&nbsp; 21-11-0306 &emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; P
                        </div>
                        <div class="col-md-1 text-right">
                           136,060.00
                        </div>
                        <div class="col-md-2 text-right">
                            $
                        </div>
                        <div class="col-md-1 text-right ">
                            33.00
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <br><br><br><br><br><br><br><br><br>
                    
                </div>
            </div>
        </div>
    </div>
</body>
</html>
