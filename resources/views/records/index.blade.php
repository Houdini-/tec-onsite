@extends('layouts.master-auth')
@section('css')
@include('layouts.auth-partials.datatables-css')
<style>
td{
    font-size: 0.90rem!important;
}
</style>
@endsection
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
    <div class="card-header card-header-contrast">Record of Transaction</div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-4 col-lg-4 mt-1">
                <label for="">DATE FROM</label>
                <input type="date" name="" id="" class="form-control form-control-sm">
            </div>
            <div class="col-4 col-sm-4 col-lg-4 mt-1">
                <label for="">DATE TO</label>
                <input type="date" name="" id="" class="form-control form-control-sm">
            </div>
        </div>
        <table class="table table-striped bg-white table-hover table-fw-widget" id="table4">
            <thead>
                <tr>
                    <td>Type</td>
                    <td>OR Number</td>
                    <td>Onsite #</td>
                    <td>Name</td>
                    <td>Travel Type</td>
                    <td>Amount</td>
                    <td>Ticket Number</td>
                    <td>Country Destination</td>
                    <td>Application Date</td>
                    <td width="15%"></td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table> 
    </div>
</div>

@endsection


@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script type="text/javascript"> 
    $(document).ready(function () {
        App.init();
        App.dataTables();
        loadDatatable('fulltax');
      

    });

    $('.custom-control-input').change(function()
    {
        var id = $(this).attr('id');
        loadDatatable(id);
    });
    function loadDatatable(id = "none")
    {
        
        @php
        
        $columns = array(['data' => 'trans_type'],['data' => 'or_no','orderable' => false],['data' => 'onsite_reference'],['data' => 'full_name'],['data' => 'class_type'],['data' => 'fulltax_amount'],['data' => 'ticket_no'],['data' => 'country_name'],['data' => 'created_at'],['data' => 'action']);
        
        @endphp
        var url = '{{ route("records.datatables",":id") }}';
        url = url.replace(':id','{{auth()->user()->id}}');
        load_datables('#table4', url ,{!! json_encode($columns) !!}, null);

    }
</script>
@endsection