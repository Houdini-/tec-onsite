@extends('layouts.master-auth')
@section('content')
@include('layouts.auth-partials.datatables-css')

<div class="card card-contrast card-border-color card-border-color-primary" id="home">
    <div class="card-header card-header-contrast">Cash Collection Deposits</div>
        <div class=" card-body ">
            <div class="row ml-2">
                <label class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" id="fulltax" name="radio-inline"checked ><span class="custom-control-label">Daily</span>
                </label>
                <label class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" id="tec" name="radio-inline" ><span class="custom-control-label">Summary</span>
                </label>
                <label class="custom-control custom-radio custom-control-inline">
                    <input class="custom-control-input" type="radio" id="rtt" name="radio-inline" ><span class="custom-control-label">Deposits</span>
                </label>
            </div>    
            <div class="row">
                <div class="col-md-12 col-xl-3 col-12"> 
                    <div class="card card-contrast " id="home">
                        <div class=" card-body main-content">
                            <div class="row">
                                <label for="" class="form-label"><b>Filter By</b></label>
                                <select class="select2 select2-xs @error('class_type') is-invalid @endif" 
                                name="class_type" 
                                id="class_type">  
                                    <option value=""  disabled selected>{{ __('page.please_select') }}</option> 
                                    <option value="1" >Offices</option>
                                    <option value="2" >Satellites</option>
                                    <option value="3" >Airports</option>
                                </select>
                            </div>
                            <br>
                            <div class="row">
                                <label for="" class="form-label"><b>Transaction Date</b></label>
                            </div>
                            <div class="row pb-2">
                                <label for="" class="form-label">From</label>
                                <input type="date" name="from" id="from" class="form-control-xs form-control">
                            </div>
                            <div class="row pb-2">
                                <label for="" class="form-label">To</label>
                                <input type="date" name="to" id="to" class="form-control-xs form-control">
                            </div>
                            <br><br><br><br><br><br>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xl-9 col-12">
                    <table class="table table-striped bg-white table-hover table-fw-widget" id="tbl">
                        <thead>
                            {{-- <tr> --}}
                                <th>Offices</th>
                                <th>Payment</th>
                                <th>Peso</th>
                                <th>Dollar</th>
                                <th>Credit Card</th>
                                <th>Total</th>
                            {{-- </tr>    --}}
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6" class="text-center"> No Records Found.. </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        </tbody>
                    </table> 
                </div>
            </div>  
        </div>

    <br><br><br> <br><br><br> <br><br><br>
    </div>
</div>
@endsection

@section('scripts')
@include('layouts.auth-partials.datatables-scripts')
<script type="text/javascript">
    $(document).ready(function(){
        //-initialize the javascript
        App.formElements();
        App.init();
        App.dataTables();
    });
  </script>

@endsection