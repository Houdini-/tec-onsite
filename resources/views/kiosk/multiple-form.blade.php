@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    {{-- <p class="color-navy h1 p-3">TRAVEL TAX SERVICES</p> --}}
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.multiple-validate')}}" method="post"> 
                  @csrf
                  <div class="row p-3 table-responsive noSwipe">
                      <table class="table table-bordered table-hover table-fw-widget text-uppercase" id="tblItems" >
                          <thead class="bg-dark text-white "> 
                              <tr>
                                  <th>Surname</th>
                                  <th>Firstname</th>
                                  <th>Middlename</th>
                                  <th>Mobile No.</th>
                                  <th>Email Address</th>
                                  <th>Destination</th>
                                  <th>Ticket No. / Reference No.</th>
                                  <th>Passport No.</th>

                              </tr>
                          </thead>
                          <tbody>
                            <tr class="">
                              <td><input type="hidden" name="last_name[]" id="last_name"><label id="lbl_lastname"></label></td>
                              <td><input type="hidden" name="first_name[]" id="first_name"><label id="lbl_firsmiddle"></label></td>
                              <td><input type="hidden" name="middle_name[]" id="middle_name"><label id="lbl_middlename"></label></td>
                              <td><input type="hidden" name="mobile_no[]" id="mobile_no"><label id="lbl_mobile"></label></td>
                              <td><input type="hidden" name="email_address[]" id="email_address"><label id="lbl_email"></label></td>
                              <td><input type="hidden" name="destination[]" id="destination"><label id="lbl_destination"></label></td>
                              <td class="d-none"><input type="hidden" name="destination_text[]" id="destination_text"><label id="lbl_destination_text"></label></td>
                              <td><input type="hidden" name="ticket_no[]" id="ticket_no"><label id="lbl_ticket"></label></td>
                              <td><input type="hidden" name="passport_no[]" id="passport_no"><label id="lbl_passport"></label></td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <br>
                    <div class="row ">
                      <div class="col-12 col-lg-4 text-left ">
                          <label for="last_name" class="h4">Surname:</label>
                          <input type="text" name="input_lastname" id="input_lastname" class="form-control form-control-md text-uppercase @error('last_name') is-invalid @endif">
                          @error('last_name') <div class="invalid-feedback">Surname is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="first_name" class="h4">Firstname:</label>
                        <input type="text" name="input_firstname" id="input_firstname" class="form-control form-control-md text-uppercase @error('first_name') is-invalid @endif">
                        @error('first_name') <div class="invalid-feedback">Firstname is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="middle_name" class="h4">Middlename:</label>
                        <input type="text" name="input_middlename" id="input_middlename"  class="form-control form-control-md text-uppercase">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12 col-lg-4 text-left">
                          <label for="mobile_no" class="h4">Mobile No.:</label>
                          <input type="text" name="input_mobile" id="input_mobile"  class="form-control form-control-md text-uppercase @error('mobile_no') is-invalid @endif">
                          @error('mobile_no') <div class="invalid-feedback">Mobile No. is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="email_address" class="h4">Email Address:</label>
                        <input type="text" name="input_email" id="input_email" class="form-control form-control-md text-uppercase @error('email_address') is-invalid @endif">
                        @error('email_address') <div class="invalid-feedback">Email Address is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="destination" class="h4">Destination:</label>
                        <select class="select2 select2-md  @error('destination') is-invalid @endif" 
                            name="input_destination" 
                            id="input_destination">  
                            <option value="" selected disabled >{{ __('page.please_select') }}</option> 
                            @foreach ($countries as $item)
                              <option value="{{$item->id}}">{{ $item->name}}</option> 
                            @endforeach
                        </select>
                        @error('destination') <div class="invalid-feedback">Destination is required</div> @endif
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-lg-4 text-left">
                        <label for="ticket_no" class="h4">Ticket No./Reference No.:</label>
                        <input type="text" name="input_ticket" id="input_ticket" class="form-control form-control-md text-uppercase @error('ticket_no') is-invalid @endif">
                        @error('ticket_no') <div class="invalid-feedback">Ticket No./Reference No. is required</div> @endif
                    </div>
                    <div class="col-12 col-lg-4 text-left">
                      <label for="passport_no" class="h4">Passport No.:</label>
                      <input type="text" name="input_passport" id="input_passport" class="form-control form-control-md text-uppercase @error('passport_no') is-invalid @endif">
                      @error('passport_no') <div class="invalid-feedback">Passport Number is required</div> @endif
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-12 col-lg-6 text-primary">
                        For Full Tax multiple transaction maximum of 5 pax per transaction
                    </div>
                    <div class="col-12 col-lg-6">
                        <button type="button" name="" id="add_btn" class="btn btn-primary btn-lg w-50">
                        <span class="mdi mdi-plus icon"></span>
                        ADD</button>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-6 col-md-4 text-right mt-2">
                        <button type="button" class="btn btn-primary btn-xl w-50" onclick="history.back()"> < Cancel </button>
                    </div>
                    <div class="col-6 offset-md-4 col-md-4 text-left py-2">
                        <button type="submit" class="btn btn-primary btn-xl w-50"> Next > </button>
                    </div>
                  </div>
              </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>
<div class="">
  <table class="table table-hover table-bordered text-uppercase d-none" id="tblCopy">
    <thead class="bg-primary text-white "> 
        <tr>
            <th>Surname</th>
            <th>Firstname</th>
            <th>Middlename</th>
            <th>Ticket No. / Reference No.</th>
            <th>Middlename</th>

        </tr>
    </thead>
    <tbody>
        <tr class="">
            <td><input type="hidden" name="last_name[]" id="last_name"><label id="lbl_lastname"></label></td>
            <td><input type="hidden" name="first_name[]" id="first_name"><label id="lbl_firstname"></label></td>
            <td><input type="hidden" name="middle_name[]" id="middle_name"><label id="lbl_middlename"></label></td>
            <td><input type="hidden" name="mobile_no[]" id="mobile_no"><label id="lbl_mobile"></label></td>
            <td><input type="hidden" name="email_address[]" id="email_address"><label id="lbl_email"></label></td>
            <td><input type="hidden" name="destination[]" id="destination"><label id="lbl_destination"></label></td>
            <td class="d-none"><input type="hidden" name="destination_text[]" id="destination_text"><label id="lbl_destination"></label></td>
            <td><input type="hidden" name="ticket_no[]" id="ticket_no"><label id="lbl_ticket"></label></td>
            <td><input type="hidden" name="passport_no[]" id="passport_no"><label id="lbl_passport"></label></td>
        </tr>
       
    </tbody>
</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    var rowCount = 0;
    App.formElements();
  $('#destination').change(function()
  {
      $('#destination_text').val($('#destination :selected').text());
  });
  $('#add_btn').click(function()
  {
        if(rowCount == 5) {
            alert('Maximum of 5 pax per transaction!');
            return;
        }
        if(rowCount == 0)  $('table#tblItems tbody>tr').empty();
         rowCount += 1;
        $("#tblCopy tbody>tr:first").clone(true).insertAfter("#tblItems tbody>tr:last");
        $("#tblItems tbody>tr:last").attr("id", "tr"+rowCount);
        $("#tblItems tbody>tr:last #row_no").text("#"+rowCount);
        $("#tblItems tbody>tr:last #row_no").css('font-weight','bold');
        $("#tblItems tbody>tr:last #lbl_lastname").text($('#input_lastname').val());
        $("#tblItems tbody>tr:last #last_name").val($('#input_lastname').val());
        $("#tblItems tbody>tr:last #lbl_firstname").text($('#input_firstname').val());
        $("#tblItems tbody>tr:last #first_name").val($('#input_firstname').val());
        $("#tblItems tbody>tr:last #lbl_middlename").text($('#input_middlename').val());
        $("#tblItems tbody>tr:last #middle_name").val($('#input_middlename').val());
        $("#tblItems tbody>tr:last #lbl_ticket").text($('#input_ticket').val());
        $("#tblItems tbody>tr:last #ticket_no").val($('#input_ticket').val());
        $("#tblItems tbody>tr:last #lbl_passport").text($('#input_passport').val());
        $("#tblItems tbody>tr:last #passport_no").val($('#input_passport').val());
        $("#tblItems tbody>tr:last #lbl_mobile").text($('#input_mobile').val());
        $("#tblItems tbody>tr:last #mobile_no").val($('#input_mobile').val());
        $("#tblItems tbody>tr:last #lbl_email").text($('#input_email').val());
        $("#tblItems tbody>tr:last #email_address").val($('#input_email').val());
        $("#tblItems tbody>tr:last #lbl_destination").text($('#input_destination :selected').text());
        $("#tblItems tbody>tr:last #destination").val($('#input_destination').val());
        $("#tblItems tbody>tr:last #destination_text").val($('#input_destination :selected').text());

  });

</script>

@endsection