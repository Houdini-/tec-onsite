@extends('layouts.master-auth')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
td{
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.store_multiple')}}" method="post"> 
                  @csrf
                  <div class="card-title">
                  
                  </div>
                  <div class="row">
                      <div class="col-12">
                        <table class="table table-bordered">
                          <thead class=" bg-secondary text-white "> 
                            <tr>
                                <th></th>
                                <th>Surname</th>
                                <th>Firstname</th>
                                <th>Middlename</th>
                                <th>Ticket No. / Reference No.</th>
                                <th>Passport No.</th>
                            </tr>
                        </thead>
                         <tbody>
                              @foreach ($details as $item)
                              <tr>
                                <td><div class="tools">
                                  <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                  <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                      <a href="{{route('application.index', ['type'=>'fulltax','id'=>encrypt($item->id)])}}" class="dropdown-item" >
                                          <i class="icon icon-left mdi mdi-money-box"></i>Full Tax
                                      </a>
                                      <a href="{{route('application.index', ['type'=>'exemption','id'=>encrypt($item->id)])}}" class="dropdown-item" >
                                          <i class="icon icon-left mdi mdi-money-off"></i>Exemption
                                      </a>
                                      <a href="{{route('application.index', ['type'=>'reduced','id'=>encrypt($item->id)])}}" class="dropdown-item" >
                                        <i class="icon icon-left mdi mdi-minus-circle-outline"></i>Reduced
                                      </a>
                                      <a href="{{route('application.index', ['type'=>'refund','id'=>encrypt($item->id)])}}" class="dropdown-item" >
                                        <i class="icon icon-left mdi mdi-undo"></i>Refund
                                      </a>
                                    </div></td>
                                <td><input type="hidden" name="last_name[]" value="{{$item->last_name}}">{{$item->last_name}}</td>
                                <td><input type="hidden" name="first_name[]" value="{{$item->first_name}}">{{$item->first_name}}</td>
                                <td><input type="hidden" name="middle_name[]" value="{{$item->first_name}}">{{$item->first_name}}</td>
                                <td><input type="hidden" name="ticket_no[]" value="{{$item->ticket_no}}">{{$item->ticket_no}}</td>
                                <td><input type="hidden" name="passport_no[]" value="{{$item->passport_no}}">{{$item->passport_no}}</td>
                              </tr>
                              @endforeach
                             
                               
                         </tbody>
                          </table>
                      </div>
                  </div>
                  <br>
                  
                </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>

@endsection
