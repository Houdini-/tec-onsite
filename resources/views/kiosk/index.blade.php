<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<style type="text/css">
        .event-background{
            background: url("img/bg-tiezadarker.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
        .header-bgs{
            background: url("img/header-bg.jpeg");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
</style>
@include('layouts.auth-partials.meta')
@yield('meta')
@include('layouts.auth-partials.css')

@yield('css')
@yield('employees_css')
<link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
<div class="header-bgs ">
    <div class="container">
       <a href="/kiosk"> <img src="{{asset('img/tieza-logo2.png')}}"  class="p-2 mx-auto img-fluid logos"> </a>
    </div>
</div>
</head>
<body class="event-background">
    @include('layouts.auth-partials.kiosk-content') 
    @include('layouts.auth-partials.scripts')
    @yield('employees_scripts')
    @yield('scripts')
    <!-- <script src="{{ asset('js/custom.js')}}"></script> -->
</body>
</html>