@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
    
}
.type_label 
{
    color:rgb(13, 25, 148) !important;
    font-weight: 900;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1 p-3">TRAVEL TAX SERVICES</p>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-center"><span class="text-white">{{$kiosk->ref_no}}</span></div>
            <div class="card-body">
                <img src="" alt="" id="img-qr" style="height:300px;width:300px">
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
            <div class="text-center"><h1><a href="{{route('kiosk.index')}}"> Back to Home</a></h1></div>  
        </div>
        </div>
      </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function()
    {
        $('.logos').addClass('d-none');
        var id = '{{$id}}';
        var url = "{{ url('/qr_code_ft/:id/_qrcode.png') }}";
        url = url.replace(':id',id);
        $('#img-qr').attr('src',url);
 
    });
</script>    

@endsection
