@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
    
}
.type_label 
{
    color:rgb(13, 25, 148) !important;
    font-weight: 900;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1 p-3">TRAVEL TAX SERVICES</p>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Type of Transaction</span></div>
            <div class="card-body">
                <br><br>
                <div class="row py-4">
                    <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
                        <button type="button" class="btn btn-dark btn-block btn-xl p-1" onclick="window.location= '{{route('kiosk.single')}}'"><span class="h3"><i class="mdi mdi-account-add"></i> SINGLE</span></button>
                    </div>
                </div>
                <div class="row py-2">
                    <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
                        <button type="button" class="btn btn-dark btn-block btn-xl p-1" onclick="window.location= '{{route('kiosk.multiple')}}'"><span class="h3"><i class="mdi mdi-accounts-add"></i> MULTIPLE</span></button>
                    </div>
                </div>
                <div class="row py-4 type_label">
                    <div class=" col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 offset-xl-3">
                        <p>For Full Tax multiple transaction maximum of 5 pax per transaction</p>
                    </div>
                </div>

            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
    </div>
</div>

@endsection
