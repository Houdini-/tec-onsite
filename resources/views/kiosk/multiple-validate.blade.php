@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
td{
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details Confirmation</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.store_multiple')}}" method="post"> 
                  @csrf
                  <div class="card-title">
                    <u>Please confirm transaction below</u> 
                  </div>
                  <div class="row">
                      <div class="col-12">
                        <table class="table table-bordered">
                          <thead class="bg-primary text-white "> 
                            <tr>
                                <th>Surname</th>
                                <th>Firstname</th>
                                <th>Middlename</th>
                                <th>Mobile No.</th>
                                <th>Email Address</th>
                                <th>Destination</th>
                                <th>Ticket No. / Reference No.</th>
                                <th>Passport No.</th>
                            </tr>
                        </thead>
                         <tbody>
                              @for ($i = 0; $i < count($details['last_name']); $i++)
                              <tr>
                                <td><input type="hidden" name="last_name[]" value="{{$details['last_name'][$i]}}">{{$details['last_name'][$i]}}</td>
                                <td><input type="hidden" name="first_name[]" value="{{$details['first_name'][$i]}}">{{$details['first_name'][$i]}}</td>
                                <td><input type="hidden" name="middle_name[]" value="{{$details['middle_name'][$i]}}">{{$details['middle_name'][$i]}}</td>
                                <td><input type="hidden" name="mobile_no[]" value="{{$details['mobile_no'][$i]}}">{{$details['mobile_no'][$i]}}</td>
                                <td><input type="hidden" name="email_address[]" value="{{$details['email_address'][$i]}}">{{$details['email_address'][$i]}}</td>
                                <td><input type="hidden" name="destination[]" value="{{$details['destination'][$i]}}">{{$details['destination_text'][$i]}}</td>
                                <td><input type="hidden" name="ticket_no[]" value="{{$details['ticket_no'][$i]}}">{{$details['ticket_no'][$i]}}</td>
                                <td><input type="hidden" name="passport_no[]" value="{{$details['passport_no'][$i]}}">{{$details['passport_no'][$i]}}</td>
                              </tr>
                              @endfor
                               
                         </tbody>
                          </table>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-12 text-center">
                      <a href="#" class="">*If incorrect click CANCEL, then edit the details to correct.</a>
                    </div>
                  </div>
                  <br>
                  <div class="row text-center"> 
                    <div class="col-6 col-md-4 text-right mt-2">
                        <button type="button" class="btn btn-primary btn-xl w-50" onclick="history.back()"> < Cancel </button>
                    </div>
                    <div class="col-6 offset-md-4 col-md-4 text-left py-2">
                        <button type="submit" class="btn btn-primary btn-xl w-50"> Next > </button>
                    </div>
                  </div>
                </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>

@endsection
