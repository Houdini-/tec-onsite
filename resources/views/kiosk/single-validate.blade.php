@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
td{
  text-transform: uppercase;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1 p-3">TRAVEL TAX SERVICES</p>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details Confirmation</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.store')}}" method="post"> 
                  @csrf
                  <div class="card-title">
                    <u>Please confirm transaction below</u> 
                  </div>
                  <div class="row">
                      <div class="col-12">
                        <table class="table table-bordered">
                            <tr >
                              <td width="35%" class="bg-dark text-white">Surname</td>
                              <td width="65%"><b>{{ $details['last_name']}}</b>
                                <input type="hidden" name="last_name" id="" value="{{ $details['last_name']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Firstname</td>
                              <td ><b>{{ $details['first_name']}}</b>
                                <input type="hidden" name="first_name" id="" value="{{ $details['first_name']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Middlename</td>
                              <td ><b>{{ $details['middle_name']}}</b>
                                <input type="hidden" name="middle_name" id="" value="{{ $details['middle_name']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Mobile No.</td>
                              <td ><b>{{ $details['mobile_no']}}</b>
                                <input type="hidden" name="mobile_no" id="" value="{{ $details['mobile_no']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Email Address</td>
                              <td ><b>{{ $details['email_address']}}</b>
                                <input type="hidden" name="email_address" id="" value="{{ $details['email_address']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Destination</td>
                              <td ><b>{{ $details['destination_text']}}</b>
                                <input type="hidden" name="destination" id="" value="{{ $details['destination']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Ticket No./Reference No.</td>
                              <td ><b>{{ $details['ticket_no']}}</b>
                                <input type="hidden" name="ticket_no" id="" value="{{ $details['ticket_no']}}">
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-dark text-white">Passport No.</td>
                              <td ><b>{{ $details['passport_no']}}</b>
                                <input type="hidden" name="passport_no" id="" value="{{ $details['passport_no']}}">
                              </td>
                            </tr>
                          </table>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-12 text-center">
                      <a href="#" class="">*If incorrect click CANCEL, then edit the details to correct.</a>
                    </div>
                  </div>
                  <br>
                  <div class="row text-center"> 
                    <div class="col-6 col-md-4 text-right mt-2">
                        <button type="button" class="btn btn-primary btn-xl w-50" onclick="history.back()"> < Cancel </button>
                    </div>
                    <div class="col-6 offset-md-4 col-md-4 text-left py-2">
                        <button type="submit" class="btn btn-primary btn-xl w-50"> Next > </button>
                    </div>
                  </div>
                </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>

@endsection
