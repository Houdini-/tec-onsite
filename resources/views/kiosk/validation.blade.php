@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1 p-3">TRAVEL TAX SERVICES</p>
    <div class="row">
        <div class="col-12 col-lg-6 offset-lg-3">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.validate')}}" method="post"> 
                  @csrf
                  <div class="card-title"></div>
                  <div class="row ">
                      <div class="col-5 text-right mt-2">
                            <h4> Surname:</h4> 
                        </div>
                      <div class="col-5 text-left py-2">
                          <input type="text" name="last_name" class="form-control form-control-md text-uppercase @error('last_name') is-invalid @endif">
                          @error('last_name') <div class="invalid-feedback">Surname is required</div> @endif
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-5 text-right mt-2">
                          <h4> Firstname:</h4> 
                      </div>
                    <div class="col-5 text-left py-2">
                        <input type="text" name="first_name"class="form-control form-control-md text-uppercase @error('first_name') is-invalid @endif">
                        @error('first_name') <div class="invalid-feedback">Firstname is required</div> @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-5 text-right mt-2">
                          <h4> Middlename:</h4> 
                      </div>
                    <div class="col-5 text-left py-2">
                        <input type="text" name="middle_name" class="form-control form-control-md text-uppercase">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-5 text-right mt-2">
                          <h4> Ticket No./Reference No.:</h4> 
                      </div>
                    <div class="col-5 text-left py-2">
                        <input type="text" name="ticket_no" class="form-control form-control-md text-uppercase @error('ticket_no') is-invalid @endif">
                        @error('ticket_no') <div class="invalid-feedback">Ticket No./Reference No. is required</div> @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-5 text-right mt-2">
                          <h4> Passport No.:</h4> 
                      </div>
                    <div class="col-5 text-left py-2">
                        <input type="text" name="passport_no" class="form-control form-control-md text-uppercase @error('passport_no') is-invalid @endif">
                        @error('passport_no') <div class="invalid-feedback">Passport Number is required</div> @endif
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-4 text-right mt-2">
                        <button type="button" class="btn btn-primary btn-xl w-50"> <<< Cancel </button>
                    </div>
                    <div class="col-4 offset-4 text-left py-2">
                        <button type="submit" class="btn btn-primary btn-xl w-50"> Next >>> </button>
                    </div>
                  </div>
                </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>

@endsection
