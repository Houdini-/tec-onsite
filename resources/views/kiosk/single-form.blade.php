@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1">TRAVEL TAX SERVICES</p>
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1  ">
          <div class="card card-contrast ">
            <div class="card-header card-header-contrast card-header-featured bg-primary text-left"><span class="text-white">Transaction Details</span></div>
            <div class="card-body">
                <form action="{{route('kiosk.validate')}}" method="post"> 
                  @csrf
                  <div class="card-title"></div>
                  <div class="row ">
                      <div class="col-12 col-lg-4 text-left ">
                          <label for="last_name" class="h4">Surname:</label>
                          <input type="text" name="last_name" class="form-control form-control-md text-uppercase @error('last_name') is-invalid @endif">
                          @error('last_name') <div class="invalid-feedback">Surname is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="first_name" class="h4">Firstname:</label>
                        <input type="text" name="first_name"class="form-control form-control-md text-uppercase @error('first_name') is-invalid @endif">
                        @error('first_name') <div class="invalid-feedback">Firstname is required</div> @endif
                      </div>
                      <div class="col-12 col-lg-4 text-left">
                        <label for="middle_name" class="h4">Middlename:</label>
                        <input type="text" name="middle_name" class="form-control form-control-md text-uppercase">
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-lg-4 text-left">
                        <label for="mobile_no" class="h4">Mobile No.:</label>
                        <input type="text" name="mobile_no" class="form-control form-control-md text-uppercase @error('mobile_no') is-invalid @endif">
                        @error('mobile_no') <div class="invalid-feedback">Mobile No. is required</div> @endif
                    </div>
                    <div class="col-12 col-lg-4 text-left">
                      <label for="email_address" class="h4">Email Address:</label>
                      <input type="text" name="email_address" class="form-control form-control-md text-uppercase @error('email_address') is-invalid @endif">
                      @error('email_address') <div class="invalid-feedback">Email Address is required</div> @endif
                    </div>
                    <div class="col-12 col-lg-4 text-left">
                      <label for="destination" class="h4">Destination:</label>
                      <select class="select2 select2-md  @error('destination') is-invalid @endif" 
                          name="destination" 
                          id="destination">  
                          <option value="" selected disabled >{{ __('page.please_select') }}</option> 
                          @foreach ($countries as $item)
                            <option value="{{$item->id}}">{{ $item->name}}</option> 
                          @endforeach
                      </select>
                      @error('destination') <div class="invalid-feedback">Destination is required</div> @endif
                      <input type="hidden" class="form-control"name="destination_text" id="destination_text">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-lg-4 text-left">
                        <label for="ticket_no" class="h4">Ticket No./Reference No.:</label>
                        <input type="text" name="ticket_no" class="form-control form-control-md text-uppercase @error('ticket_no') is-invalid @endif">
                        @error('ticket_no') <div class="invalid-feedback">Ticket No./Reference No. is required</div> @endif
                    </div>
                    <div class="col-12 col-lg-4 text-left">
                      <label for="passport_no" class="h4">Passport No.:</label>
                      <input type="text" name="passport_no" class="form-control form-control-md text-uppercase @error('passport_no') is-invalid @endif">
                      @error('passport_no') <div class="invalid-feedback">Passport Number is required</div> @endif
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-6 col-md-4 text-right mt-2">
                        <button type="button" class="btn btn-primary btn-xl w-50" onclick="history.back()"> < Cancel </button>
                    </div>
                    <div class="col-6 offset-md-4 col-md-4 text-left py-2">
                        <button type="submit" class="btn btn-primary btn-xl w-50"> Next > </button>
                    </div>
                  </div>
                </form>
            </div>
            <div class="card-footer card-footer-contrast text-muted bg-primary"></div>
          </div>
        </div>
      </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
        App.formElements();
    $('#destination').change(function()
    {
        $('#destination_text').val($('#destination :selected').text());
    });
  });
</script>
@endsection