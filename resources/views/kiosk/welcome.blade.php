@extends('kiosk.index')
@section('css')
<style>
.color-navy{
    font-family:'Arial Black' !important;
    color:rgb(13, 25, 148) !important;
}
</style>
@endsection
@section('content')
 <div class="text-center">
    <p class="color-navy h1">ONLINE TRAVEL TAX SERVICES SYSTEM</p>
    <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 p-5 img-fluid " style="height: 30em;">
    <br><br>
    <button class="p-2 w-25 btn btn-primary btn-lg rounded" onclick="window.location ='{{route('kiosk.sm')}}'">START</button>
</div>
<br><br><br><br>
<div class="ml-5">
<h5 class="color-navy">For Inquiries or clarifications please contact us at <a href=""><u>traveltax.helpdesk@tieza.gov.ph</u></a></h5>
<h5 class="color-navy">TIEZA Central Office</h5>
<h6 class="color-navy">(02) 8 249 5987</h5>
<h5><a href=""><u>traveltax@tieza.gov.ph</u></a></h5>
<h5>Monday to Thursday 8:00 AM to 3:00 PM</h5>

</div>

@endsection
