@extends('layouts.master-auth')
@section('content')
<div class="card card-border-color card-border-color-primary" id="home">
    <div class="card-header">Dashboard</div>
        <div class=" main-content card-body ">
            <div class="row py-2">
                <div class="col-12 col-lg-6 col-xl-6 p-2 border border-5 border-warning">
                    Travel Tax Payment
                    <div class="row py-2">
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">First Class</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark1">0,5,3,7,5,10,3,6,5,10</div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ft_fc}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Economy </div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark2">0,5,3,7,5,10,3,6,5,10</div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ft_ec}}</span>
                                </div>
                            </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-6 p-2 border border-5 border-danger">
                    Travel Tax Exemption
                    <div class="row py-2">
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Awaiting Documents</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3">0,5,3,7,5,10,3,6,5,10</div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$tec_ad}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Approved Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark4">0,5,3,7,5,10,3,6,5,10</div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$tec_aa}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Denied Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark5">   </div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$tec_da}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Total Applications</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$tec_ta}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row py-2 ">
                <div class="col-12 col-lg-6 col-xl-6 p-2 border border-5 border-info">
                    Reduced Travel Tax Certificate
                    <div class="row py-2">
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Awaiting Documents</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$rtt_ad}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Approved Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark4"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$rtt_aa}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Denied Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark5">   </div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$rtt_da}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Total Applications</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$rtt_ta}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-6 p-2 border border-5 border-success">
                    Travel Tax Refund
                    <div class="row py-2">
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Awaiting Documents</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ttr_ad}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Approved Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark4"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ttr_aa}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Denied Application</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark5">   </div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ttr_da}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                        <div class="col-12 col-lg-12 col-xl-6">
                            <div class="widget mb-1">
                                <div class="widget-head mb-0">
                                    <div class="tools">
                                        <a href="" target="_blank"><span class="icon mdi mdi-print"></span></a>
                                    </div>
                                    <div class="title">Total Applications</div>
                                </div>
                            </div>
                            <div class="widget widget-tile">
                            <div class="chart sparkline" id="spark3"></div>
                            <div class="data-info">
                                <div class="desc"></div>
                                <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span class="number">{{$ttr_ta}}</span>
                                </div>
                            </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //-initialize the javascript
    
        App.init();
        App.chartsSparklines();
    });
  </script>

@endsection