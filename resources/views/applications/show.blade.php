@extends('layouts.master-auth')
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
<div class="card-header card-header-contrast">Transactions</div>
  <div class="card-body">
  @if($tec)
        <div class="tec">
                <form method="post" action="{{route('application.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>

                    <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">Trans. Date</label>
                        <input type="hidden" value="{{$tec->id}}" id="tec_id">
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="date" id="tec_trans_date" name="trans_date" class="form-control form-control-xs" value="{{$tec->date_app}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                   
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 p-1">
                        <label class="control-label" for="last_name">Last Name</label>
                        <input type="text" id="tec_last_name" name="last_name" class="form-control form-control-xs @error('last_name') is-invalid @endif" value="{{$tec->last_name}}">
                        @error('last_name') <div class="invalid-feedback">Last Name is required</div> @endif

                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <label class="control-label" for="last_name">First Name</label>
                        <input type="text" id="tec_first_name" name="first_name" class="form-control form-control-xs @error('first_name') is-invalid @endif" value="{{$tec->first_name}}">
                        @error('first_name') <div class="invalid-feedback">First Name is required</div> @endif
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-1 col-xl-1 p-1">
                        <label class="control-label" for="last_name">M.I</label>
                        <input type="text" id="tec_middle_name" name="middle_name" class="form-control form-control-xs @error('middle_name') is-invalid @endif"value="{{$tec->middle_name}}" >
                        @error('middle_name') <div class="invalid-feedback">M.I is required</div> @endif
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                        <label class="control-label" for="last_name">Ext.Name</label>
                        <input type="text" id="tec_ext_name" name="ext_name" class="form-control form-control-xs " value="{{$tec->ext_name}}">
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-4 col-lg-4 col-xl-4 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">Trans. No</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="text" id="trans_no" class="form-control form-control-xs" value="{{'APP-0000'.$tec->id}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Mobile No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="tec_mobile" name="mobile" class="form-control form-control-xs @error('mobile') is-invalid @endif" value="{{$tec->mobile_no}}">
                        @error('mobile') <div class="invalid-feedback">Mobile is required</div> @endif
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-6 col-lg-6 col-xl-6 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">TEC Cert. No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="text" id="tec_trans_certno"  name="trans_certno" class="form-control form-control-xs" value="{{$tec->tec_id}}">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Email Address</label>
                    
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="email" id="tec_email"  name="email" class="form-control form-control-xs @error('email') is-invalid @endif" value="{{$tec->email}}">
                        @error('email') <div class="invalid-feedback">Email is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Section</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs  @error('section') is-invalid @endif" 
                                name="section" 
                                id="tec_section">   
                                <option value="" disabled >{{ __('page.please_select') }}</option> 
                                @if($tec_sect)
                                    @foreach($tec_sect as $key => $val)
                                        @if($tec->applicant_type_id == $val->id)
                                        <option value="{{$val->id}}" selected>{{ $val->code }}</option>
                                        @else
                                        <option value="{{$val->id}}">{{ $val->code }}</option>
                                        @endif
                                    @endforeach
                                @endif
                        </select>
                        @error('section') <div class="invalid-feedback">Section is required</div> @endif
                    </div>
                    <div class="col-12 col-xs-12 col-sm-12  col-md-6 col-lg-6 col-xl-6 p-0 mt-0">
                        <textarea class="form-control form-control-xs" rows="" id="section_name" readonly></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Passport No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="tec_passport" name="passport" class="form-control form-control-xs  @error('passport') is-invalid @endif" value="{{$tec->passport_no}}">
                        @error('passport') <div class="invalid-feedback">Passport is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Ticket No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="tec_ticket" name="ticket" class="form-control form-control-xs  @error('ticket') is-invalid @endif" value="{{$tec->ticket_no}}">
                        @error('ticket') <div class="invalid-feedback">Ticket is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Date when Ticket was issued </label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="tec_date_issued" name="date_issued" class="form-control form-control-xs  @error('date_issued') is-invalid @endif" value="{{$tec->date_ticket_issued}}">
                        @error('date_issued') <div class="invalid-feedback">Date Issued is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Validity</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="tec_validity" name="validity" class="form-control form-control-xs @error('validity') is-invalid @endif"value="{{$tec->date_validity}}">
                        @error('validity') <div class="invalid-feedback">Validity is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Date of Flight</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="tec_flight_date" name="flight_date" class="form-control form-control-xs  @error('flight_date') is-invalid @endif" value="{{$tec->date_flight}}" >
                        @error('flight_date') <div class="invalid-feedback">Date of Flight is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Destination</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs @error('destination') is-invalid @endif" 
                                name="destination" 
                                id="tec_destination">   
                                <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                @if($countries)
                                    @foreach($countries as $key => $val)

                                    @if($tec->country_id == $val->id)
                                    <option value="{{$val->id}}" selected>{{ $val->name }}</option>
                                    @else
                                    <option value="{{$val->id}}">{{ $val->name }}</option>
                                    @endif
                                    @endforeach
                                @endif
                        </select>
                        @error('destination') <div class="invalid-feedback">Destination is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Airlines</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs @error('airlines') is-invalid @endif" 
                                name="airlines" 
                                id="tec_airlines">   
                                <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                @if($airlines)
                                    @foreach($airlines as $key => $val)
                                    @if($tec->airlines_id == $val->id)
                                    <option value="{{$val->id}}" selected>{{ $val->name }}</option>
                                    @else
                                    <option value="{{$val->id}}">{{ $val->name }}</option>
                                    @endif
                                    @endforeach
                                @endif
                        </select>
                        @error('airlines') <div class="invalid-feedback">Airlines is required</div> @endif
                    </div>
                    <div id="tec_generate" class="d-none col-6 col-xs-6 col-sm-6  col-md-8 col-lg-8 col-xl-8 text-right p-1">
                        <button type="button" class="btn btn-secondary btn-md"><i class="mdi mdi-floppy mdi-lg"></i> Generate Certificate</button>
                    </div>
                </div>
              
                </form>
        </div>
  @elseif($fulltax)
    <div class="fulltax  ">
                <form method="post" action="{{route('fulltax_save')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 text-right mt-1 p-0">
                            <label class="control-label" for="trans_date">Trans. Date</label>
                            <input type="hidden" value="{{$fulltax->id}}" id="ft_id">
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                            <input type="date" id="trans_date" class="form-control form-control-xs datetimepicker" value="{{$fulltax->date_app}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                    
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 p-1">
                            <label class="control-label" for="last_name">Last Name</label>
                            <input type="text" id="last_name" name="last_name"class="form-control form-control-xs @error('last_name') is-invalid @endif" value="{{$fulltax->last_name}}">
                            @error('last_name') <div class="invalid-feedback">Last Name is required</div> @endif
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <label class="control-label" for="last_name">First Name</label>
                            <input type="text" id="first_name" name="first_name"  class="form-control form-control-xs @error('first_name') is-invalid @endif" value="{{$fulltax->first_name}}">
                            @error('first_name') <div class="invalid-feedback">First Name is required</div> @endif
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                            <label class="control-label" for="last_name">M.I</label>
                            <input type="text" id="middle_name" name="middle_name" class="form-control form-control-xs" value="{{$fulltax->middle_name}}">
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                            <label class="control-label" for="last_name">Ext.Name</label>
                            <input type="text" id="ext_name" name="ext_name" class="form-control form-control-xs" value="{{$fulltax->ext_name}}">
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-4 col-lg-4 col-xl-4 text-right mt-1 p-0">
                            <label class="control-label" for="trans_date">Trans. No</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                            <input type="text" id="trans_date" class="form-control form-control-xs">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Class</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs @error('class_type') is-invalid @endif" 
                                    name="class_type" 
                                    id="class_type">  
                                    <option value="" disabled >{{ __('page.please_select') }}</option> 
                                    <option value="1" @if($fulltax->class == "1") selected  @endif>{{ __('page.first_class') }}</option>
                                    <option value="2" @if($fulltax->class == "2") selected  @endif>{{ __('page.business_class') }}</option>

                            </select>
                            @error('class_type') <div class="invalid-feedback">Class is required</div> @endif
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2"> 
                            <input type="text" class="form-control form-control-lg text-danger text-bold" id="class_amt" name="class_amt" readonly>
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right mt-0 p-1">
                        <label class="control-label" for="last_name">Mode of Payment</label>
                        </div>
                        <div class="col-4 col-xs-4 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                            <select class="select2 select2-xs @error('mop') is-invalid @endif" 
                                    name="mop" 
                                    id="mop">   
                                    <option value="" disabled >{{ __('page.please_select') }}</option> 
                                    <option value="1"@if($fulltax->mop == "1") selected  @endif> {{ __('page.peso') }}</option>
                                    <option value="2"@if($fulltax->mop == "2") selected  @endif>{{ __('page.dollars') }}</option>
                                    <option value="3"@if($fulltax->mop == "3") selected  @endif>{{ __('page.master_card') }}</option>
                                    <option value="4"@if($fulltax->mop == "4") selected  @endif>{{ __('page.visa') }}</option>

                            </select>
                            @error('mop') <div class="invalid-feedback">Mode of Payment is required</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Type</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs @error('fulltax_type') is-invalid @endif" 
                                    name="fulltax_type" 
                                    id="fulltax_type">   
                                    <option value="" disabled >{{ __('page.please_select') }}</option> 
                                    <option value="1"@if($fulltax->type == "1") selected  @endif>More Than 1 Year</option>
                                    <option value="2"@if($fulltax->type == "2") selected  @endif>Permanent Resident</option>
                                    <option value="3"@if($fulltax->type == "3") selected  @endif>Tourist</option>
                                    {{-- <option value="4"@if($fulltax->type == "4") selected  @endif>Type 4</option>
                                    <option value="5"@if($fulltax->type == "5") selected  @endif>Type 5</option> --}}

                            </select>
                            @error('fulltax_type') <div class="invalid-feedback">Type is required</div> @endif
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 "> </div>
                        
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right mop-card d-none"> Ref #</div>
                        
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 mop-card d-none">
                            <input type="text" class="form-control form-control-xs" name="card_ref"   value="{{$fulltax->card_ref}}">
                        </div>

                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right mop-dollar d-none"> Dollar Rate</div>
                        
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 mop-dollar d-none">
                            <input type="text" class="form-control form-control-xs" name="dollar_rate"   value="{{$fulltax->dollar_rate}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Passport No.</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="passport" name="passport" class="form-control form-control-xs @error('passport') is-invalid @endif" value="{{$fulltax->passport_no}}">
                        @error('passport') <div class="invalid-feedback">Passport No. is required</div> @endif
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 "> </div>
                        
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 text-right mop-card d-none"> Card No.</div>
                        
                        <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-2 col-xl-2 mop-card d-none">
                            <input type="text" class="form-control form-control-xs" name="card_no"  value="{{$fulltax->card_no}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Ticket No.</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="ticket" name="ticket" class="form-control form-control-xs @error('ticket') is-invalid @endif" value="{{$fulltax->ticket_no}}">
                        @error('ticket') <div class="invalid-feedback">Ticket No. is required</div> @endif

                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Departure Date</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="departure" name="departure" class="form-control form-control-xs @error('departure') is-invalid @endif" value="{{$fulltax->departure_date}}">
                        @error('departure') <div class="invalid-feedback">Departure Date is required</div> @endif
                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">Destination</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                            <select class="select2 select2-xs @error('ft_destination') is-invalid @endif" 
                                    name="ft_destination" 
                                    id="ft_destination">   
                                    <option value="" disabled >{{ __('page.please_select') }}</option> 
                                    @if($countries)
                                        @foreach($countries as $key => $val)
                                        @if($fulltax->airlines_id == $val->id)
                                        <option value="{{$val->id}}" selected>{{ $val->name }}</option>
                                        @else
                                        <option value="{{$val->id}}">{{ $val->name }}</option>
                                        @endif
                                        @endforeach
                                    @endif
                            </select>
                        @error('ft_destination') <div class="invalid-feedback">Destination is required</div> @endif

                        </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>
                        <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1"> </div>

                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                            <label class="control-label" for="last_name">OR No.</label>
                        </div>
                        <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="or_no" name="or_no" class="form-control form-control-xs @error('or_no') is-invalid @endif" value="{{$fulltax->or_no}}">
                        @error('or_no') <div class="invalid-feedback">OR No. is required</div> @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"></div>
                        <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                            <!-- <button type="submit" class="btn btn-primary " id="save"><span class="mdi mdi-floppy"></span> Save</button>
                            <button type="button" class="btn btn-danger"><span class="mdi mdi-close-circle"></span> Cancel</button> -->
                        </div>
                    </div>
                </form>
            </div>
  @elseif($rtt)   
  <div class="rtt">
                <form method="post" action="{{route('rtt_save')}}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">Trans. Date</label>
                        <input type="hidden" value="{{$rtt->id}}" id="rtt_id">
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="date" id="trans_date" name="trans_date" class="form-control form-control-xs datetimepicker" value="{{$rtt->date_app}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 col-xs-3 col-sm-3 col-md-9 col-lg-9 col-xl-9"></div>
                   
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 p-1">
                        <label class="control-label" for="last_name">Last Name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control form-control-xs @error('last_name') is-invalid @endif" value="{{$rtt->last_name}}">
                        @error('last_name') <div class="invalid-feedback">Last Name is required</div> @endif

                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <label class="control-label" for="last_name">First Name</label>
                        <input type="text" id="first_name" name="first_name" class="form-control form-control-xs @error('first_name') is-invalid @endif" value="{{$rtt->first_name}}">
                        @error('first_name') <div class="invalid-feedback">First Name is required</div> @endif
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-2 col-lg-1 col-xl-1 p-1">
                        <label class="control-label" for="last_name">M.I</label>
                        <input type="text" id="middle_name" name="middle_name" class="form-control form-control-xs @error('middle_name') is-invalid @endif" value="{{$rtt->middle_name}}">
                        @error('middle_name') <div class="invalid-feedback">M.I is required</div> @endif
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xl-1 p-1">
                        <label class="control-label" for="last_name">Ext.Name</label>
                        <input type="text" id="ext_name" name="ext_name" class="form-control form-control-xs" value="{{$rtt->ext_name}}"> 
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-4 col-lg-4 col-xl-4 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">Trans. No</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="text" id="trans_no" class="form-control form-control-xs" value="{{$rtt->app_id}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-4 col-lg-4 col-xl-4  mt-1 p-1">
                        <label class="custom-control custom-checkbox custom-control-inline">
                          <input class="custom-control-input" type="checkbox" checked=""><span class="custom-control-label custom-control-color">With Ticket</span>
                        </label>
                    </div>
                    <div class="col-3 col-xs-3 col-sm-3 col-md-6 col-lg-6 col-xl-6 text-right mt-1 p-0">
                        <label class="control-label" for="trans_date">RTT Cert. No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                        <input type="text" id="trans_certno"  name="trans_certno" class="form-control form-control-xs" value="{{$rtt->rtt_id}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Mobile No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="mobile" name="mobile" class="form-control form-control-xs @error('mobile') is-invalid @endif" value="{{$rtt->mobile_no}}">
                        @error('mobile') <div class="invalid-feedback">Mobile is required</div> @endif
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Email Address</label>
                    
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="email" id="email"  name="email" class="form-control form-control-xs @error('email') is-invalid @endif" value="{{$rtt->email}}">
                        @error('email') <div class="invalid-feedback">Email is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Section</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs  @error('rtt_section') is-invalid @endif" 
                                name="rtt_section" 
                                id="rtt_section">   
                                <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                @if($rtt_sect)
                                    @foreach($rtt_sect as $key => $val)
                                        @if($rtt->type_applicant_id == $val->id)
                                        <option value="{{$val->id}}" selected>{{ $val->code }}</option>
                                        @else
                                        <option value="{{$val->id}}" >{{ $val->code }}</option>

                                        @endif
                                    @endforeach
                                @endif
                        </select>
                        @error('rtt_section') <div class="invalid-feedback">Section is required</div> @endif
                    </div>
                    <div class="col-12 col-xs-12 col-sm-12  col-md-6 col-lg-6 col-xl-6 p-0 mt-0">
                        <textarea class="form-control form-control-xs" rows="" id="rtt_section_name" readonly></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Amount</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="amount" name="amount" class="form-control form-control-xs  @error('amount') is-invalid @endif" value="{{$rtt->reduced_amount}}">
                        @error('amount') <div class="invalid-feedback">Amount is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Passport No.</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="passport" name="passport" class="form-control form-control-xs  @error('passport') is-invalid @endif" value="{{$rtt->passport_no}}">
                        @error('passport') <div class="invalid-feedback">Passport is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Reservation Code</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="text" id="reservation" name="reservation" class="form-control form-control-xs  @error('reservation') is-invalid @endif" value="{{$rtt->ticket_no}}">
                        @error('reservation') <div class="invalid-feedback">Reservation Code is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Date when Ticket was issued </label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="date_issued" name="date_issued" class="form-control form-control-xs  @error('date_issued') is-invalid @endif" value="{{$rtt->date_ticket_issued}}">
                        @error('date_issued') <div class="invalid-feedback">Date Issued is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Validity</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="validity" name="validity" class="form-control form-control-xs @error('validity') is-invalid @endif" value="{{$rtt->date_validity}}">
                        @error('validity') <div class="invalid-feedback">Validity is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Date of Flight</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <input type="date" id="flight_date" name="flight_date" class="form-control form-control-xs  @error('flight_date') is-invalid @endif"value="{{$rtt->date_flight}}" >
                        @error('flight_date') <div class="invalid-feedback">Date of Flight is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Destination</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs @error('destination') is-invalid @endif" 
                                name="rtt_destination" 
                                id="rtt_destination">   
                                <option value="" disabled >{{ __('page.please_select') }}</option> 
                                @if($countries)
                                    @foreach($countries as $key => $val)
                                    @if($rtt->country_id == $val->id)
                                    <option value="{{$val->id}}" selected>{{ $val->name }}</option>
                                    @else
                                    <option value="{{$val->id}}">{{ $val->name }}</option>
                                    @endif
                                    @endforeach
                                @endif
                        </select>
                        @error('destination') <div class="invalid-feedback">Destination is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-right mt-1 p-1">
                        <label class="control-label" for="last_name">Airlines</label>
                    </div>
                    <div class="col-6 col-xs-6 col-sm-6  col-md-2 col-lg-2 col-xl-2 p-1">
                        <select class="select2 select2-xs @error('rtt_airlines') is-invalid @endif" 
                                name="rtt_airlines" 
                                id="rtt_airlines">   
                                <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                                @if($airlines)
                                    @foreach($airlines as $key => $val)
                                    <option value="{{$val->id}}" @if($rtt->airlines_id == $val->id) selected     @endif>{{ $val->name }}</option>
                                    @endforeach
                                @endif
                        </select>
                        @error('rtt_airlines') <div class="invalid-feedback">Airlines is required</div> @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"></div>
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                        <!-- <button type="submit" class="btn btn-primary " id="save"><span class="mdi mdi-floppy"></span> Save</button>
                        <button type="button" class="btn btn-danger"><span class="mdi mdi-close-circle"></span> Cancel</button> -->
                    </div>
                </div>
                </form>
            </div>      
  @endif
  <div class="row">
                    <div class="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9"></div>
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                        <button type="button" id="generate_cert" class="btn btn-secondary btn-md"><i class="mdi mdi-floppy mdi-lg"></i> Generate Certificate</button>
                    </div>
                    </div>
                </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript"> 
    var f_class = "2700.00";
    var e_class = "1620.00";
  $(document).ready(function () {
        App.formElements();
        var type = "{{$type}}";
        if(type == "fulltax")
        {
            $('#generate_cert').text('PRINT OR');
        }
        else  $('#generate_cert').text('GENERATE CERTIFICATE');
        $('.tec :input').prop('readonly', true);
        $('.tec .select2').prop('disabled', true);
        $('#tec_section').trigger("change");
        $('#mop').trigger("change");


        $('.fulltax :input').prop('readonly', true);
        $('.fulltax .select2').prop('disabled', true);
        $('#class_type').trigger("change");


        $('.rtt :input').prop('readonly', true);
        $('.rtt .select2').prop('disabled', true);
        $('#rtt_section').trigger("change");
  });
  $('#generate_cert').click(function()
  {
        var type = "{{$type}}";
        var id = "";
        if(type == "tec") id = $('#tec_id').val();
        else if(type == "rtt") id = $('#rtt_id').val();
        else if(type == "fulltax") id = $('#ft_id').val();

        var url = "{{ route('generate_cert',['id'=>':id','type'=>':type']) }}";
        url = url.replace(':id',id);
        url = url.replace(':type',type);

        location.href = url;

  });
  $('#class_type').change(function()
    {
        if($(this).val() == "1") $('#class_amt').val(f_class);
        else if($(this).val() == "2") $('#class_amt').val(e_class);

    });
  $('#tec_section').change(function()
    {
       
        var url = "{{ route('getSD', ":id") }}";
        url = url.replace(':id', $(this).val());
        $.ajax({
            url:url,
            type:"get",
            success:function(result)
            {
                $('#section_name').val(result);
            },
            error:function(result)
            {
                console.log(result);
            }
        })
    });
    $('#rtt_section').change(function()
    {
        var url = "{{ route('getSD', ":id") }}";
        url = url.replace(':id', $(this).val());
        $.ajax({
            url:url,
            type:"get",
            success:function(result)
            {
                $('#rtt_section_name').val(result);
            },
            error:function(result)
            {
                console.log(result);
            }
        })
    });

    $('#mop').change(function()
    {
        if($(this).val() == '3' || $(this).val() == "4")
        {
            $('.mop-card').addClass('d-block');
            $('.mop-card').removeClass('d-none');
            $('.mop-dollar').addClass('d-none');
            $('.mop-dollar').removeClass('d-block');
        }
        else if ($(this).val() == "2")
        {
            $('.mop-card').addClass('d-none');
            $('.mop-card').removeClass('d-block');
            $('.mop-dollar').removeClass('d-none');
            $('.mop-dollar').addClass('d-block');
        }
        else{
            $('.mop-card').addClass('d-none');
            $('.mop-dollar').addClass('d-none');
            $('.mop-card').removeClass('d-block');
            $('.mop-dollar').removeClass('d-block');

        }

    });

</script>
@endsection

