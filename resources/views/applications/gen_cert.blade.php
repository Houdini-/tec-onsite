
<!DOCTYPE html>
<html>
<style>
body
{
    background:url("{{ asset('img/greenbg_world.png') }}");
   background-position:center top;
   background-size:750px 450px;
   background-repeat:no-repeat;
   
}   
</style>
<head>
    <title>Hi</title>
</head>
<body>
    <h2><center>Travel Tax Exemption Certificate</center></h2>
    <img src="{{ asset('img/girl.png') }}" width="115" height="115" style="margin-top:-95px;position:absolute">
    <br>
    <div style="font-size:10pt">

    <p>This is to certify that <strong>Mr./Ms. </strong>{{ $tec->first_name.' '.$tec->last_name}} with passport number <strong>{{ $tec->passport_no}} is exempted </strong> from payment 
    of travel tax pursuant to Sec.21 of President Decree (P.D) 1183 , as amended.

    </p>
    <img src="{{ asset('img/greenlogo.png') }}" height="500" width="700" style="position:absolute;margin-top:-155px;z-index:-1">
    <br>
    <label>Ticket Number/Booking Ref. Number: <strong><u>{{$tec->ticket_no}}</u></strong></label><br>
    <label>Date Issued:  <strong><u>{{$tec->date_ticket_issued}}</u></strong></label><br>
    <label>TEC No:  <strong><u>TEC-0001</u></strong></label><br>
    <img src="{{ asset('img/cristeto_ocampo_.png') }}" height="150" width="200" style="position:absolute;margin-top:-85px;margin-left:500px;z-index:-1">

    <div style="text-align:right">
       <strong> CRISTETO G. OCAMPO</strong><br>
       <i>Department Manager</i><br>
       <i>Travel Tax Department</i>
    </div>
</div>
    
        <h5><strong><u>IMPORTANT REMINDERS</u></strong></h5>
        <div style="font-size:8pt">
            <strong>1. 	Valid only for travel to:</strong></br>
            <strong>2. 	Not valid if departing after:</strong></br>
            <strong>3. 	Not valid with erasures/alterations <span style="margin-left:400px">MBV0013-ELR0010</span></strong></br>
            <strong>4. 	Valid only for one departure/issuance of ticket <span style="margin-left:350px">{{$tec->date_application}}</span> </strong></br>
        </div>
    <br>
    <br>
    <div style="font-size:8pt"><b>
        <center><i>PLEASE PRINT THIS DOCUMENT AND PRESENT TO THE CHECK IN COUNTER ON THE DAY OF YOUR FLIGHT.</i></center></br>
        <center><i>INCASE YOU ARE BOOKING/REBOOKING YOUR TICKET, PRESENT THIS DOCUMENT TO THE AIRLINE TICKETING OFFICES.</i></center></b>
    </div>
</body>
</html>