@extends('layouts.master-auth')
<link rel="stylesheet" href="{{asset('css/application.css')}}">
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
  <div class="card-header card-header-contrast ">Transaction</div>
    <div class=" card-body ">
      <div class="card card-contrast border-primary " id="">
        <div class=" card-body p-0 ">
          <div class="text-center bg-primary p-2 text-white">Payment Details</div>
          <div class="row p-2">
            <div class="col-1 text-right"><label for="" class="pt-2">PAYEE</label></div>
            <div class="col-7">
              <input type="text" class="form-control form-control-sm" value="{{$details[0]->last_name.', '.$details[0]->first_name.' '.$details[0]->middle_name}}" readonly>
              <br>
              <form action="{{route('save_payment_or')}}" method="post">
                @csrf
                <input type="hidden" name="_id" value="{{$details[0]->onsite_reference}}">
                <div class="row p-1">
                  <div class="col-4 pt-2 text-right">
                      <label for="">OFFICIAL RECEIPT</label>
                  </div>
                  <div class="col-8">
                      <input type="text" id="or_nos" name="or_no" class="form-control form-control-sm" value="{{$details[0]->or_no}}" disabled>
                  </div>
                </div>
                <div class="row p-1">
                  <div class="col-4 pt-2 text-right">
                      <label for="">PAYMENT OPTION</label>
                  </div>
                  <div class="col-8 ">
                    <select name="payment_option" id="payment_option" class="form-control form-control-sm" tabindex="3" disabled>
                        <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                        <option value="1" @if($details[0]->mop == "1") selected @endif>PESO</option>
                        <option value="2" @if($details[0]->mop == "2") selected @endif>DOLLAR</option>
                        <option value="3" @if($details[0]->mop == "3") selected @endif>MASTERCARD</option>
                        <option value="4" @if($details[0]->mop == "4") selected @endif>VISA</option>
                        <option value="5" @if($details[0]->mop == "5") selected @endif>MIXED PAYMENT</option>
                      </select>
                    <hr></hr>
                    <div class="py-1 div_mixed d-none">
                      <label for="">Mixed Options</label>
                      <select class="form-control form-control-sm @error('fulltax_type') is-invalid @endif" 
                          name="mop_multi" 
                          id="mop_multi" disabled>    
                          <option value="" disabled selected>{{ __('page.please_select') }}</option> 
                          <option value="1" @if( $details[0]->mop_multi  == "1")selected @endif>Peso/Dollar</option>
                          <option value="2" @if( $details[0]->mop_multi  == "2")selected @endif>Peso/Credit Card</option>
                          <option value="3" @if( $details[0]->mop_multi  == "3")selected @endif>Dollar/Credit Card</option>
                          <option value="4" @if( $details[0]->mop_multi  == "4")selected @endif>Peso/Dollar/Credit Card</option>
                      </select>
                    </div>
                    <div class="py-1 row div_peso d-none">
                      <div class="col-12">
                        <label for=""> Peso Amount</label>
                        <input type="text" name="peso_" id="peso_" class="form-control form-control-sm" value="{{$details[0]->peso_amt}}" disabled>
                      </div>
                    </div>
                    <div class="py-1 row div_dollar d-none">
                      <div class="col-12">
                        <label for=""> Dollar Amount</label>
                        <input type="text" name="dollar_" id="dollar_" class="form-control form-control-sm"value="{{$details[0]->dollar_rate}}" disabled>
                      </div>
                    </div>
                    <div class="py-1 row div_card d-none">
                      <div class="col-6">
                        <label for=""> Card Reference No</label>
                        <input type="text" name="card_ref" id="card_ref" class="form-control form-control-sm"value="{{$details[0]->card_ref}}" disabled>
                      </div>
                      <div class="col-6">
                        <label for=""> Card No</label>
                        <input type="text" name="card_no" id="card_no" class="form-control form-control-sm"value="{{$details[0]->card_no}}" disabled>
                      </div>
                    </div>
                    
                    <div class="text-right py-2">
                      <button type="button" id="btn_edit" class="btn btn-primary"><i class="mdi mdi-edit"></i> Edit</button>
                      <div class="div_edit d-none">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" id="btn_cancel" class="btn btn-danger">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-4  text-center  ">
              <div class="card card-contrast card-border-color shadow card-border-color-primary" id="home">
                <div class="card-header card-header-contrast ">Transaction</div>
                  <div class=" card-body ">
                    <label for="" class="h4">TOTAL AMOUNT</label>
                    <br>
                    <label for="" class="h3 text-danger"id= "total_amt"></label>
                    <br><br>
                    <a href="/onsite_print_or/{{$details[0]->onsite_reference}}" id="print_or" class="btn btn-success btn-lg text-white" > <i class="mdi mdi-print"></i> PRINT RECEIPT</a>
                  </div>
              </div>
            </div>
          </div>
          <div class="p-2">
            <table id="tblMain" class="table table-striped   border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
              <thead class="bg-primary text-white">
                <tr>
                    <th>SECTION</th>
                    <th>CLASS</th>
                    <th>LAST NAME</th> 
                    <th>FIRST NAME</th> 
                    <th>M.I</th> 
                    <th>CERTIFICATE NO.</th> 
                    <th>TRANSACTION</th> 
                    <th>AMOUNT</th> 
                    {{-- <th></th>  --}}
                </tr>
              </thead>
              <tbody>
                @php $total_amt = 0; @endphp
                @foreach ($details as $item)
                
                <tr>
                  <td>{{$item->section}}</td>
                  <td>{{$item->class}}</td>
                  <td>{{$item->last_name}}</td> 
                  <td>{{$item->first_name}}</td> 
                  <td>{{$item->middle_name}}</td> 
                  <td>{{$item->cert_no}}</td> 
                  <td>{{$item->trans}}</td> 
                  <td>{{number_format($item->_amount,2)}}</td> 
                  {{-- <td>
                    <button type="button" name="" id="add_passenger" class="btn btn-primary">  EDIT</button>
                    <button type="button" name="" id="add_passenger" class="btn btn-danger">REMOVE</button>
                  </td>  --}} 
                </tr>
                @php $total_amt += floatval($item->_amount); @endphp
                @endforeach
                
              </tbody>
            </table>
          </div>
          
          <br>
          <div class="card-footer card-footer-contrast p-2 ">
            <div class="row p-2">
                <div class="col-1 pt-2 text-right">
                  <label for="">CASHIER</label>
                </div>
                <div class="col-1">
                  <input type="text" class="form-control form-control-sm" value="@if(Auth::user()->isCashier()) {{Auth::user()->username}} @else {{$usettings->user->username }} @endif" readonly>
                </div>
                <div class="col-3">
                  <input type="text" class="form-control form-control-sm" value=" @if(Auth::user()->isCashier()) {{Auth::user()->getFullName()}} @else {{$usettings->user->first_name.' '.$usettings->user->last_name}} @endif " readonly>
                </div>
                <div class="col-1 offset-1">
                    <input type="text" class="form-control form-control-sm" value="{{count($details)}} PAX" readonly>
                </div>
                <div class="col-2 pt-2">
                    <label for="">(MAXIMUM OF 5 PAX)</label>
                </div>
                <div class="col-3 text-right">
                    <button type="button" name="" id="add_record" class="btn btn-primary btn-lg" > <i class="mdi mdi-plus"></i> ADD NEW RECORD</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="modal-container modal-effect-1 full-width" id="nft-default">
  <div class="modal-content">
    <div class="modal-header">
      <h3>CERTIFICATE</h3>
      <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="rtt d-none">
        <div class="details">
          <img src="{{asset('img/rtt-clean.png')}}" height="350px" width="80%" alt="" >
          <label for="" class="d_name"></label><br>
          <label for="" class="d_passport"></label>
          <label for="" class="d_amount"></label>
          <label for="" class="d_section"></label>
          <label for="" class="d_date">{{date('m-d-Y', strtotime(now()))}}</label>
          <label for="" class="d_processor"></label>
          <label for="" class="d_certno"></label>
        </div>
      </div>
      <div class="tec d-none">
        <div class=" details">
          <img src="{{asset('img/tec-clean.png')}}" height="350px" width="80%" alt="">
          <label for="" class="tec_name"></label>
          <label for="" class="tec_passport"></label>
          <label for="" class="tec_section"></label>
          <label for="" class="tec_ticket"></label>
          <label for="" class="tec_date">{{date('m-d-Y', strtotime(now()))}}</label>
          <label for="" class="tec_or"></label>
          <label for="" class="tec_processor"></label>
          <label for="" class="tec_certno"></label>
        </div>
      </div>
      <div class="text-right p-2">
        <button type="button" class="btn btn-primary btn_print" id=""> <i class="mdi mdi-print"></i> PRINT CERTIFICATE</button>
      </div>
    </div>
  </div>
</div>
<div class="modal-overlay"></div>
@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
  // window.print();
    App.formElements();
    $.fn.niftyModal('setDefaults',{
        overlaySelector: '.modal-overlay',
        contentSelector: '.modal-content',
        closeSelector: '.modal-close',
        classAddAfterOpen: 'modal-show'
    });
    window.onhashchange = function() {
    }
    var count_trans = '{{count($details)}}';
    var counter = 0;
    var trans_type = '{{$details[0]->trans}}';
    if(trans_type == "EXEMPTION"){
      $('.tec').removeClass('d-none');
      $('.rtt').addClass('d-none');
      $('.tec_name').text('{{$details[0]->first_name.' '.$details[0]->middle_name.' '.$details[0]->last_name}}');
      $('.tec_passport').text('{{$details[0]->passport_no}}');
      $('.tec_section').text('{{$details[0]->section}}');
      $('.tec_ticket').text('{{$details[0]->ticket_no}}');
      $('.tec_or').text('N/A');
      $('.tec_certno').text('{{$details[0]->cert_no}}');
      $('.tec_processor').text('{{$details[0]->processor_name}}');
    }
    else if(trans_type == "REDUCED"){
      $('.rtt').removeClass('d-none');
      $('.tec').addClass('d-none');
      $('.d_name').text('{{$details[0]->first_name.' '.$details[0]->middle_name.' '.$details[0]->last_name}}');
      $('.d_passport').text('{{$details[0]->passport_no}}');
      $('.d_section').text('{{$details[0]->section}}');
      $('.d_amount').text('{{$details[0]->_amount}}');
      $('.d_certno').text('{{$details[0]->cert_no}}');
      $('.d_processor').text('{{$details[0]->processor_name}}');
      
    }
    $('.btn_print').attr('id','{{$details[0]->id ?? ""}}')
    
    if(trans_type != "FULL TAX") $('#nft-default').niftyModal('show');
    else show_nextcert();
    $('#total_amt').text('{{number_format($total_amt,2)}}');
    $('#add_record').click(function()
    {
        location.href="{{route('application.index')}}";
    });
    $('.close').click(function(){
      show_nextcert();
    });
    $('.btn_print').click(function()
    {
      var id = $(this).attr('id');
      if(id != ""){
        print_cert(id,'EXEMPTION');
      }
      show_nextcert();
    });
    $('#btn_edit').click(function()
    {
      $('.div_edit').removeClass('d-none');
      $(this).addClass('d-none');
      $('#payment_option').prop('disabled',false);
      $('#or_nos').prop('disabled',false);

    });
    $('#btn_cancel').click(function()
    {
        $('.div_edit').addClass('d-none');
        $('#btn_edit').removeClass('d-none');
        $('#payment_option').prop('disabled',true);
        $('#or_nos').prop('disabled',true);
    });
    $('#payment_option').change(function(){
        var value = $(this).val();
        if(value == "1"){
          $('.div_peso').removeClass('d-none');
          $('.div_dollar').addClass('d-none');
          $('.div_card').addClass('d-none');
          $('.div_mixed').addClass('d-none');

          $('#peso_').prop('disabled',false);
        }
        else if(value == "2"){
          $('.div_dollar').removeClass('d-none');
          $('.div_peso').addClass('d-none');
          $('.div_card').addClass('d-none');
          $('#dollar_').prop('disabled',false);
          $('.div_mixed').addClass('d-none');

        }
        else if(value == "3" || value == "4"){
          $('.div_card').removeClass('d-none');
          $('.div_peso').addClass('d-none');
          $('.div_dollar').addClass('d-none');
          $('#card_ref').prop('disabled',false);
          $('#card_no').prop('disabled',false);
          $('.div_mixed').addClass('d-none');

        }
        else if(value == "5"){
          $('.div_mixed').removeClass('d-none');
          $('#mop_multi').prop('disabled',false);
          
        }
    });
    $('#mop_multi').change(function()
    {
      var value = $(this).val();
      if(value =="1"){
        $('.div_peso').removeClass('d-none');
        $('.div_dollar').removeClass('d-none');
        $('.div_card').addClass('d-none');
        $('#peso_').prop('disabled',false);
        $('#dollar_').prop('disabled',false);
      }
      else if(value == "2"){
        $('.div_peso').removeClass('d-none');
        $('.div_card').removeClass('d-none');
        $('.div_dollar').addClass('d-none');
        $('#peso_').prop('disabled',false);
        $('#card_ref').prop('disabled',false);
        $('#card_no').prop('disabled',false);
      }
      else if(value == "3"){
        $('.div_dollar').removeClass('d-none');
        $('.div_card').removeClass('d-none');
        $('.div_peso').addClass('d-none');
        $('#dollar_').prop('disabled',false);
        $('#card_ref').prop('disabled',false);
        $('#card_no').prop('disabled',false);
      }
      else if(value == "4"){
        $('.div_dollar').removeClass('d-none');
        $('.div_card').removeClass('d-none');
        $('.div_peso').removeClass('d-none');
        $('#peso_').prop('disabled',false);
        $('#dollar_').prop('disabled',false);
        $('#card_ref').prop('disabled',false);
        $('#card_no').prop('disabled',false);
      }
    })
    $('#payment_option').change();
    $('#mop_multi').change();
    function show_nextcert(){
      counter ++;
      if(counter <= count_trans) {
        if(counter == 1){
          var trans_type = '{{$details[1]->trans ?? "" }}';
          if(trans_type == "EXEMPTION"){
            $('.tec').removeClass('d-none');
            $('.rtt').addClass('d-none');
            $('.tec_name').text('{{ strtoupper(($details[1]->first_name ?? "").' '.($details[1]->middle_name ?? "").' '.($details[1]->last_name??""))}}');
            $('.tec_passport').text('{{$details[1]->passport_no ?? ""}}');
            $('.tec_section').text('{{$details[1]->section ?? ""}}');
            $('.tec_ticket').text('{{$details[1]->ticket_no ?? ""}}');
            $('.tec_or').text('N/A');
            $('.tec_certno').text('{{$details[1]->cert_no ?? ""}}');
            $('.tec_processor').text('{{$details[1]->processor_name ?? ""}}');
          }
          else if(trans_type == "REDUCED"){
            $('.rtt').removeClass('d-none');
            $('.tec').addClass('d-none');
            $('.d_name').text('{{strtoupper(($details[1]->first_name??"").' '.($details[1]->middle_name??"").' '.($details[1]->last_name ?? ""))}}');
            $('.d_passport').text('{{$details[1]->passport_no ?? ""}}');
            $('.d_section').text('{{$details[1]->section ?? ""}}');
            $('.d_amount').text('{{$details[1]->_amount ?? ""}}');
            $('.d_certno').text('{{$details[1]->cert_no ?? ""}}');
            $('.d_processor').text('{{$details[1]->processor_name ?? ""}}');
          }
          $('.btn_print').attr('id','{{$details[1]->id ?? ""}}');

        }
        else if(counter == 2){
          var trans_type = '{{$details[2]->trans ?? "" }}';
          if(trans_type == "EXEMPTION"){
            $('.tec').removeClass('d-none');
            $('.rtt').addClass('d-none');
            $('.tec_name').text('{{ strtoupper(($details[2]->first_name ?? "").' '.($details[2]->middle_name ?? "").' '.($details[2]->last_name??""))}}');
            $('.tec_passport').text('{{$details[2]->passport_no ?? ""}}');
            $('.tec_section').text('{{$details[2]->section ?? ""}}');
            $('.tec_ticket').text('{{$details[2]->ticket_no ?? ""}}');
            $('.tec_or').text('N/A');
            $('.tec_certno').text('{{$details[2]->cert_no ?? ""}}');
            $('.tec_processor').text('{{$details[2]->processor_name ?? ""}}');
          }
          else if(trans_type == "REDUCED"){
            $('.rtt').removeClass('d-none');
            $('.tec').addClass('d-none');
            $('.d_name').text('{{strtoupper(($details[2]->first_name??"").' '.($details[2]->middle_name??"").' '.($details[2]->last_name ?? ""))}}');
            $('.d_passport').text('{{$details[2]->passport_no ?? ""}}');
            $('.d_section').text('{{$details[2]->section ?? ""}}');
            $('.d_amount').text('{{$details[2]->_amount ?? ""}}');
            $('.d_certno').text('{{$details[2]->cert_no ?? ""}}');
            $('.d_processor').text('{{$details[2]->processor_name ?? ""}}');
          }
          $('.btn_print').attr('id','{{$details[2]->id ?? ""}}')

        }
        else if(counter == 3){
          var trans_type = '{{$details[3]->trans ?? "" }}';
          if(trans_type == "EXEMPTION"){
            $('.tec').removeClass('d-none');
            $('.rtt').addClass('d-none');
            $('.tec_name').text('{{ strtoupper(($details[3]->first_name ?? "").' '.($details[3]->middle_name ?? "").' '.($details[3]->last_name??""))}}');
            $('.tec_passport').text('{{$details[3]->passport_no ?? ""}}');
            $('.tec_section').text('{{$details[3]->section ?? ""}}');
            $('.tec_ticket').text('{{$details[3]->ticket_no ?? ""}}');
            $('.tec_or').text('N/A');
            $('.tec_certno').text('{{$details[3]->cert_no ?? ""}}');
            $('.tec_processor').text('{{$details[3]->processor_name ?? ""}}');
          }
          else if(trans_type == "REDUCED"){
            $('.rtt').removeClass('d-none');
            $('.tec').addClass('d-none');
            $('.d_name').text('{{strtoupper(($details[3]->first_name??"").' '.($details[3]->middle_name??"").' '.($details[3]->last_name ?? ""))}}');
            $('.d_passport').text('{{$details[3]->passport_no ?? ""}}');
            $('.d_section').text('{{$details[3]->section ?? ""}}');
            $('.d_amount').text('{{$details[3]->_amount ?? ""}}');
            $('.d_certno').text('{{$details[3]->cert_no ?? ""}}');
            $('.d_processor').text('{{$details[3]->processor_name ?? ""}}');
          }
          $('.btn_print').attr('id','{{$details[3]->id ?? ""}}')

        }
        else if(counter == 4){
          var trans_type = '{{$details[4]->trans ?? "" }}';
          if(trans_type == "EXEMPTION"){
            $('.tec').removeClass('d-none');
            $('.rtt').addClass('d-none');
            $('.tec_name').text('{{ strtoupper(($details[4]->first_name ?? "").' '.($details[4]->middle_name ?? "").' '.($details[4]->last_name??""))}}');
            $('.tec_passport').text('{{$details[4]->passport_no ?? ""}}');
            $('.tec_section').text('{{$details[4]->section ?? ""}}');
            $('.tec_ticket').text('{{$details[4]->ticket_no ?? ""}}');
            $('.tec_or').text('N/A');
            $('.tec_certno').text('{{$details[4]->cert_no ?? ""}}');
            $('.tec_processor').text('{{$details[4]->processor_name ?? ""}}');
          }
          else if(trans_type == "REDUCED"){
            $('.rtt').removeClass('d-none');
            $('.tec').addClass('d-none');
            $('.d_name').text('{{strtoupper(($details[4]->first_name??"").' '.($details[4]->middle_name??"").' '.($details[4]->last_name ?? ""))}}');
            $('.d_passport').text('{{$details[4]->passport_no ?? ""}}');
            $('.d_section').text('{{$details[4]->section ?? ""}}');
            $('.d_amount').text('{{$details[4]->_amount ?? ""}}');
            $('.d_certno').text('{{$details[4]->cert_no ?? ""}}');
            $('.d_processor').text('{{$details[4]->processor_name ?? ""}}');
          }
          $('.btn_print').attr('id','{{$details[4]->id ?? ""}}')

        }
       
       if( trans_type == "REDUCED" || trans_type == "EXEMPTION") $('#nft-default').niftyModal('show');
      }
    }
    function print_cert(id,type){
        if(type == "FULL TAX") type = "fulltax";
        else if(type == "EXEMPTION") type = "tec";
        else if(type == "REDUCED") type = "rtt";

        var url = "{{ route('generate_cert',['id'=>':id','type'=>':type']) }}";
        url = url.replace(':id',id);
        url = url.replace(':type',type);
        location.href = url;
    }
})

</script>
@endsection