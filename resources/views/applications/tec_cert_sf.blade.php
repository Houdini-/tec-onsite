<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<link rel="stylesheet" href="{{asset('css/print.css')}}" media="print" type="text/css">
<style media="print">
    .noPrint{ display: none; }
    .yesPrint{ display: block !important; }
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style type="text/css">
        body {
            font-family: courier;
            font-size: 15pt;
            font-weight:bold;
            /*  */
           
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
     

        .page-header, .page-header-space {
            height: 1000px;
            width:1000px;
            /* background:url("{{ asset('img/pc-teccertsf.png') }}"); */
            background-repeat: no-repeat;
            position: absolute;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: block;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }



        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: none;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
        p{
            
        }
    </style>
</head>
<body>
   
    <div>
        <div class="container-fluid">
            <div class="col-md-12">
          
                        <div class="page">
                            <div class="page-header-space">
                               <br><br><br>
                                <p style="margin-top:23px;margin-left:250px;font-size 24px;">{{ strtoupper($tec->first_name.' '.$tec->middle_name .' '.$tec->last_name) }}</p>
                             
                                <p style="margin-top:-23px;margin-left:12px;">{{$tec->passport_no}}</p>
                                <p style="margin-top:-20px;margin-left:12px;">{{$tec->code}}
                                    <span style="margin-left:395px;">{{$tec->ticket_no}}</span>
                                </p>
                                <p style="margin-top:-15px;margin-left:60px;">
                                    {{$tec->date_ticket_issued}}
                                </p>
                                
                                <p style="margin-top:-15px;margin-left:60px;">
                                    <!-- //or_no -->

                                </p>
                                <p style="margin-top:45px;margin-left:25px;">
                                     {{$tec->processor_name}}
                                 </p>
                            </div>
                           
                        </div>

            </div>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function()
{
    window.print();

    window.onafterprint = function(){
        history.go(-1);
        
    }
   
})
</script>
</body>

</html>