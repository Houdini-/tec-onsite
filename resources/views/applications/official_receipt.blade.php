<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<link rel="stylesheet" href="{{asset('css/print.css')}}" media="print" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <style type="text/css">
        body {
            font-family: courier;
            font-size: 15pt;
            font-weight:bold;
            /*  */
           
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
     

        .page-header, .page-header-space {
            height: 1000px;
            width:1000px;
            /* background:url("{{ asset('img/orsf.png') }}"); */
            background-repeat: no-repeat;
            position: absolute;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: block;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }



        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: none;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
        p{
            
        }
    </style>
</head>
<body>
   
    <div>
        <div class="container-fluid">
            <div class="col-md-12">
          
                        <div class="page">
                            <div class="page-header-space">
                            <p style="margin-left:60px;margin-top:220px">
                                {{date('m-d-Y', strtotime(now()));}}
                            </p>
                                <p style="margin-left:60px;margin-top:40px">
                                    @if($recipients) {{ count($recipients).' PAX'}} @else {{$ft->payor_name ?? ''}} @endif
                                </p>
                                <p style="margin-top:55px;margin-left:20px;font-size:14px;height:190px"> 
                                @if($recipients)
                                    @foreach($recipients as $items)
                                    <span style="">{{$items->first_name.' '.$items->last_name}}</span>
                                    <span style="margin-top:55px;margin-left:65px;"> {{$items->ticket_no}} </span>
                                    <span style="margin-top:55px;margin-left:55px"> {{$items->fulltax_amount}}</span>
                                    <br><br>
                                    @php $total += $items->fulltax_amount; @endphp
                                    @endforeach
                                @else
                                <span >{{$ft->payor_name}}</span>
                                    <span style="margin-top:55px;margin-left:65px;"> {{$ft->ticket_no}} </span>
                                    <span style="margin-top:55px;margin-left:55px"> {{$ft->fulltax_amount}}</span>
                                    <br><br>
                                @endif
<!--                                 
                                <span >{{$ft->payor_name}}</span>
                                <span style="margin-top:55px;margin-left:65px;"> {{$ft->ticket_no}} </span>
                                <span style="margin-top:55px;margin-left:55px"> {{$ft->fulltax_amount}}</span>
                                <br><br>
                                <span>{{$ft->payor_name}}</span>
                                <span style="margin-left:65px;"> {{$ft->ticket_no}} </span>
                                <span style="margin-left:55px"> {{$ft->fulltax_amount}}</span>
                                <br><br>
                                <span>{{$ft->payor_name}}</span>
                                <span style="margin-left:65px;"> {{$ft->ticket_no}} </span>
                                <span style="margin-left:55px"> {{$ft->fulltax_amount}}</span> -->
                                </p>
                                <p style="margin-top:-5px;margin-left:325px;"> @if($total != 0) {{$total}} @else {{$ft->fulltax_amount}} @endif</p>
                                
                                <p style="margin-top:185px;margin-left:200px;"> {{$ft->processor_name}}</p>
                              
                            </div>
                           
                        </div>

            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function()
    {
        window.print();
    })
</script>
</body>

</html>