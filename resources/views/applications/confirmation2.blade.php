@extends('layouts.master-auth')
<link rel="stylesheet" href="{{asset('css/application.css')}}">
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
  <div class="card-header card-header-contrast ">Transaction</div>
    <div class=" card-body ">
      <div class="card card-contrast border-primary " id="">
        <div class=" card-body p-0 ">
          <div class="text-center bg-primary p-2 text-white">Applicant Details</div>
          <div class="p-2">
            <table id="tblMain" class="table table-striped   border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
              <thead class="bg-primary text-white">
                <tr>
                    <th>NAME OF PASSENGER</th>
                    <th>PASSPORT NUMBER</th>
                    <th>SECTION</th> 
                    <th>TICKET NUMBER</th> 
                    <th>DATE ISSUED</th> 
                    <th>TEC NUMBER</th> 
                    <th></th> 
                </tr>
              </thead>
              <tbody>
                  @foreach ($details as $item)
                  <tr>
                      <td>{{$item->first_name.' '.$item->middle_name.' '.$item->last_name}}</td>
                      <td>{{$item->passport_no}}</td>
                      <td>{{$item->section}}</td>
                      <td>{{$item->ticket_no}}</td>
                      <td>{{$item->application_date}}</td>
                      <td>{{$item->cert_no}}</td>
                      <td><button class="btn btn-primary " onclick="location.href = '{{route('generate_cert',["id" => $item->id,"type"=> "tec"])}}'"> <i class="mdi mdi-print"></i> PRINT CERTIFICATE</button></td>
                   </tr>
                   @endforeach
              </tbody>
            </table>
          <div class="card-footer card-footer-contrast p-2 text-right ">
            <button class="btn btn-primary " onclick="location.href='/application'"> <i class="mdi mdi-plus"></i> ADD RECORD</button>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
  

})
</script>
@endsection