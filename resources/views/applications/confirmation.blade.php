@extends('layouts.master-auth')
<link rel="stylesheet" href="{{asset('css/application.css')}}">
@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
  <div class="card-header card-header-contrast ">Transaction</div>
    <div class=" card-body ">
      <div class="card card-contrast border-primary " id="">
        <div class=" card-body p-0 ">
          <div class="text-center bg-primary p-2 text-white">Payment Details</div>
          <div class="row p-2">
            <div class="col-1 text-right"><label for="" class="pt-2">PAYEE</label></div>
            <div class="col-4">
              <input type="text" class="form-control form-control-sm" value="{{$details[0]->last_name.', '.$details[0]->first_name.' '.$details[0]->middle_name}}" readonly>
            </div>
            <div class="col-1">
              <input type="text" class="form-control form-control-sm" value="{{count($details)}} PAX" readonly>
            </div>
            <div class="col-2 pt-2">
              <label for="">(MAXIMUM OF 5 PAX)</label>
            </div>
          </div>
          <br>
          <div class="p-2">
            <table id="tblMain" class="table table-striped   border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
              <thead class="bg-primary text-white">
                <tr>
                    <th>SECTION</th>
                    <th>CLASS</th>
                    <th>LAST NAME</th> 
                    <th>FIRST NAME</th> 
                    <th>M.I</th> 
                    <th>CERTIFICATE NO.</th> 
                    <th>TRANSACTION</th> 
                    <th>AMOUNT</th> 
                    <th></th> 
                </tr>
              </thead>
              <tbody>
                @php $total_amt = 0; @endphp
                @foreach ($details as $item)
                
                <tr>
                  <td>{{$item->section}}</td>
                  <td>{{$item->class}}</td>
                  <td>{{$item->last_name}}</td> 
                  <td>{{$item->first_name}}</td> 
                  <td>{{$item->middle_name}}</td> 
                  <td>{{$item->cert_no}}</td> 
                  <td>{{$item->trans}}</td> 
                  <td>{{number_format($item->_amount,2)}}</td> 
                  <td>
                    <button type="button" name="" id="add_passenger" class="btn btn-primary">  EDIT</button>
                    <button type="button" name="" id="add_passenger" class="btn btn-danger">REMOVE</button>
                  </td> 
                </tr>
                @php $total_amt += floatval($item->_amount); @endphp
                @endforeach
                
              </tbody>
            </table>
          </div>
          <div class="row p-2">
            <div class="col-1 pt-2 text-right">
              <label for="">CASHIER</label>
            </div>
            <div class="col-1">
              <input type="text" class="form-control form-control-sm" value="{{$usettings->user->username}}" readonly>
            </div>
            <div class="col-3">
              <input type="text" class="form-control form-control-sm" value="{{$usettings->user->first_name.' '.$usettings->user->last_name}}" readonly>
            </div>
            <div class="col-5 text-right">
              <label for="" class="h4"><b> TOTAL AMOUNT</b></label>
            </div>
            <div class="col-2">
              <label for="" class="h4 text-danger"><b> {{number_format($total_amt,2)}}</b></label>
            </div>
          </div>
          <br>
          <div class="card-footer card-footer-contrast p-2 ">
            <div class=" text-right">
              <button type="button" name="" id="{{$details[0]->onsite_reference}}" class="btn btn-primary btn-lg send_cashier" > <i class="mdi mdi-mail-send"></i> SEND TO CASHIER</button>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript"> 
$(document).ready(function()
{
  $('.send_cashier').click(function()
  {
    var url = "{{route('payment_',['id' =>':id' ,'cashier' => ':cashier'])}}";
    url = url.replace(':id',$(this).attr('id'));
    url = url.replace(':cashier',"{{$usettings->user->id}}");

    location.href = url;
  })

})
</script>
@endsection