@extends('layouts.master-auth')
<link rel="stylesheet" href="{{asset('css/application.css')}}">
<link rel="stylesheet" href="{{asset('css/print.css')}}" media="print" type="text/css">

@section('content')
<div class="card card-contrast card-border-color card-border-color-primary" id="home">
  <div class="card-header card-header-contrast ">Transaction</div>
    <div class=" card-body ">
      <div class="card card-contrast border-primary " id="">
        <div class=" card-body p-0 ">
          <div class="text-center bg-primary p-2 text-white">Applicant Information</div>
            <div class="row pt-3 px-2">
              <div class="col-4">
                  <label for="">LAST NAME</label>
                  <input type="text" id="_lastname" class="form-control form-control-sm" >
              </div>
              <div class="col-4">
                <label for="">FIRST NAME</label>
                <input type="text" id="_firstname"class="form-control form-control-sm" tabindex="1">
              </div>
              <div class="col-4">
                <label for="">M.I</label>
                <input type="text" id="_middlename" class="form-control form-control-sm" tabindex="2">
              </div>
            </div>
            <div class="row p-1">
              <div class="col-4"><br><br><br>
                <input type="text" id="_amount" class="form-control form-control-lg text-center text-danger font-weight-bold h-50  " tabindex="-1"readonly> 
              </div>
              <div class="col-8">
                <div class="row p-1">
                  <div class="col-1 pt-2">
                    <label for="code" >CODE</label>
                  </div>
                  <div class="col-5"> 
                    <select name="" id="_code" class="form-control form-control-sm" tabindex="3">
                      <option value="1">FULLTAX</option>
                      <option value="2">EXEMPTION</option>
                      <option value="3">REDUCED</option>
                    </select>
                  </div>
                  <div class="col-6 d-none with_ticket">
                    <select name="" id="_withticket" class="form-control form-control-sm text-danger font-weight-bold" tabindex="3">
                      <option value="1">WITH TICKET</option>
                      <option value="0">WITHOUT TICKET</option>
                    </select>
                  </div>
                </div>
                <div class="row p-1">
                  <div class="col-1 pt-2">
                    <label for="">SECTION</label>
                  </div>
                  <div class="col-2 pr-0">
                    <select name="" id="_sectioncode" class="form-control form-control-sm rtt_amt" tabindex="4">
                      {{-- @foreach ($section as $item)
                          <option value="{{$item->id}}">{{$item->code}}</option>
                      @endforeach --}}
                    </select>
                  </div>
                  <div class="col-9">
                    <textarea name="_sectiontext" id="_sectiontext" class="w-100" style="font-size:11px" tabindex="-1" row="10" col="50"  disabled>
                    </textarea>
                  </div>
                </div>
                <div class="row p-1">
                  <div class="col-1 pt-2">
                    <label for="code" >CLASS</label>
                  </div>
                  <div class="col-5">
                    <select name="" id="_class" class="form-control form-control-sm rtt_amt" tabindex="5">
                      <option value="2">ECONOMY/BUSINESS CLASS</option>
                      <option value="1">FIRST CLASS</option>
                    </select>
                  </div>
                </div>
                <div class="row p-1">
                  <div class="col-1 pt-2">
                    <label for="">TYPE</label>
                  </div>
                  <div class="col-11">
                    <select name="" id="_type" class="form-control form-control-sm" tabindex="6">
                      @foreach ($passenger as $item)
                          <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="row p-1 d-others d-none">
                  <div class="col-1 pt-2">
                    <label for=""></label>
                  </div>
                  <div class="col-5">
                    <input type="text" id="_type_others" class="form-control form-control-sm" tabindex="2">
                  </div>
                </div>
              </div>
            </div>
            <hr class="mx-4"></hr>
            <div class="row p-1">
              <div class="col-5 pl-2 pt-2 text-right">
                  <label for="code" >PASSPORT NUMBER</label>
              </div>
              <div class="col-7">
                <div class="row">
                  <div class="col-5 p-0 ">
                    <input type="text" id="_passport"class="form-control form-control-sm" tabindex="7">
                  </div>
                </div>
              </div>
            </div>
            <div class="row p-1">
              <div class="col-5 pl-2 pt-2 text-right">
                  <label for="code" >CONFIRMATION/TICKET NUMBER</label>
              </div>
              <div class="col-7">
                <div class="row">
                  <div class="col-5 p-0 ">
                    <input type="text" id="_ticket" class="form-control form-control-sm" tabindex="8">
                  </div>
                </div>
              </div>
            </div>
            <div class="row p-1">
              <div class="col-5 pl-2 pt-2 text-right">
                  <label for="code" >VALIDITY</label>
              </div>
              <div class="col-7">
                <div class="row">
                  <div class="col-5 p-0 ">
                    <input type="date" id="_validity" class="form-control form-control-sm" tabindex="9">
                  </div>
                </div>
              </div>
            </div>
            <div class="row p-1">
              <div class="col-5 pl-2 pt-2 text-right">
                  <label for="code" >DESTINATION</label>
              </div>
              <div class="col-7">
                <div class="row">
                  <div class="col-5 p-0 ">
                    <select name="" id="_destination" class="form-control form-control-sm" tabindex="10">
                      @foreach ($countries as $item)
                          <option value="{{$item->id}}">{{$item->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div><br>
          </div>
          <div class="card-footer card-footer-contrast p-2 ">
            <div class="row">
              <div class="col-6 ">
                {{-- <label for="" id="lbl_type"></label>
                <label for="" id="lbl_cert" class="h3"></label> --}}
              </div>
              <div class="col-6 text-right">
                <button type="button" name="" id="add_passenger" class="btn btn-primary" tabindex="11"> <i class="mdi mdi-plus"></i> ADD PASSENGER</button>
                <div class="d-none div_upd">
                  <button type="button" name="" id="update_passenger" class="btn btn-primary" > <i class="mdi mdi-edit"></i> UPDATE PASSENGER</button>
                  <button type="button" name="" id="cancel_btn" class="btn btn-danger" > CANCEL</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="px-5">
        <table id="tblMain" class="table table-striped   border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
          <thead class="bg-primary text-white">
            <tr>
                <th>--</th>
                <th>LAST NAME</th>
                <th>FIRST NAME</th> 
                <th>M.I</th> 
                <th>TICKET NUMBER</th> 
                <th>DESTINATION</th> 
                <th>AMOUNT</th> 
                <th>TRANSACTION</th> 
                <th class="d-none">--</th>  
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><button type="button" class="btn btn-primary btntbl_edit"></button></td>
              <td><label id="lbl_lastname" for=""></label></td>
              <td><label id="lbl_firstname" for=""></label></td> 
              <td><label id="lbl_middlename" for=""></label></td> 
              <td><label id="lbl_ticket" for=""></label></td> 
              <td><label id="lbl_destination" for=""></label></td> 
              <td><label id="lbl_amount" for=""></label></td> 
              <td><label id="lbl_transaction" for=""></label></td> 
              <td class="d-none">
                <input type="text" id="lastname">
                <input type="text" id="firstname">  
                <input type="text" id="middlename">  
                <input type="text" id="code">  
                <input type="text" id="section">  
                <input type="text" id="class">  
                <input type="text" id="type"> 
                <input type="text" id="passport">  
                <input type="text" id="ticket">  
                <input type="text" id="validity">  
                <input type="text" id="destination">  
                <input type="text" id="amount">  
                <input type="text" id="withticket">  
              </td> 
          </tr>
          </tbody>
        </table>
        <table id="tblCopy" class="table table-striped bg-primary d-none border table-fw-widget text-uppercase" id="tblItems" style="overflow:auto !important" >
          <thead class="bg-primary text-white">
            <tr>
                <th>--</th>
                <th>LAST NAME</th>
                <th>FIRST NAME</th> 
                <th>M.I</th> 
                <th>TICKET NUMBER</th> 
                <th>DESTINATION</th> 
                <th>AMOUNT</th> 
                <th>TRANSACTION</th> 
                {{-- <th class="d-none">--</th>  --}}
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><button type="button" class="btn btn-primary btntbl_edit"></button></td>
              <td><label id="lbl_lastname" for=""></label></td>
              <td><label id="lbl_firstname" for=""></label></td> 
              <td><label id="lbl_middlename" for=""></label></td> 
              <td><label id="lbl_ticket" for=""></label></td> 
              <td><label id="lbl_destination" for=""></label></td> 
              <td><label id="lbl_amount" for=""></label></td> 
              <td><label id="lbl_transaction" for=""></label></td> 
              <td class="d-none">
                <input type="text" id="lastname">
                <input type="text" id="firstname">  
                <input type="text" id="middlename">  
                <input type="text" id="code">  
                <input type="text" id="section">  
                <input type="text" id="class">  
                <input type="text" id="type"> 
                <input type="text" id="passport">  
                <input type="text" id="ticket">  
                <input type="text" id="validity">  
                <input type="text" id="destination">  
                <input type="text" id="amount">  
                <input type="text" id="withticket">  
                <input type="text" id="type_others">  

              </td> 
          </tr>
          </tbody>
        </table>
        <div class="row">
          <div class="col-12 text-right">
            <button type="button" name="" id="save_btn" class="btn btn-primary"> <i class="mdi mdi-print"></i> &nbsp&nbsp&nbsp SAVE &nbsp&nbsp&nbsp</button>
          </div>
        </div>
      </div>
      <br><br><br><br><br>  
    </div>
  </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript"> 

    var f_class = "2,700.00";
    var e_class = "1,620.00";
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
    var rowCount = 0;
    var trCurID = "";
  $(document).ready(function () {
        App.formElements();
       $('#_lastname').focus();
       $('#_class').change();
       $('#_code').change();
       $('#_sectioncode').change();
  });
  $('.form-control').on('keypress', function(e) {

    if(e.keyCode == 13 ) {
      e.preventDefault();
      var tabindex = $(e.target).prop('tabindex') + 1;
      if($('[tabindex=' + tabindex + ']').is(':disabled')) tabindex ++;
      if($('[tabindex=' + tabindex + ']').is(':disabled') && $('#_code').val('2') ) tabindex++;
      $('[tabindex=' + tabindex + ']').focus();
    }
    
  });
  $('.form-control').on('keydown',function(e){
    if(e.ctrlKey && e.keyCode == 83 ){
      e.preventDefault();
      $('#save_btn').click();
    }
  });
  $('#_code').on('keypress',function(e)
  {
      e.preventDefault();
      if(e.keyCode == 69 || e.keyCode == 101) {
        $(this).val('2');
        exemption();
        getsection('1');
      }
      else if(e.keyCode == 70 || e.keyCode == 102){
        $(this).val('1');
        fulltax();
      }
      else if(e.keyCode == 82 || e.keyCode == 114){
        $(this).val('3');
        reduced();
        getsection('3');
      }
     
  })
  $('#_code').change(function(){
    var val = $(this).val();
    var type = "";
    if(val == "1") fulltax();
    else if(val == "2") { exemption(); type = "1";}
    else if(val == "3") { reduced(); type = "3";}

    if(val != "1"){
     getsection(type);
    }
    
  })
  $('#_class').on('keypress',function(e)
  {
      e.preventDefault();
      if(e.keyCode == 69 || e.keyCode == 101) {//economoy
        $(this).val('2');
      }
      else if(e.keyCode == 70 || e.keyCode == 102){//firstclass
        $(this).val('1');
      }
      $(this).change();
  })
  $('#_class').change(function(){
    if($('#_code').val() == "1"){
      var val = $(this).val();
      if(val == "2") $('#_amount').val(e_class);
      else if(val == "1") $('#_amount').val(f_class);
    }
  })
  $('#add_passenger').click(function(){
      var trans_type = $('#_code').val();
      if(validateFields(trans_type))
      {
        if(rowCount == 0)  $('table#tblMain tbody>tr').empty();
        rowCount += 1;
        $("#tblCopy tbody>tr:first").clone(true).insertAfter("#tblMain tbody>tr:last");
        $("#tblMain tbody>tr:last").attr('id','row_id'+rowCount);
        $("#tblMain tbody>tr:last .btntbl_edit").text("EDIT");
        $("#tblMain tbody>tr:last #lbl_lastname").text($('#_lastname').val());
        $("#tblMain tbody>tr:last #lbl_firstname").text($('#_firstname').val());
        $("#tblMain tbody>tr:last #lbl_middlename").text($('#_middlename').val());
        $("#tblMain tbody>tr:last #lbl_ticket").text($('#_ticket').val());
        $("#tblMain tbody>tr:last #lbl_destination").text($('#_destination :selected').text());
        var trans = "";
        if(trans_type == "1") trans = "FULLTAX";
        else if(trans_type == "2") trans = "EXEMPTION";
        else if(trans_type == "3") trans = "REDUCED";
        
        $("#tblMain tbody>tr:last #lbl_transaction").text(trans);
        $('#tblMain tbody>tr:last #lastname').val($('#_lastname').val());
        $('#tblMain tbody>tr:last #firstname').val($('#_firstname').val());
        $('#tblMain tbody>tr:last #middlename').val($('#_middlename').val());
        $('#tblMain tbody>tr:last #section').val($('#_sectioncode').val());
        $('#tblMain tbody>tr:last #code').val($('#_code').val());
        $('#tblMain tbody>tr:last #class').val($('#_class').val());
        $('#tblMain tbody>tr:last #type').val($('#_type').val());
        $('#tblMain tbody>tr:last #passport').val($('#_passport').val());
        $('#tblMain tbody>tr:last #ticket').val($('#_ticket').val());
        $('#tblMain tbody>tr:last #validity').val($('#_validity').val());
        $('#tblMain tbody>tr:last #destination').val($('#_destination').val());
        $('#tblMain tbody>tr:last #withticket').val($('#_withticket').val());
        $('#tblMain tbody>tr:last #type_others').val($('#_type_others').val());

        if(trans == "EXEMPTION"){
          $("#tblMain tbody>tr:last #lbl_amount").text("0.00");
          $('#tblMain tbody>tr:last #amount').val("0.00");
        }
        else {
          $("#tblMain tbody>tr:last #lbl_amount").text($('#_amount').val());
          $('#tblMain tbody>tr:last #amount').val($('#_amount').val());
        }

        $('#_lastname').val("");
        $('#_firstname').val("");
        $('#_middlename').val("");
        $('#_code').val("1");
        $('#_code').change();
        $('#_class').val("2");
        $('#_type').val("1");
        $('.rtt_amt').change();
        $('#_passport').val("");
        $('#_ticket').val("");
        $('#_validity').val("");
        $('#_destination').val("2");
        if(rowCount == 5) {
          $(this).addClass('d-none');
        }
        $('input').removeClass('is-invalid');
        $('#_lastname').focus();

      }

  })
  $('#_sectioncode').change(function()
  {
    if($('#_code').val() == "1") return;
    var url = "{{ route('getSD', ":id") }}";
    url = url.replace(':id', $(this).val());
    $.ajax({
        url:url,
        type:"get",
        success:function(result)
        {
          $ ('#_sectiontext').val(result);
        },
        error:function(result)
        {
            console.log(result);
        }
    })
  })
  $('#save_btn').click(function()
  {
    var check = $('#row_id1 #lastname').val();
    if(check == null) {
           var error = "Please add atleast 1 passenger.";
            const swal_error = alert_warning(error, 1500);
            swal_error.then((value) => {
            });
            return;
    }
    var formData = new FormData();
    $("#tblMain tr").each(function() { 
       if($(this).attr('id'))
       {
         var id = $(this).attr('id');
         formData.append('lastname[]',$('#'+id+' #lastname').val());
         formData.append('firstname[]',$('#'+id+' #firstname').val());
         formData.append('middlename[]',$('#'+id+' #middlename').val());
         formData.append('code[]',$('#'+id+' #code').val());
         formData.append('section[]',$('#'+id+' #section').val());
         formData.append('class[]',$('#'+id+' #class').val());
         formData.append('type[]',$('#'+id+' #type').val());
         formData.append('passport[]',$('#'+id+' #passport').val());
         formData.append('ticket[]',$('#'+id+' #ticket').val());
         formData.append('validity[]',$('#'+id+' #validity').val());
         formData.append('destination[]',$('#'+id+' #destination').val());
         formData.append('amount[]',$('#'+id+' #amount').val());
         formData.append('withticket[]',$('#'+id+' #withticket').val());
         formData.append('type_others[]',$('#'+id+' #type_others').val());

       }
    });   
    Swal.fire({
      title: '', 
      html: '<b>Are you sure you want to save this record?</b> <br> If yes, click the continue button, otherwise, cancel to review your details.',
      type:'question',
      confirmButtonText: 'Continue',
      showCancelButton: true,
      cancelButtonText: 'Cancel'
      }).then((result) => {
      if (result.value) {
        $.ajax({
          url:"{{route('application.store')}}",
          type:"post",
          data:formData,
          processData: false,
          contentType: false,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          success:function(response)
          {
            if(response != ""){
              success = "You will be redirected to another page..";
              const swal_success = alert_success(success, 1500);
              swal_success.then((value) => {
              var url = "{{route('confirmation_',':id')}}";
              url = url.replace(':id',response);
              location.href = url;
              });
            }
          },
          error:function(response)
          {
              alert(response);
          }
        });
      }
    });
  })
  $('.rtt_amt').change(function(){
    var code = $('#_code').val();
    if(code == "3" && section != ""){
      var section = $('#_sectioncode').val();
      var _class = $('#_class').val();
      $.ajax({
        url:"/getrtt_amt/"+section+"/"+_class,
        type:"get",
        success:function(response){
          $('#_amount').val(response);
        },
        error:function(response){

        }
      })

    }
  })
  $('.btntbl_edit').click(function()
  {
    var id = $(this).closest('tr').attr('id');
    trCurID = id;
    var type = $('#'+id+' #code').val();
    if(type == "2") type = "1";
    getsection(type);
    $('#_lastname').val($('#'+id+' #lastname').val());
    $('#_firstname').val($('#'+id+' #firstname').val());
    $('#_middlename').val($('#'+id+' #middlename').val());
    $('#_code').val($('#'+id+' #code').val());
    $('#_class').val($('#'+id+' #class').val());
    $('#_type').val($('#'+id+' #type').val());
    $('#_passport').val($('#'+id+' #passport').val());
    $('#_ticket').val($('#'+id+' #ticket').val());
    $('#_validity').val($('#'+id+' #validity').val());
    $('#_destination').val($('#'+id+' #destination').val());
    $('#_code').change();
    
    var section = $('#'+id+' #section').val();
    setTimeout(function() { 
      $('#_sectioncode').val(section);
      $('#_sectioncode').change();
    }, 100);

    $('#_lastname').focus();
    $('.div_upd').removeClass('d-none');
    $('.div_upd').addClass('d-block');
    $('#add_passenger').addClass('d-none');
  })
  $("#cancel_btn").click(function()
  {
    $('.div_upd').removeClass('d-block');
    $('.div_upd').addClass('d-none');
    // $('#add_passenger').addClass('d-block');
    $('#add_passenger').removeClass('d-none');

  })
  $('#update_passenger').click(function()
  {
    $('#'+trCurID+' #lastname').val($('#_lastname').val());
    $('#'+trCurID+' #firstname').val($('#_firstname').val());
    $('#'+trCurID+' #middlename').val($('#_middlename').val());
    $('#'+trCurID+' #code').val($('#_code').val());
    $('#'+trCurID+' #amount').val($('#_amount').val());
    $('#'+trCurID+' #section').val($('#_sectioncode').val());
    $('#'+trCurID+' #class').val($('#_class').val());
    $('#'+trCurID+' #type').val($('#_type').val());
    $('#'+trCurID+' #passport').val($('#_passport').val());
    $('#'+trCurID+' #ticket').val($('#_ticket').val());
    $('#'+trCurID+' #validity').val($('#_validity').val());
    $('#'+trCurID+' #destination').val($('#_destination').val());
    $('#'+trCurID+' #lbl_lastname').text($('#_lastname').val());
    $('#'+trCurID+' #lbl_firstname').text($('#_firstname').val());
    $('#'+trCurID+' #lbl_middlename').text($('#_middlename').val());
    $('#'+trCurID+' #lbl_ticket').text($('#_ticket').val());
    $('#'+trCurID+' #lbl_destination').text($('#_destination :selected').text());
    $('#'+trCurID+' #lbl_amount').text($('#_amount').val());

    var code = $('#_code').val();
    if(code == "1") code = "FULLTAX";
    else if(code == "2") code = "EXEMPTION";
    else if(code == "3") code = "REDUCED";

    if(code == "EXEMPTION") {
      $('#'+trCurID+' #lbl_amount').text("0.00");
      $('#'+trCurID+' #amount').val("");
    }
    $('#'+trCurID+' #lbl_transaction').text(code);
  })
  $('#_type').change(function(){
    if($(this).val() == "9")$('.d-others').removeClass('d-none');
    else $('.d-others').addClass('d-none');
  })
  function fulltax(){
      $('#_sectioncode').prop('disabled',true);
      $('#_validity').prop('disabled',true);
      $('#_passport').prop('disabled',true);
      $('#_type').prop('disabled',false);
      $('#_class').prop('disabled',false);

      $('.with_ticket').addClass('d-none');

  }
  function exemption(){
      $('#_sectioncode').prop('disabled',false);
      $('#_validity').prop('disabled',false);
      $('#_type').prop('disabled',true);
      $('#_class').prop('disabled',true);
      $('.with_ticket').addClass('d-none');
      $('#_passport').prop('disabled',false);

      // $('#lbl_type').text('TEC: ');
      // $('#lbl_cert').text('{{$usettings['tec_no'] ?? ''}}');


  }
  function reduced(){
      $('#_sectioncode').prop('disabled',false);
      $('#_validity').prop('disabled',false);
      $('#_type').prop('disabled',true);
      $('#_class').prop('disabled',false);
      $('.with_ticket').removeClass('d-none');
      $('#_passport').prop('disabled',false);

  }
  function validateFields(type)
  {
    
    if(!$('#_lastname').val().trim() ) { $('#_lastname').addClass('is-invalid');}
    if(!$('#_firstname').val().trim() ) { $('#_firstname').addClass('is-invalid');}
    if(!$('#_middlename').val().trim() ) { $('#_middlename').addClass('is-invalid');}
    if(!$('#_passport').val().trim() && type != "1") { $('#_passport').addClass('is-invalid');}
    if(!$('#_ticket').val().trim() ) { $('#_ticket').addClass('is-invalid');}

    if($('#_lastname').val().trim() && $('#_firstname').val().trim() && $('#_middlename').val().trim()
         && $('#_ticket').val().trim()) {
         
          if( !$('#_passport').val().trim() && type != "1" ) return false;
          else return true;
    }
    else return false;
  }
  function getsection(type)
  {
    var url = "/get_sectwhere/"+type;
      $.ajax({
        url:url,
        type:"get",
        success:function(response){
          $('#_sectioncode').find('option').remove().end();
          for (let index = 0; index < response.length; index++) {
            const element = response[index];
            var o = new Option(element.code, element.id);
            $("#_sectioncode").append(o);
          }
          $('#_sectioncode').change();
        },
        error:function(response){

        }
      })
  }

</script>
@endsection