@extends('layouts.master-auth')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/css/jquery.niftymodals.css') }}"/>
<style>
</style>
@endsection

@section('content')
    <div class="page-head">
        <div class="page-head-title">Welcome!</div>
        <div class="page-description">
                Online Travel Tax Application
        </div>
    </div>
    <br>
@endsection


@section('scripts')
<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
    @yield('additional-scripts')
   
    <script type="text/javascript">
       
    </script>
@endsection