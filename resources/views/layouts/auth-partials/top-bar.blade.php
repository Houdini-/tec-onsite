<nav class="navbar navbar-expand fixed-top be-top-header">
    <div class="container-fluid">
        <div class="be-navbar-header ">
            <a href="/" >
                <img src="{{ asset('img/tieza-nav-3.png') }}" alt="Logo" height="65px" width="100%">
            </a>
        </div>
        <div class="page-title"><span>{{ __('page.'.$module) }}</span></div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
                <li class="nav-item text-white">
                    {{-- <button type="button" name="" id="" class=" nav-link" ><i class="mdi mdi-cog"></i>User Settings</button> --}}
                    <a href="#" id="modal_settings" role="button"  data-modal="modal1" 
                    data-dismiss="modal"  class="nav-link border border-white md-trigger"><i class="mdi mdi-settings"></i> User Settings</a>
                
                </li>
                <li class="nav-item text-white">
                    <a href="#" role="button"class="nav-link ">{{ '('.trans('page.'.Auth::user()->getUserLevel()).') '.Auth::user()->username.' - '.Auth::user()->getFullName()}}</a>
                
                </li>
                <li class="nav-item dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><img src="{{ asset('img/avatar-150.png') }}" alt="Avatar"></a>
                    <div role="menu" class="dropdown-menu">
                        <div class="user-info">
                            <div class="user-name">
                                {{ Auth::user()->getFullName() }}
                            </div>
                            <div class="user-position online">Available</div>
                        </div>
                        <a href="{{ url('/account') }}" class="dropdown-item"><span class="icon mdi mdi-account-circle"></span>&nbsp;{{ __('page.account') }}
                        </a>
                        <a href="{{ route('logout') }}"  class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="icon mdi mdi-power"></span>&nbsp;{{ __('page.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="modal-container custom-width colored-header colored-header-primary custom-width modal-effect-16" id="modal1" tabindex="-1" role="dialog">
    <div class="modal-content modal-dialog ">
        <div class=" modal-header  modal-header-colored">
        <h3 class="modal-title"><span class="mdi mdi-settings"></span> User Settings</h3>
        <button class="close modal-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
        </div>
        <div class="modal-body">
            <form action="{{route('usettings_save')}}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="row py-1">
                <div class="col-3 py-1 text-right">
                    <label for="code">Office/Location</label>
                </div>
                <div class="col-9">
                        <select class="form-control form-control-sm" name="office_id" id="office_id">
                        <option selected disabled>Please select option</option>
                            @foreach($office as $item)
                            <option value="{{$item->id}}"  @if($item->id == $usettings['office_id'])selected @endif>{{$item->name}}</option>

                            @endforeach
                        </select>
                </div>
            </div>
            <div class="row py-1">
                <div class="col-3 py-1 text-right">
                    <label for="code">Shift</label>
                </div>
                <div class="col-9">
                      <select class="form-control form-control-sm" name="shift_id" id="shift_id">
                        <option selected disabled>Please select option</option>
                         @foreach($shifts as $item)
                         <option value="{{$item->id}}" @if($item->id == $usettings['shift_id'])selected @endif>{{$item->name.' ('.$item->from.'-'.$item->to.')'}}</option>

                         @endforeach
                      </select>
                </div>
            </div>
        @if(Auth::user()->isSuperAdmin()|| Auth::user()->isProcessor())
            <hr></hr>
            <div class="row py-1">
                <div class="col-3 py-2 text-right">
                    <label for="code">Series</label>
                </div>
                <div class="col-4">
                   <input type="text" value="{{$usettings['start_tec'] ?? ''}}" class="form-control form-control-sm" name="start_tec" id="start_tec" >
                </div>
                <div class="col-1 py-2 text-right">
                    <label for="code">To</label>
                </div>
                <div class="col-4">
                    <input type="text" value="{{$usettings['end_tec'] ?? ''}}" class="form-control form-control-sm" name="end_tec" id="end_tec" >
                 </div>
            </div>
            <div class="row">
                <div class="col-3 py-1 text-right">
                    <label for="code">Exemption Certificate No.</label>
                </div>
                <div class="col-9">
                   <input type="text" value="{{$usettings['tec_no'] ?? ''}}" class="form-control form-control-sm" name="tec_no" id="tec_no" >
                </div>
            </div>

            <hr></hr>
            <div class="row py-1">
                <div class="col-3 py-2 text-right">
                    <label for="code">Series</label>
                </div>
                <div class="col-4">
                   <input type="text" value="{{$usettings['start_rtt'] ?? ''}}" class="form-control form-control-sm" name="start_rtt" id="start_rtt" >
                </div>
                <div class="col-1 py-2 text-right">
                    <label for="code">To</label>
                </div>
                <div class="col-4">
                    <input type="text" value="{{$usettings['end_rtt'] ?? ''}}" class="form-control form-control-sm" name="end_rtt" id="end_rtt" >
                 </div>
            </div>
            <div class="row">
                <div class="col-3 py-1 text-right">
                    <label for="code">Reduced Certificate No.</label>
                </div>
                <div class="col-9">
                   <input type="text" value="{{$usettings['rtt_no'] ?? ''}}" class="form-control form-control-sm" name="rtt_no" id="rtt_no" >
                </div>
            </div>
            @endif
            <hr></hr>

            <div class="row py-1">
                <div class="col-3 py-2 text-right">
                    <label for="code">Series</label>
                </div>
                <div class="col-4">
                   <input type="text" value="{{$usettings['start_or'] ?? ''}}" class="form-control form-control-sm" name="start_or" id="start_or" >
                </div>
                <div class="col-1 py-2 text-right">
                    <label for="code">To</label>
                </div>
                <div class="col-4">
                    <input type="text" value="{{$usettings['end_or'] ?? ''}}" class="form-control form-control-sm" name="end_or" id="end_or" >
                 </div>
            </div>
            <div class="row">
                <div class="col-3 py-1 text-right">
                    <label for="code">Official Receipt No.</label>
                </div>
                <div class="col-9">
                   <input type="text" value="{{$usettings['or_no'] ?? ''}}" class="form-control form-control-sm" name="or_no" id="or_no" >
                </div>
            </div>
            @if(Auth::user()->isSuperAdmin() || Auth::user()->isProcessor())
            <hr></hr>
            <div class="row py-1">
                <div class="col-3 py-1 text-right">
                    <label for="code">Cashier ID</label>
                </div>
                <div class="col-9">
                     <select class="form-control form-control-sm" name="level" id="cashier">
                        <option  >Please select option</option>
                         @foreach($cashier as $item)
                         <option value="{{$item->id}}" @if($usettings['or_no'] ?? '' == $item->id) selected @endif>{{$item->username.' - '.( $item->first_name .' '.$item->last_name )}}</option>

                         @endforeach
                      </select>
                </div>
            </div>
            @endif
            <br>
            <div class="row py-1">
                <div class="col-3 py-1 text-right">
                
                </div>
                <div class="col-9 text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger modal-close" onclick="">Cancel</button>

                </div>
            </div>
            </form>
        </div>
        
    </div>
</div>
<script src="{{ asset('beagle-v1.7.1/dist/html/assets/lib/jquery.niftymodals/js/jquery.niftymodals.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        $.fn.niftyModal('setDefaults',{
      	overlaySelector: '.modal-overlay',
      	contentSelector: '.modal-content',
      	closeSelector: '.modal-close',
      	classAddAfterOpen: 'modal-show'
        });

       
    });
    // $( window ).on("load", function() {
        
    //     $.ajax({
    //         url:"{{route('get_usettings')}}",
    //         type:"get",
    //         success:function(response)
    //         {
    //             $('#tec_no').val(response.tec_no);
    //             $('#rtt_no').val(response.rtt_no);
    //             $('#or_no').val(response.or_no);
    //             $('#cashier').val(response.cashier_id);
    //         },
    //         error:function(response)
    //         {
    //             alert("error");
    //         }
    //     });
    // });
</script>