<style>
a{
    color:inherit !important;
}
</style>
<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="/home">MENU</a>
        <div class="left-sidebar-spacer">
          <div class="left-sidebar-scroll">
            <div class="left-sidebar-content">
                    <ul class="sidebar-elements ">
                        <li class="divider ">ONSITE</li>
                        @if(Auth::user()->isSuperAdmin() || Auth::user()->isProcessor())
                        <li class=""><a href="{{route('home.index')}}"><i class="icon mdi mdi-home"></i><span>Home</span></a></li>
                        <li class=""><a href="{{route('filesetup.index')}}"><i class="icon mdi mdi-file-text"></i><span>File Setup</span></a> </li>
                        <li class=""><a href="{{route('dashboard.index')}}"><i class="icon mdi mdi-view-dashboard"></i><span>Dashboard</span></a></li>
                        <li class=""><a href="{{route('application.index')}}"><i class="icon mdi mdi-flight-takeoff"></i><span>Transactions</span></a> </li>
                        @endif

                        @if(Auth::user()->isSuperAdmin()|| Auth::user()->isProcessor() || Auth::user()->isCashier()) 
                        <li class=""><a href="{{route('records.index')}}"><i class="icon mdi mdi-storage"></i><span>Records</span></a> </li>
                        @endif
                        @if(Auth::user()->isSuperAdmin()|| Auth::user()->isProcessor())
                        {{-- <li class=""><a href="{{route('collection.index')}}"><i class="icon mdi mdi-collection-text"></i><span>Collections</span></a> </li> --}}
                        <li class=""><a href="{{route('reports.index')}}"><i class="icon mdi mdi-format-list-numbered"></i><span>Reports</span></a> </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
      
	$(document).ready(function(){
        App.init();
    });
</script>
    