
	<script src="{{ asset('beagle-v1.7.1/src/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('beagle-v1.7.1/src/js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('beagle-v1.7.1/src/js/app-ui-sweetalert2.js') }}"></script>
	<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/moment.js/min/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-form-elements.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('beagle-v1.7.1/src/assets/lib/jquery.sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-charts-sparkline.js')}}" type="text/javascript"></script>
  
	<script src="{{ asset('axios/axios.min.js')}}"></script> 