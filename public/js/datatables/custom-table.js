        
        function load_datables(tblID, jsonURL, columns, orderby = 1, ordercol = 'desc')
        {
            var datatable_obj = {
                'destroy' : true,
                'processing' : true,
                'serverSide' : true,
                'responsive' : true,
                'autoWidth' : false,
                'ajax' : {
                    "url" : jsonURL
                },
                "aLengthMenu" : [[25, 50, 75, -1], [25, 50, 75, "All"]],
                "iDisplayLength" : 25,
                "columns" : columns,
                "bStateSave" : true,
            }

            if(orderby)
            {
                datatable_obj["order"] = [[ orderby, ordercol]];
            }

            $(tblID).DataTable(datatable_obj);
        }